//
//  ReminderTimeModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-16.
//

import Foundation


enum TimeOfDay {
case am, pm
}

struct ReminderTimeModel {
    
    private var hourValue : Int = 1
    private var minutesValue : Int = 1
    private var ampmValue : String = "AM"
    
    var hour : Int {
        get { hourValue }
    }
    
    var minutes : Int {
        get { minutesValue }
    }
    
    var ampm : String {
        get { ampmValue }
    }
    
    mutating func set24Hour(_ hour : Int) {
        
        let verified24Hour = min(max(hour, 0), 23)
        let hour12Format = verified24Hour > 12 ? verified24Hour - 12 : verified24Hour
        let verifiedHourValue = min(max(hour12Format, 1), 12)
        
        ampmValue = verified24Hour > 11 ? "PM" : "AM"
        hourValue = verifiedHourValue
    }
    
    mutating func set12Hour(_ hour : Int) {
        
        let verifiedHour = min(max(hour, 1), 12)
        hourValue = verifiedHour
    }
    
    mutating func setMinutes(_ minutes : Int) {
        
        let verifiedMinutesValue = min(max(minutes, 1), 59)
        minutesValue = verifiedMinutesValue
    }
    
    mutating func setAMPM(_ timeofday : TimeOfDay) {
        
        ampmValue = timeofday == .am ? "AM" : "PM"
    }
}
