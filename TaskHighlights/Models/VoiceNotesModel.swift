//
//  VoiceNotesModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import Foundation


class VoiceNotesModel: Hashable, Equatable {
    
    static func == (lhs: VoiceNotesModel, rhs: VoiceNotesModel) -> Bool {
        lhs.id == rhs.id
    }
    
    
    let id = UUID()
    let title : String
    let recordingURL : URL
    
    init() {
        self.title = ""
        self.recordingURL = URL(fileURLWithPath: "")
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(title)
        hasher.combine(recordingURL)
    }
}
