//
//  CalendarDayModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import Foundation


struct CalendarDayModel : Hashable, Equatable {
    
    let id = UUID()
    let day : Int
}
