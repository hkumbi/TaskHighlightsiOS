//
//  CustomDateModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import Foundation


struct CustomDateModel : Hashable, Equatable, CustomStringConvertible {
    let id = UUID()
    let day : Int
    let weekday : String
    
    var description: String {
        get { "Day : \(day) Weekday : \(weekday)" }
    }
}
