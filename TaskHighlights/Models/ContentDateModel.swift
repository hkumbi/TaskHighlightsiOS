//
//  ContentDateModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import Foundation


class ContentDateModel : Hashable, Equatable, CustomStringConvertible {
        
    static func == (lhs: ContentDateModel, rhs: ContentDateModel) -> Bool {
        lhs.yearValue == rhs.yearValue &&
        lhs.monthValue == rhs.monthValue &&
        lhs.dayValue == rhs.dayValue
    }
        
    
    private let yearValue : Int
    private let monthValue : Int
    private let dayValue : Int
    
    var year : Int {
        get { yearValue }
    }
    
    var month : Int {
        get { monthValue }
    }
    
    var day : Int {
        get { dayValue }
    }
    
    var description: String {
        get {
            let monthSymbol = Calendar.current.monthSymbols[month-1]
            return "\(monthSymbol) \(day), \(year)"
        }
    }
    
    
    init(_ date : Date) {
        
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        
        self.yearValue = components.year ?? 0
        self.monthValue = components.month ?? 0
        self.dayValue = components.day ?? 0
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(yearValue)
        hasher.combine(monthValue)
        hasher.combine(dayValue)
    }
    
}
