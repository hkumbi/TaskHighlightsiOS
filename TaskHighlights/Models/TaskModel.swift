//
//  TaskModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import Foundation

class TaskModel: Hashable, Equatable {
    
    static func == (lhs: TaskModel, rhs: TaskModel) -> Bool {
        lhs.id == rhs.id
    }

    let id = UUID()
    let string : String = ""
    let description : String = ""
    
    init() {
        
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(string)
        hasher.combine(description)
    }
    
}
