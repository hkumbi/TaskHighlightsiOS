//
//  HighlightModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import Foundation
import UIKit


class HighlightModel : Hashable, Equatable {
    
    static func == (lhs: HighlightModel, rhs: HighlightModel) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    var image : UIImage?
    
    
    init(_ image : UIImage? = nil) {
        self.image = image
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(image)
    }
    
}
