//
//  SectionModel.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import Foundation
import UIKit


enum SectionModel : Hashable, Equatable {
    case main
}

