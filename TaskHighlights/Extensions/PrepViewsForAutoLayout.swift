//
//  PrepViewsForAutoLayout.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import Foundation
import UIKit

extension UIView {
    
    static func viewPreppedForAutoLayout() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}

extension UICollectionView {
    
    static func preppedForAutoLayout() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}

extension DefaultCollectionView {
    
    static func defaultPreppedForAutoLayout() -> DefaultCollectionView {
        let collection = DefaultCollectionView()
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }
}

extension DefaultTableView {
    
    static func defaultPreppedForAutoLayout() -> DefaultTableView {
        let tableView = DefaultTableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }
}

extension UILabel {
    
    static func preppedForAutoLayout() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}


extension UIButton {
    
    static func preppedForAutoLayout() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension UIImageView {
    
    static func preppedForAutoLayout() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}


extension UITextView {
    
    static func preppedForAutoLayout() -> UITextView {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }
}


extension IconButton {
    
    static func iconPreppedForAutoLayout() -> IconButton {
        let button = IconButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension BackgroundButton {
    
    static func backgroundPreppedForAutoLayout() -> BackgroundButton {
        let button = BackgroundButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}


extension UIPickerView {
    
    static func preppedForAutoLayout() -> UIPickerView {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }
}


extension UITextField {
    
    static func preppedForAutoLayout() -> UITextField {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}


extension UISegmentedControl {
    
    static func preppedForAutoLayout() -> UISegmentedControl {
        
        let segmented = UISegmentedControl()
        segmented.translatesAutoresizingMaskIntoConstraints = false
        return segmented
    }
}


extension UIStackView {
    
    static func preppedForAutoLayout() -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
}


extension UISwitch {
    
    static func preppedForAutoLayout() -> UISwitch {
        
        let switchView = UISwitch()
        switchView.translatesAutoresizingMaskIntoConstraints = false
        return switchView
    }
}
