//
//  LineSpacing.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import Foundation
import UIKit


extension UILabel {
    
    func addLineSpacing(_ spacing : CGFloat = 2) {
        
        guard let text = text else { return }
        
        let attributedString = NSMutableAttributedString(string: text)
        
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineSpacing = spacing
        paragraphStyle.alignment = textAlignment
        
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length)
        )
        
        attributedText = attributedString
    }
    
}
