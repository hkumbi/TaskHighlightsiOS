//
//  DateSubtraction.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation


extension Date {
    
    static func - (lhs : Date, rhs : Date) -> TimeInterval {
        lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}
