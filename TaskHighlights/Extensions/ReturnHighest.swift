//
//  ReturnHighest.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-25.
//

import Foundation
import UIKit


extension CGFloat {
    func returnHighest(_ secondValue : CGFloat) -> CGFloat {
        self < secondValue ? secondValue : self
    }
}
