//
//  Color.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import Foundation
import UIKit


extension UIColor {
    
    static var customGreen = UIColor(red: 76/255, green: 147/255, blue: 76/255, alpha: 1)
}
