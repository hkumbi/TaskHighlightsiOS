//
//  TextSize.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import Foundation
import UIKit


extension String {

    static func getLabelHeight(text: String, font: UIFont, width: CGFloat = 0) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    static func getLabelWidth(text: String, font: UIFont, height: CGFloat = 0) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
}

