//
//  TextButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import Foundation
import UIKit


extension UIButton {
    
    func setTitleTextPressColors(_ color : UIColor) {
        
        setTitleColor(color, for: .normal)
        setTitleColor(color.withAlphaComponent(0.5), for: .highlighted)
        setTitleColor(color.withAlphaComponent(0.4), for: .disabled)
    }
    
}
