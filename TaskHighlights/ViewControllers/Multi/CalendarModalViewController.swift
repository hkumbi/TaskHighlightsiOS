//
//  CalendarModalViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class CalendarModalViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, CalendarDayModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, CalendarDayModel>
    
    private let contentView = CalendarView()
    private let calendarHelper = CalendarHelper()
    private lazy var dataSource = makeDataSource()
    
    private var collectionView : UICollectionView {
        get { contentView.modalView.collectionView }
    }
    
    private let dayCellID = "DayCellIdentifier"
    private var selectedDate = Date()
    private var selectedDay : Int?
    
    unowned var vcDelegate : CalendarVCDelegate!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        setUpCollectionView()
        setUpGesture()
        
        setUpCalendarData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalTopConstant = -contentView.modalView.heightConstant
        animateOpen()
    }
    

    private func setUpGesture() {
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesturehandler))
        contentView.setUpModalPanGesture(pan)
    }
    
    private func setUpCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = dataSource
        collectionView.allowsSelection = true
        collectionView.register(CalendarDayCollectionViewCell.self, forCellWithReuseIdentifier: dayCellID)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CalendarDayCollectionViewCell.cellSize
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView.collectionViewLayout = layout
    }
    
    private func makeDataSource() -> DataSource {
        let source = DataSource(collectionView: collectionView){
            [unowned self]collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: dayCellID, for: indexPath) as! CalendarDayCollectionViewCell
            
            cell.text = item.day == 0 ? "" : "\(item.day)"
            cell.isSelected = item.day == selectedDay
            cell.isUserInteractionEnabled = item.day != 0 && item.day != selectedDay
            
            return cell
        }
        
        return source
    }
    
    
    @objc private func panGesturehandler(_ gesture : UIPanGestureRecognizer) {
        
        let transY = gesture.translation(in: view).y
        
        switch gesture.state {
        case .changed:
            let dimmingMultiplier = 1+(transY/contentView.modalView.heightConstant)
            contentView.modalTopConstant = transY <= 0 ? transY : 0
            contentView.dimmingAlpha = CalendarView.maxDimmingAlpha*dimmingMultiplier

        case .ended:
            let closeConstant = -contentView.modalView.heightConstant/4
            
            if transY <= closeConstant {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }

}

extension CalendarModalViewController {
    
    func setStartingDate(_ date : Date) {
        
        selectedDate = date
        calendarHelper.setSelectedMonth(date)
        setUpCalendarData()
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalTopConstant = -contentView.modalView.heightConstant
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            
            dismiss(animated: false)
        }
    }
    
    private func animateOpen() {
        
        view.layoutIfNeeded()

        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalTopConstant = 0
            contentView.dimmingAlpha = CalendarView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func setUpCalendarData() {
        
        let zeroIndexStartingDay = calendarHelper.getFirstDayOfMonth()-1
        let daysInMonth = calendarHelper.getDaysInSelectedMonth()
        var days : [CalendarDayModel] = []

        for _ in 0..<zeroIndexStartingDay {
            days.append(CalendarDayModel(day: 0))
        }
        
        for i in 1...daysInMonth {
            days.append(CalendarDayModel(day: i))
        }
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems(days)
        dataSource.apply(snap, animatingDifferences: false)
        
        contentView.modalView.monthAndYear = calendarHelper.getMonthAndYear()
        
        if let day = calendarHelper.isDateInSelectedMonth(selectedDate) {
            let selectedItem = days.first(where:  { $0.day == day })!
            let selectedIndexPath = dataSource.indexPath(for: selectedItem)
            collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .centeredHorizontally)
        }
    }
    
}


extension CalendarModalViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = dataSource.itemIdentifier(for: indexPath)!
        selectedDate = calendarHelper.getSelectedDate(item.day)
        
        // Share Selected Day
        vcDelegate.selectedDateChanged(selectedDate)
        animateClose()
    }
    
}


extension CalendarModalViewController : CalendarButtonDelegate {
    
    func buttonPressed(for type: CalendarButtonType) {
        
        switch type {
        case .previous:
            previousButtonHandler()
        case .next:
            nextButtonHandler()
        case .close:
            animateClose()
        }
    }
    
}

extension CalendarModalViewController {
    
    func previousButtonHandler() {
        calendarHelper.goToPreviousMonth()
        setUpCalendarData()
    }
    
    func nextButtonHandler() {
        calendarHelper.goToNextMonth()
        setUpCalendarData()
    }
    
}
