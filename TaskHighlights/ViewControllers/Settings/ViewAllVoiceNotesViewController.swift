//
//  ViewAllVoiceNotesViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class ViewAllVoiceNotesViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<ContentDateModel, VoiceNotesModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<ContentDateModel, VoiceNotesModel>
    
    private struct Cells {
        static let header = "HeaderCell"
        static let voiceNote = "VoiceNotesCell"
    }

    private let contentView = SettingsViewAllView()
    
    private lazy var dataSource = makeDataSource()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        setUpCollectionView()
        makeData()
    }
    
    private func makeData() {
        
        var snap = Snap()
        let today = Date()
        
        for i in 0...5 {
            let date = Calendar.current.date(byAdding: .day, value: -i, to: today)!
            let section = ContentDateModel(date)
            
            snap.appendSections([section])
            
            for _ in 0...i {
                snap.appendItems([VoiceNotesModel()], toSection: section)
            }
        }
        
        dataSource.apply(snap)
    }

}


extension ViewAllVoiceNotesViewController {
    
    private func setUpCollectionView() {
        
        let collection = contentView.collectionView
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.allowsSelection = true
        collection.register(ViewAllSectionHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Cells.header)
        collection.register(SettingsVoiceNoteCollectionViewCell.self, forCellWithReuseIdentifier: Cells.voiceNote)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        let spacing = SettingsVoiceNoteCollectionViewCell.spacing
        
        layout.itemSize = SettingsVoiceNoteCollectionViewCell.cellSize
        layout.headerReferenceSize = ViewAllSectionHeaderCollectionReusableView.size
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = spacing
        layout.sectionInset = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        
        return layout
    }
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cells.voiceNote, for: indexPath) as! SettingsVoiceNoteCollectionViewCell
            
            cell.progress = 0
            
            return cell
        }
        
        source.supplementaryViewProvider = {
            [unowned self] collectionView, kind, indexPath in
            
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Cells.header, for: indexPath) as! ViewAllSectionHeaderCollectionReusableView
            
            let section = dataSource.sectionIdentifier(for: indexPath.section)!
            header.date = "\(section)"
            
            return header
        }
        
        return source
    }
    
}

extension ViewAllVoiceNotesViewController : UICollectionViewDelegate {
    
    
    
}


extension ViewAllVoiceNotesViewController : ViewAllButtonDelegate {
    
    func backButtonPressed() {
        print("Back")
    }
    
}
