//
//  SettingsViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class SettingsViewController: UIViewController {
    
    private let contentView = SettingsView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
    }
    

}


extension SettingsViewController : SettingsButtonDelegate {
    
    func buttonPressed(for type: SettingsButtonType) {
        
        switch type {
        case .back:
            print("Back")
        case .highlights:
            print("Highlights")
        case .voiceNotes:
            print("Voice notes")
        case .faq:
            print("FAQ")
        case .deleteMyData:
            print("Delete my data")
        }
    }
    
    func tempUnitChanged(_ value: Int) {
        print("\(value)")
    }
    
        
}
