//
//  ViewAllHighlightsViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class ViewAllHighlightsViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<ContentDateModel, HighlightModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<ContentDateModel, HighlightModel>
    
    private struct Cells {
        static let highlight = "HiglightCell"
        static let header = "HeaderCell"
    }
    
    private let contentView = SettingsViewAllView()
    
    private lazy var dataSource = makeDataSource()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        configureCollectionView()
        makeData()
    }

    private func makeData() {
        
        var snap = Snap()
        let today = Date()
        
        for i in 0...5 {
            let date = Calendar.current.date(byAdding: .day, value: -i, to: today)!
            let section = ContentDateModel(date)
            snap.appendSections([section])
            
            for _ in 0...i {
                snap.appendItems([HighlightModel()], toSection: section)
            }
        }
        
        dataSource.apply(snap)
    }
}

extension ViewAllHighlightsViewController {
    
    private func configureCollectionView() {
        
        let collection = contentView.collectionView
        
        collection.delegate = self
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.allowsSelection = true
        collection.register(SettingsHighlightsCollectionViewCell.self, forCellWithReuseIdentifier: Cells.highlight)
        collection.register(ViewAllSectionHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Cells.header)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = SettingsHighlightsCollectionViewCell.cellSize
        layout.headerReferenceSize = ViewAllSectionHeaderCollectionReusableView.size
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        return layout
    }
    

    private func makeDataSource() -> DataSource {
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self]collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: Cells.highlight, for: indexPath) as! SettingsHighlightsCollectionViewCell
            
            return cell
        }
        
        source.supplementaryViewProvider = {
            [unowned self]collection, kind, indexPath in
            
            guard kind == UICollectionView.elementKindSectionHeader else { return nil }
            
            let header = collection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Cells.header, for: indexPath) as! ViewAllSectionHeaderCollectionReusableView
            
            let section = dataSource.sectionIdentifier(for: indexPath.section)
            header.date = "\(section!)"
            
            return header
                 
        }
        
        return source
    }
    
}


extension ViewAllHighlightsViewController: UICollectionViewDelegate {
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Hello")
    }
    
}


extension ViewAllHighlightsViewController : ViewAllButtonDelegate {
    
    func backButtonPressed() {
        print("Back")
    }
    
}
