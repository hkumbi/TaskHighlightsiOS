//
//  CameraViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit

class CameraViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    private let contentView = CameraView()
    private let sessionHelper = CameraSessionHelper()
    
    private var sessionWasRunning = false
    private var allowNavigationActionValue : Bool = true
    private var allowCaptureActionValue : Bool = true
    private var allowCameraConfigActionValue : Bool = true
    
    unowned var coordinatorDelegate : CameraCoordinatorDelegate!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .black
        view.addSubview(contentView)
        
        contentView.changeViewState(to: .authorized)
        contentView.buttonDelegate = self
        contentView.blockingActionDelegate = self
        
        sessionHelper.configDelegate = self
        sessionHelper.photoActionDelegate = self
        sessionHelper.getPermission()
        sessionHelper.setUpPreview(layer: contentView.previewView.videoPreviewLayer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if sessionWasRunning {
            sessionHelper.startSession()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sessionWasRunning = sessionHelper.sessionIsRunning
        sessionHelper.stopSession()
        allowAllActions()
    }
    
}


extension CameraViewController : CameraConfigActionDelegate {
    
    func cameraRestricted() {
        contentView.changeViewState(to: .restricted)
    }
    
    func cameraDenied() {
        contentView.changeViewState(to: .denied)
    }
    
    func cameraAuthorized() {
        contentView.changeViewState(to: .authorized)
    }
    
    func cameraConfigError() {
        contentView.changeViewState(to: .error)
    }
    
    func sessionConfigSuccess() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(avCaptureError), name: .AVCaptureSessionRuntimeError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(avCaptureSessionInterrupted), name: .AVCaptureSessionWasInterrupted, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(avCaptureSessionInterruptionEnded), name: .AVCaptureSessionInterruptionEnded, object: nil)
    }
    
    func cameraDevicesSwitched() {
        allowAllActions()
    }
    
}

extension CameraViewController {
    
    @objc private func avCaptureSessionInterruptionEnded() {
        if sessionWasRunning {
            sessionHelper.startSession()
        }
    }
    
    @objc private func avCaptureSessionInterrupted() {
        sessionWasRunning = sessionHelper.sessionIsRunning
        
        if sessionWasRunning {
            sessionHelper.stopSession()
        }
    }
    
    @objc private func avCaptureError() {
        sessionHelper.stopSession()
        contentView.changeViewState(to: .error)
    }
    
}


extension CameraViewController : CameraBlockingActionDelegate {
    
    var allowNavigationAction: Bool {
        get { allowNavigationActionValue }
        set { allowNavigationActionValue = newValue }
    }
    var allowCaptureAction: Bool {
        get { allowCaptureActionValue }
        set { allowCaptureActionValue = newValue }
    }
    
    var allowCameraConfigAction: Bool {
        get { allowCameraConfigActionValue }
        set { allowCameraConfigActionValue = newValue }
    }
    
    func flipCameraActionStarted() {
        allowCaptureAction = false
        allowCameraConfigAction = false
    }
    
    func captureActionStarted() {
        allowNavigationAction = false
        allowCaptureAction = false
        allowCameraConfigAction = false
    }

    func navigationActionStarted() {
        allowNavigationAction = false
        allowCaptureAction = false
        allowCameraConfigAction = false
    }
    
    func allowAllActions() {
        allowNavigationAction = true
        allowCaptureAction = true
        allowCameraConfigAction = true
    }
    
    
}


extension CameraViewController : CameraButtonDelegate {
    
    func buttonPressed(for type: CameraButtonType) {
        
        switch type {
        case .close:
            navigationActionStarted()
            coordinatorDelegate.navigationButtonPressed(for: .close)
            
        case .capture:
            captureActionStarted()
            if let orientation = contentView.previewView.orientation {
                sessionHelper.takePhoto(orientation)
            } else {
                showErrorVC()
                allowAllActions()
            }
            
        case .flip:
            flipCameraActionStarted()
            sessionHelper.switchCamera()
            
        case .gallery:
            navigationActionStarted()
            coordinatorDelegate.navigationButtonPressed(for: .gallery)
            
        case .unsplash:
            navigationActionStarted()
            coordinatorDelegate.navigationButtonPressed(for: .unsplash)
            
        case .cameraPermission:
            openSettings()
        }
    }
    
}

//********************************************
//
//  Camera Button Delegate Helper Methods
//
//********************************************

extension CameraViewController {
    
    private func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url)
        else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func showErrorVC() {
        
        let errorVC = UIAlertController(
            title: "Photo Capture Error",
            message: "An error has occured when capturing a photo. Please try again later.",
            preferredStyle: .alert)
        
        errorVC.addAction(UIAlertAction(title: "Cancel", style: .cancel){
            [unowned self]_ in
            dismiss(animated: true)
        })
        
        present(errorVC, animated: true)
    }
        
}


extension CameraViewController : PhotoCaptureActionDelegate {
    
    func photoCaptureError() {
        
        showErrorVC()
        allowAllActions()
    }
    
    func photoCaptured(_ image: UIImage) {
        coordinatorDelegate.photoCaptured(image)
    }
    
}
