//
//  CreateTaskImportanceModalViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import UIKit

class CreateTaskImportanceModalViewController: UIViewController {
    
    private let contentView = CreateTaskImportanceView()
    
    private let importanceData : [String] = ["Low", "Medium", "High"]
    private var closeHeight : CGFloat!
    
    unowned var vcDelegate : CreateTaskImportanceVCDelegate!
    
    var importance : TaskImportance = .low
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        contentView.modalView.pickerDataSource = self
        contentView.modalView.pickerDelegate = self
        
        contentView.dimmingAlpha = CreateTaskImportanceView.maxDimmingAlpha
        
        setUpGesture()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        closeHeight = contentView.modalView.heightConstant/4
        contentView.modalBottomConstant = contentView.modalView.heightConstant
        
        contentView.modalView.setValue(importance.rawValue)
        contentView.modalView.selectedValue = importanceData[importance.rawValue]

        animateOpen()
    }

    private func setUpGesture() {
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        contentView.addPanGesture(pan)
    }
    
    @objc private func handlePan(_ pan : UIPanGestureRecognizer) {
        
        let transY = pan.translation(in: view).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let multiplier = 1-(transY/contentView.modalView.heightConstant)
                
                contentView.modalBottomConstant = transY
                contentView.dimmingAlpha = multiplier*CreateTaskImportanceView.maxDimmingAlpha
            }
            
        case .ended:
            let veloY = pan.velocity(in: view).y
            
            if transY >= closeHeight || veloY > 500 {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }
    
    private func animateOpen() {
        
        view.layoutIfNeeded()

        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = CreateTaskImportanceView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = contentView.modalView.heightConstant
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            
            dismiss(animated: false)
        }
    }
    
}


extension CreateTaskImportanceModalViewController : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        importanceData.count
    }
    
}

extension CreateTaskImportanceModalViewController : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        importanceData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        UIScreen.main.bounds.height*0.05
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        contentView.modalView.selectedValue = importanceData[row]
    }
    
}


extension CreateTaskImportanceModalViewController : CreateTaskImportanceButtonDelegate {
    
    func buttonPressed(for type: CreateTaskImportanceButtonType) {
        
        if type == .save {
            let selectedRow = contentView.modalView.pickerSelectedRow
            let importance = TaskImportance(rawValue: selectedRow)!
            vcDelegate.importanceChanged(to: importance)
        }
        
        animateClose()
    }
    
}
