//
//  CreateVoiceNoteRecordingViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-20.
//

import UIKit
import AVFAudio

class CreateVoiceNoteRecordingViewController: UIViewController {
    
    private let contentView = CreateVoiceNoteView()
    
    private var audioRecorder : AVAudioRecorder!

    private var isProcessingRecordRequest : Bool = false
    private var recordingCancelled : Bool = false
    private var isFirstShowing : Bool = true
    private var uiTimer : Timer?
    private var timerStartDate : Date!
    private let maxRecordingTime : Double = 120
    private var recordingStoppedAfterEnteringBackground : Bool = false
    
    unowned var vcDelegate : CreateVoiceNoteRecordingVCDelegate!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        contentView.timeLeft = "02:00"
        contentView.changeViewState(isMicAllowed: true)
        
        requestAudioPermission()
        
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActiveBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActiveForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isFirstShowing {
            contentView.animateOpen()
            isFirstShowing = false
        }
    }
        
    private func requestAudioPermission() {
        
        AVAudioSession.sharedInstance().requestRecordPermission({
            isAllowed in
            DispatchQueue.main.async {[unowned self] in
                contentView.changeViewState(isMicAllowed: isAllowed)
            }
        })
    }
    
    @objc private func willResignActiveBackground() {
        
        if audioRecorder.isRecording {
            recordingStoppedAfterEnteringBackground = true
            audioRecorder?.stop()
            stopTimer()
            contentView.setToIdleState()
        }
    }

    @objc private func didBecomeActiveForeground() {
        
        guard recordingStoppedAfterEnteringBackground else { return }
                
        if let audioRecorder = audioRecorder, vcDelegate.recordingCreated(audioRecorder.url) {
            
            contentView.animateClose(){[unowned self] in
                dismiss(animated: false)
            }
            
        } else {
            showErrorVC(test: "R3")
            contentView.setToIdleState()
            contentView.timeLeft = "02:00"
        }
        
        recordingStoppedAfterEnteringBackground = false
    }
    
}

extension CreateVoiceNoteRecordingViewController : AVAudioRecorderDelegate {
    
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {

        guard !recordingCancelled, !recordingStoppedAfterEnteringBackground else { return }
           
        if flag && vcDelegate.recordingCreated(recorder.url) {
            contentView.animateClose(){[unowned self] in
                dismiss(animated: false)
            }
            
        } else {
            showErrorVC(test: "R3")
            contentView.setToIdleState()
            contentView.timeLeft = "02:00"
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        //StopTimer
        showErrorVC(test: "R4")
        contentView.setToIdleState()
        contentView.timeLeft = "02:00"
    }
    
}


extension CreateVoiceNoteRecordingViewController: CreateVoiceNoteButtonDelegate {
    
    func buttonPressed(for type: CreateVoiceNoteButtonType) {
        
        switch type {
        case .action:
            actionHandler()
        case .close:
            closeHandler()
        case .allowMic:
            openSettings()
        }
    }
    
}


extension CreateVoiceNoteRecordingViewController {
    
    private func actionHandler() {
                
        if audioRecorder?.isRecording ?? false {
            stopRecording()
        } else {
            startRecording()
        }
    }
    
    private func closeHandler() {
        
        if audioRecorder?.isRecording ?? false {
            recordingCancelled = true
            audioRecorder.stop()
            audioRecorder.deleteRecording()
            contentView.setToIdleState()
            contentView.timeLeft = "02:00"
            stopTimer()
        }
        
        let handler = {[unowned self] in
            dismiss(animated: false)
        }
        
        contentView.animateClose(completionHandler: handler)
    }
    
    private func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url)
        else {
            let errorVC = UIAlertController(
                title: "Can't Open Settings",
                message: "Your settings cannot be opened. Please try again later.",
                preferredStyle: .alert)
            errorVC.addAction(UIAlertAction(title: "Close", style: .cancel){
                [unowned self]_ in
                dismiss(animated: true)
            })
            present(errorVC, animated: true)
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
}


extension CreateVoiceNoteRecordingViewController {
    
    private func checkAudioSessionBeforeRecording() -> Bool {
        
        let audioSession = AVAudioSession.sharedInstance()

        do {
            try audioSession.setCategory(.playAndRecord, mode: .default)
            try audioSession.setActive(true)
        } catch _ {
            showErrorVC(test: "R5")
            contentView.setToIdleState()
            contentView.timeLeft = "02:00"
            return false
        }
        
        return true
    }
    
    private func startRecording() {
        
        guard checkAudioSessionBeforeRecording(), let url = createAudioURL() else {
            showErrorVC(test: "R6")
            return
        }

        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 1200,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
        ]
                
        do {
            audioRecorder = try AVAudioRecorder(url: url, settings: settings)
            audioRecorder.delegate = self

            if audioRecorder.record(forDuration: maxRecordingTime) {
                startTimer()
                contentView.setToRecordingState()
            }

        } catch _ {
            showErrorVC(test: "R7")
            contentView.setToIdleState()
            contentView.timeLeft = "02:00"
            return
        }
    }
    
    private func stopRecording() {
        audioRecorder.stop()
        stopTimer()
        contentView.setToIdleState()
    }
    
    private func createAudioURL() -> URL? {
        
        var documentFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        documentFolder.appendPathComponent("audio")
        documentFolder.appendPathComponent("temp")
        
        do {
            try FileManager.default.createDirectory(at: documentFolder, withIntermediateDirectories: true)
        } catch _ {
            return nil
        }

        documentFolder.appendPathComponent("\(UUID())")
        documentFolder.appendPathExtension("m4a")
        
        return documentFolder
    }
    
    private func showErrorVC(test : String) {
        
        let errorVC = UIAlertController(
            title: "Audio Error",
            message: "An error occured when attempting to set up audio recording. Please try again later.\(test)",
            preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Close", style: .cancel){
            [unowned self]_ in
            dismiss(animated: true)
        }
        
        errorVC.addAction(closeAction)
        
        present(errorVC, animated: true)
    }
    
    private func startTimer() {
        timerStartDate = Date()
        uiTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true){
            [unowned self]_ in
            
            let secondsLeft = maxRecordingTime - (Date() - timerStartDate)
            let minutes = Int(secondsLeft/60)
            let seconds = Int(secondsLeft) % 60
            contentView.timeLeft = String(format: "%0.2d:%0.2d", minutes, seconds)
        }
    }
    
    private func stopTimer() {
        uiTimer?.invalidate()
    }
    
}
