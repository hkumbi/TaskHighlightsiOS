//
//  UserPhotoGalleryViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit
import PhotosUI

class UserPhotoGalleryViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, PHAsset>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, PHAsset>
    
    
    private let contentView = UserPhotoGalleryView()
    
    private let photoCellID = "PhotoCellIdentifier"
    private lazy var dataSource = makeDataSource()
    private lazy var cachingManager = PHCachingImageManager()
    private var isLimitedAccess : Bool = false
    private var requestImageSize : CGSize!
    
    private var selectedPHAsset : PHAsset?
    private let requestOptions = PHImageRequestOptions()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpCollectionView()
        
        contentView.buttonDelegate = self
        contentView.setViewState(.notDetermined)
        
        requestOptions.isNetworkAccessAllowed = true
        requestOptions.resizeMode = .fast

        requestImageSize = CGSize(width: UserPhotoCollectionViewCell.size.width*2, height: UserPhotoCollectionViewCell.size.height*2)
        
        requestPermission()
    }
    
    private func requestPermission() {
        
        PHPhotoLibrary.requestAuthorization(for: .readWrite) {  status in
            
            DispatchQueue.main.async { [unowned self] in
                contentView.setViewState(status)
                handleStatusChange(status)
            }
        }
    }
    
    private func handleStatusChange(_ status : PHAuthorizationStatus) {
        
        switch status {
        case .notDetermined, .restricted, .denied:
            break
            
        case .authorized:
            isLimitedAccess = false
            PHPhotoLibrary.shared().register(self)
            handleAssets()

        case .limited:
            isLimitedAccess = true
            PHPhotoLibrary.shared().register(self)
            handleAssets()

        @unknown default:
            fatalError()
        }
    }
    
    private func handleAssets() {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
        
        let fetchAssets = PHAsset.fetchAssets(with: .image, options: options)
        var assets : [PHAsset] = []
        
        fetchAssets.enumerateObjects({ asset, index, _ in
            assets.append(asset)
        })
        
        cachingManager.startCachingImages(for: assets, targetSize: requestImageSize, contentMode: .aspectFill, options: requestOptions)
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems(assets)
        
        DispatchQueue.main.async {[unowned self] in
            dataSource.apply(snap, animatingDifferences: true)
        }
    }

}


extension UserPhotoGalleryViewController {
    
    private func makeDataSource() -> DataSource {
        let source = DataSource(collectionView: contentView.collectionView){
            [unowned self] collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: photoCellID, for: indexPath) as! UserPhotoCollectionViewCell
            
            cell.identifier = item.localIdentifier
            
            cachingManager.requestImage(for: item, targetSize: requestImageSize, contentMode: .aspectFill, options: requestOptions){ image, _ in
                
                if item.localIdentifier == cell.identifier {
                    cell.image = image
                }
            }
            
            return cell
        }
        
        return source
    }
    
    private func setUpCollectionView() {
        let collection = contentView.collectionView
        
        collection.dataSource = dataSource
        collection.delegate = self
        collection.allowsSelection = true
        collection.register(UserPhotoCollectionViewCell.self, forCellWithReuseIdentifier: photoCellID)
        collection.collectionViewLayout = makeLayout()
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = UserPhotoCollectionViewCell.size
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.scrollDirection = .vertical

        return layout
    }
    
}

extension UserPhotoGalleryViewController : PHPhotoLibraryChangeObserver {
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        handleAssets()
    }
    
}

extension UserPhotoGalleryViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! UserPhotoCollectionViewCell
        let item = dataSource.itemIdentifier(for: indexPath)
        
        selectedPHAsset = item
        contentView.selectedImage = cell.image
    }
    
}


extension UserPhotoGalleryViewController : UserPhotoGalleryButtonDelegate {
    
    func buttonPressed(for type: UserPhotoGalleryButtonType) {
        
        switch type {
        case .cancel:
            print("Cancel")
            
        case .save:
            print("Save")
            
        case .preview:
            previewHandler()
            
        case .allowAccess:
            openSettings()
            
        case .manage:
            if isLimitedAccess {
                manageLimitedPermission()
            } else {
                managePermissions()
            }

        }
    }
    
}

//**************************************
//
// User Photo Gallery Button Delegate Helpers
//
//**************************************

extension UserPhotoGalleryViewController {
    
    private func manageLimitedPermission() {
        
        let actionSheet = UIAlertController(
            title: "Manage Access",
            message: "Select more photos or go to settings and allow access to all photos",
            preferredStyle: .alert)
        
        let limitedAction = UIAlertAction(
            title: "Select More Photos",
            style: .default) { [unowned self]_ in
            PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self)
        }
        actionSheet.addAction(limitedAction)
        
        let manageAction = UIAlertAction(
            title: "Allow access to all photos",
            style: .default) { [unowned self]_ in
            openSettings()
        }
        actionSheet.addAction(manageAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    private func managePermissions() {
        let actionSheet = UIAlertController(
            title: "Manage Access",
            message: "Go to settings to limit access to photos",
            preferredStyle: .alert)
        
        let manageAction = UIAlertAction(
            title: "Limit Access",
            style: .destructive) { [unowned self]_ in
            openSettings()
        }
        actionSheet.addAction(manageAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        actionSheet.addAction(cancelAction)
        
        present(actionSheet, animated: true)
    }
    
    private func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url)
        else { return }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    private func previewHandler() {
        
        let targetSize = CGSize(width: UIScreen.main.bounds.width*2, height: UIScreen.main.bounds.height*2)
        
        guard let selectedPHAsset = selectedPHAsset else { return }
        
        cachingManager.requestImage(for: selectedPHAsset, targetSize: targetSize, contentMode: .aspectFit, options: requestOptions){
            [unowned self] image,_ in
            
            print(image)
        }
    }
    
}
