//
//  CreateVoiceNoteTitleViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-20.
//

import UIKit
import AVFAudio

class CreateVoiceNoteTitleViewController: UIViewController {
    
    private let contentView = CreateVoiceNoteTitleView()
    
    private var selectedDate = Date()
    
    private var uiTimer : Timer?
    private var audioPlayer : AVAudioPlayer!
    private var recordingFile : URL!
    private var audioWasPlaying : Bool = false
    
    unowned var coordinatorDelegate : CreateVoiceNotesTitleCoordinatorDelegate!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.textViewDelegate = self
        contentView.buttonDelegate = self
        
        contentView.progress = 0
        contentView.time = "00:00"
        contentView.saveIsEnabled = false
        contentView.isMaxCharLabelHidden = true
        
        formatDate(selectedDate)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActiveBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActiveForeground), name: UIApplication.didBecomeActiveNotification, object: nil)

    }
    
    func setAudioURL(_ url : URL) -> Bool {
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.numberOfLoops = -1
            recordingFile = url
        } catch _ {
            return false
        }
        
        return true
    }

    private func formatDate(_ date : Date) {
        
        if Calendar.current.isDateInToday(date) {
            contentView.date = "Today"
        } else if Calendar.current.isDateInTomorrow(date) {
            contentView.date = "Tomorrow"
        } else if Calendar.current.isDateInYesterday(date) {
            contentView.date = "Yesterday"
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM dd, yyyy"
            contentView.date = formatter.string(from: date)
        }
    }
    
    private func clearTempFolder() {

        var tempFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        tempFolder.appendPathComponent("audio")
        tempFolder.appendPathComponent("temp")

        do {
            let files = try FileManager.default.contentsOfDirectory(at: tempFolder, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            for file in files {
                try FileManager.default.removeItem(at: file)
            }
        } catch _ {
            return
        }
    }
    
    @objc private func willResignActiveBackground() {
        
        if audioPlayer.isPlaying {
            audioWasPlaying = true
            audioPlayer.pause()
        }
    }

    @objc private func didBecomeActiveForeground() {
        
        if audioWasPlaying {
            audioPlayer.play()
            audioWasPlaying = false
        }
    }
    
}

extension CreateVoiceNoteTitleViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        contentView.isMaxCharLabelHidden = newText.count <= 100
        return newText.count <= 100
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let isTextEmpty = textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        contentView.saveIsEnabled = !isTextEmpty
        contentView.titlePlaceholderIsHidden = !textView.text.isEmpty
    }
    
}

extension CreateVoiceNoteTitleViewController : CreateVoiceNoteTitleButtonDelegate {
    
    func buttonPressed(for type: CreateVoiceNoteTitleButtonType) {
                
        switch type {
        case .back:
            view.endEditing(true)
            coordinatorDelegate.popCreateVoiceNoteTitleVC()

        case .date:
            view.endEditing(true)
            openCalendar()

        case .audioControl:
            if audioPlayer.isPlaying {
                audioPlayer.pause()
                contentView.audioControlIcon = UIImage(systemName: "play")
                stopTimer()
            } else {
                audioPlayer.play()
                contentView.audioControlIcon = UIImage(systemName: "pause")
                startTimer()
            }
            
        case .save:
            view.endEditing(true)

            if let newFile = createAudioFile() {
                moveFile(to: newFile)
                coordinatorDelegate.voiceNoteSaved(newFile)
            }
        }
    }
    
}

extension CreateVoiceNoteTitleViewController {
    
    func openCalendar() {
        
        let calendarVC = CalendarModalViewController()
        calendarVC.modalPresentationStyle = .overCurrentContext
        calendarVC.vcDelegate = self
        calendarVC.setStartingDate(selectedDate)
        present(calendarVC, animated: false)
        
        if audioPlayer.isPlaying {
            audioWasPlaying = true
            audioPlayer.pause()
        }
    }
    
    func startTimer()  {
        uiTimer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true){
            [unowned self]_ in
            let minutes = Int(audioPlayer.currentTime/60)
            let seconds = Int(audioPlayer.currentTime) % 60
            
            DispatchQueue.main.async {[unowned self] in
                contentView.progress = audioPlayer.currentTime/audioPlayer.duration
                contentView.time = String(format: "%0.2d:%0.2d", minutes, seconds)
            }
        }
    }
    
    func stopTimer() {
        uiTimer?.invalidate()
        uiTimer = nil
    }
    
    private func createAudioFile() -> URL? {
        
        var folder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        folder.appendPathComponent("audio")
        
        do {
            try FileManager.default.createDirectory(at: folder, withIntermediateDirectories: true)
        } catch _ {
            return nil
        }
        
        folder.appendPathComponent("\(UUID())")
        folder.appendPathExtension("m4a")
        
        return folder
    }
    
    private func moveFile(to url : URL) {
                
        do {
            try FileManager.default.moveItem(at: recordingFile, to: url)
        } catch _ {
            return
        }
    }
    
}


extension CreateVoiceNoteTitleViewController : CalendarVCDelegate {
    
    func selectedDateChanged(_ date: Date) {
        selectedDate = date
        formatDate(date)
    }
    
    func vcClosed() {
        if audioWasPlaying {
            audioPlayer.play()
            audioWasPlaying = false
        }
    }

}

