//
//  CreateTaskViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-16.
//

import UIKit

enum TaskImportance : Int {
    case low = 0, medium = 1, high = 2
}

class CreateTaskViewController: UIViewController {
    
    private let contentView = CreateTaskView()
    
    unowned var coordinatorDelegate : CreateTaskCoordinatorDelegate!
    
    private var selectedDate : Date! {
        didSet { formatDate() }
    }

    private var titleIsValid : Bool = false {
        didSet { verifyInputs() }
    }
    
    private var textIsValid : Bool = false {
        didSet { verifyInputs() }
    }
    
    private var importance : TaskImportance = .low {
        didSet { updateImportance() }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.textFieldDelegate = self
        contentView.textViewDelegate = self
        contentView.buttonDelegate = self
        
        verifyInputs()
        
        selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        importance = .low
    }
    
    private func formatDate() {
        
        if Calendar.current.isDateInTomorrow(selectedDate) {
            contentView.date = "Tomorrow"
        } else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM dd, yyyy"
            contentView.date = dateFormatter.string(from: selectedDate)
        }
    }
    
    private func updateImportance() {
        
        switch importance {
        case .low:
            contentView.importanceText = "Low"
            contentView.importanceIconColor = .lightGray

        case .medium:
            contentView.importanceText = "Medium"
            contentView.importanceIconColor = .systemYellow

        case .high:
            contentView.importanceText = "High"
            contentView.importanceIconColor = .red
        }
    }
    
    private func verifyInputs() {
        contentView.saveIsEnabled = titleIsValid && textIsValid
    }
    
}

extension CreateTaskViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let newTextIsEmpty = newText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty

        titleIsValid = !newTextIsEmpty
        contentView.titleMaxCharIsHidden = newText.count < 80

        return newText.count <= 80
    }
    
}

extension CreateTaskViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let newTextIsEmpty = newText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        textIsValid = !newTextIsEmpty
        contentView.textMaxCharIsHidden = newText.count < 2000

        return newText.count <= 2000
    }
    
    func textViewDidChange(_ textView: UITextView) {
        contentView.textViewPlaceholderIsHidden = !textView.text.isEmpty
    }
    
}

extension CreateTaskViewController : CreateTaskButtonDelegate {
    
    func buttonPressed(for type: CreateTaskButtonType) {
        
        view.endEditing(true)
        
        switch type {
        case .back:
            coordinatorDelegate.goBackFromCreateTaskVC()
            
        case .date:
            let calendarVC = CalendarModalViewController()
            calendarVC.modalPresentationStyle = .overCurrentContext
            calendarVC.vcDelegate = self
            calendarVC.setStartingDate(selectedDate)
            present(calendarVC, animated: false)
            
        case .importance:
            let importanceVC = CreateTaskImportanceModalViewController()
            importanceVC.modalPresentationStyle = .overCurrentContext
            importanceVC.vcDelegate = self
            importanceVC.importance = importance
            present(importanceVC, animated: false)
            
        case .save:
            coordinatorDelegate.goBackFromCreateTaskVC()
        }
    }
    
}


extension CreateTaskViewController : CalendarVCDelegate {
    
    func selectedDateChanged(_ date: Date) {
        selectedDate = date
    }
    
    func vcClosed() {
        
    }
    
}


extension CreateTaskViewController : CreateTaskImportanceVCDelegate {
    
    func importanceChanged(to importance: TaskImportance) {
        
        self.importance = importance
    }
    
}
