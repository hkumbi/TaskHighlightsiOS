//
//  PhotoPreviewViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit

class PhotoPreviewViewController: UIViewController {

    private let contentView = PhotoPreviewView()
    
    var image : UIImage? {
        get { contentView.image }
        set { contentView.image = newValue }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .black
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        contentView.image = UIImage(named: "forest")
    }

}

extension PhotoPreviewViewController : PhotoPreviewButtonDelegate {
    
    func buttonPressed(for type: PhotoPreviewButtonType) {
        
        switch type {
        case .back:
            print("Back")
        case .save:
            print("Next")
        }
    }
    
}
