//
//  CreateModalViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import UIKit

class CreateModalViewController: UIViewController {
    
    private let contentView = CreateView()
    
    unowned var vcDelegate : CreateVCDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalBottomConstant = contentView.modalView.heightConstant
        contentView.animateOpen()
    }
    

}

extension CreateModalViewController : CreateButtonDelegate {
    
    func buttonPressed(for type: CreateButtonType) {
        
        switch type {
        case .photoHighlight:
            contentView.animateClose {[unowned self] in
                dismiss(animated: false)
                vcDelegate.createContent(for: .highlights)
            }
            
            
        case .voiceNote:
            contentView.animateClose {[unowned self] in
                dismiss(animated: false)
                vcDelegate.createContent(for: .voiceNotes)
            }
            
        case .task:
            contentView.animateClose {[unowned self] in
                dismiss(animated: false)
                vcDelegate.createContent(for: .task)
            }
            
        case .cancel:
            contentView.animateClose {[unowned self] in
                dismiss(animated: false)
            }
        }
    }
    
}

extension CreateModalViewController {
    
    
    
}



