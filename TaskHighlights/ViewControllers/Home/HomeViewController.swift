//
//  HomeViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class HomeViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, CustomDateModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, CustomDateModel>
    
    private let contentView = HomeView()
    
    private lazy var dataSource = makeDataSource()
    
    private let calendar = Calendar.current
    private let calendarHelper = CalendarHelper()
    private let dateCellID = "DateCellIdentifier"
    private var selectedDate = Date()
    
    unowned var coordinatorDelegate : HomeCoordinatorDelegate!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        configureCollectionView()
        
        setUpCollectionViewData()
    }
    
    private func configureCollectionView() {
        
        let collection = contentView.collectionView
        collection.showsHorizontalScrollIndicator = false
        collection.dataSource = dataSource
        collection.collectionViewLayout = makeLayout()
        collection.register(HomeDateCollectionViewCell.self, forCellWithReuseIdentifier: dateCellID)
    }
    
    private func makeLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal
        layout.itemSize = HomeDateCollectionViewCell.cellSize
        layout.minimumLineSpacing = 20
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        
        return layout
    }
    
    private func makeDataSource() -> DataSource {
        let source = DataSource(collectionView: contentView.collectionView) {
            [unowned self]collection, indexPath, item in
            
            let cell = collection.dequeueReusableCell(withReuseIdentifier: dateCellID, for: indexPath) as! HomeDateCollectionViewCell
                        
            cell.day = "\(item.day)"
            cell.weekday = item.weekday
            
            return cell
        }
        
        return source
    }
    
    private func setUpCollectionViewData() {
        
        let daysRange = calendar.range(of: .day, in: .weekOfMonth, for: selectedDate)!
        let daysInWeek = Array(daysRange.lowerBound..<daysRange.upperBound)
        let component = calendar.dateComponents([.year, .month, .day, .weekday], from: selectedDate)
        let positionInValidDays = daysInWeek.firstIndex(of: component.day!)!
        let zeroIndexWeekday = component.weekday!-1
        let firstWeekdayIndex = zeroIndexWeekday - positionInValidDays
                
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        contentView.monthAndYearText = formatter.string(from: selectedDate)
        
        var weekdayIndex = firstWeekdayIndex
        var snap = Snap()
        var items : [CustomDateModel] = []
        
        snap.appendSections([.main])
        
        for day in daysInWeek {
            
            let weekdaySymbol = calendar.shortWeekdaySymbols[weekdayIndex]
            weekdayIndex = weekdayIndex + 1

            items.append(CustomDateModel(day: day, weekday: weekdaySymbol))
        }
        
        snap.appendItems(items)
        dataSource.apply(snap)
        
        let selectedDayItem = items.first(where: { $0.day == component.day! })!
        let indexPath = dataSource.indexPath(for: selectedDayItem)
        
        contentView.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
    }
    
}

extension HomeViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = dataSource.itemIdentifier(for: indexPath)!
        selectedDate = calendarHelper.getSelectedDate(item.day)
    }
    
}


extension HomeViewController : HomeButtonDelegate {
    
    func buttonPressed(for type: HomeButtonType) {
        
        switch type {
        case .weather:
            let weatherModalVC = WeatherModalViewController()
            weatherModalVC.modalPresentationStyle = .overCurrentContext
            present(weatherModalVC, animated: false)
            
        case .settings:
            print("Settings")
            
        case .viewCalendar:
            let calendarVC = CalendarModalViewController()
            calendarVC.modalPresentationStyle = .overCurrentContext
            calendarVC.vcDelegate = self
            calendarVC.setStartingDate(selectedDate)
            present(calendarVC, animated: false)
            
        case .create:
            let createVC = CreateModalViewController()
            createVC.modalPresentationStyle = .overCurrentContext
            createVC.vcDelegate = self
            present(createVC, animated: false)

        case .highlightsSection:
            print("Highlights Section")

        case .remindersSection:
            print("Reminders Section")

        case .tasksSection:
            print("Tasks Section")
        }
    }
    
    
}


extension HomeViewController : CalendarVCDelegate {
    
    func selectedDateChanged(_ date: Date) {
        selectedDate = date
        setUpCollectionViewData()
    }
    
    func vcClosed() {
        
    }
    
}


extension HomeViewController : CreateVCDelegate {
    
    func createContent(for type: CreateContentType) {
        
        switch type {
        case .highlights:
            coordinatorDelegate.createContent(for: .camera)
        case .task:
            coordinatorDelegate.createContent(for: .task)
        case .voiceNotes:
            let voiceNotesVC = CreateVoiceNoteRecordingViewController()
            voiceNotesVC.modalPresentationStyle = .overCurrentContext
            voiceNotesVC.vcDelegate = self
            present(voiceNotesVC, animated: false)
        }
    }
    
}


extension HomeViewController : CreateVoiceNoteRecordingVCDelegate {
    
    func recordingCreated(_ url: URL) -> Bool {
        coordinatorDelegate.voiceNoteCreated(url)
    }
    
}
