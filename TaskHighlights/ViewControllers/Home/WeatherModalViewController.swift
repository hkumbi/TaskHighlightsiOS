//
//  WeatherModalViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-25.
//

import UIKit

class WeatherModalViewController: UIViewController {
    
    private let contentView = WeatherView()
    
    private var closeHeight : CGFloat = 0
    private let nightBlue = UIColor(red: .zero, green: 11/255, blue: 49/255, alpha: 1)
    private let dayBlue = UIColor(red: 80/255, green: 137/255, blue: 198/255, alpha: 1)


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalView.buttonDelegate = self
        
        closeHeight = contentView.modalView.height/3
        contentView.modalView.backgroundColor = nightBlue
        
        setUpPanGesture()
        contentView.modalView.setPrecipitationVisibility(false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalBottomConstant = contentView.modalView.height
        animateOpen()
    }

    private func setUpPanGesture() {
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        contentView.modalView.addGestureRecognizer(pan)
    }
    
    @objc private func handlePan(_ pan : UIPanGestureRecognizer) {
        
        let transY = pan.translation(in: view).y
        let veloY = pan.velocity(in: view).y
        
        switch pan.state {
        case .changed:
            if transY >= 0 {
                let multiplier = transY/contentView.modalView.height
                contentView.dimmingAlpha = WeatherView.maxDimmingAlpha - multiplier*WeatherView.maxDimmingAlpha
                contentView.modalBottomConstant = transY
            }
            
            
        case .ended, .cancelled, .failed:
            if closeHeight <= transY || veloY >= 500 {
                animateClose()
            } else {
                animateOpen()
            }
            
        default:
            break
        }
    }
}

extension WeatherModalViewController : WeatherButtonDelegate {
    
    func closeButtonPressed() {
        animateClose()
    }
    
}


extension WeatherModalViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = 0
            contentView.dimmingAlpha = WeatherView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalBottomConstant = contentView.modalView.height
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
}
