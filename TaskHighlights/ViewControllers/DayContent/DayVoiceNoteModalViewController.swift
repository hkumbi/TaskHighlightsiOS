//
//  DayVoiceNoteModalViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import UIKit

class DayVoiceNoteModalViewController: UIViewController {
    
    private let contentView = DayVoiceNoteModalView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        contentView.modalContentView.buttonDelegate = self
        
        contentView.modalContentView.title = "Hello"
        contentView.modalContentView.progress = 0.2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.modalContentView.bottomSafeAreaConstant = view.safeAreaInsets.bottom
        contentView.modalContentBottomConstant = contentView.modalContentView.height
        
        animateOpen()
    }

}


extension DayVoiceNoteModalViewController : DayVoiceNoteModalButtonDelegate {
    
    func buttonPressed(for type: DayVoiceNoteModalButtonType) {
        
        switch type {
        case .delete:
            print("Delete")
        case .audioAction:
            print("Audio Action")
        case .close:
            animateClose()
        }
    }
    
}


extension DayVoiceNoteModalViewController {
    
    private func animateOpen() {
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalContentBottomConstant = 0
            contentView.dimmingAlpha = DayVoiceNoteModalView.maxDimmingAlpha
            view.layoutIfNeeded()
        }
    }
    
    private func animateClose() {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            contentView.modalContentBottomConstant = contentView.modalContentView.height
            contentView.dimmingAlpha = 0
            view.layoutIfNeeded()
        } completion: { [unowned self]_ in
            dismiss(animated: false)
        }
    }
    
}
