//
//  DayVoiceNotesViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DayVoiceNotesViewController: UIViewController {
    
    private typealias DataSource = UITableViewDiffableDataSource<SectionModel, VoiceNotesModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, VoiceNotesModel>
    
    private let contentView = DayVoiceNotesView()
    
    private lazy var dataSource = makeDataSource()
    private let voiceNoteID = "VoiceNotesIdentifier"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        setUpTableView()
        makeData()
    }
    
    private func makeData(){
        
        var snap = Snap()
        
        snap.appendSections([.main])
        snap.appendItems([VoiceNotesModel(), VoiceNotesModel(), VoiceNotesModel()])
        
        dataSource.apply(snap)
    }

}


extension DayVoiceNotesViewController {
    
    private func setUpTableView() {
        
        let tableView = contentView.tableView
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.rowHeight = DayVoiceNoteTableViewCell.height
        tableView.allowsSelection = false
        tableView.register(DayVoiceNoteTableViewCell.self, forCellReuseIdentifier: voiceNoteID)
    }
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: voiceNoteID, for: indexPath) as! DayVoiceNoteTableViewCell
            
            cell.id = item.id
            cell.buttonDelegate = self
            cell.progress = 0.3
            
            return cell
        }
        
        return source
    }
    
}

extension DayVoiceNotesViewController : UITableViewDelegate {
    
}


extension DayVoiceNotesViewController : DayVoiceNoteCellButtonDelegate {
    
    func cellPressed(_ id: UUID) {
        let voiceNoteVC = DayVoiceNoteModalViewController()
        voiceNoteVC.modalPresentationStyle = .overCurrentContext
        present(voiceNoteVC, animated: false)
    }
    
    func audioActionButtonPressed(_ id: UUID) {
        print("Audio Action Pressed")
    }
    
}
