//
//  DayHighlightsViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DayHighlightsViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<SectionModel, HighlightModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, HighlightModel>
    
    override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
    
    private let contentView = DayHighlightContentView()
    
    private lazy var imageDataSource = makeDataSource(for: contentView.imageCollectionView)
    private lazy var previewDataSource = makeDataSource(for: contentView.previewCollectionView)
    private let highlightCellID = "HighlightCellIdentifier"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        contentView.buttonDelegate = self
        
        setUpCollectionView(for: contentView.imageCollectionView)
        setUpCollectionView(for: contentView.previewCollectionView)
        
        contentView.imageCollectionView.dataSource = imageDataSource
        contentView.previewCollectionView.dataSource = previewDataSource
        contentView.previewCollectionView.allowsSelection = true
        
        contentView.imageCollectionView.collectionViewLayout = makeImageLayout()
        contentView.previewCollectionView.collectionViewLayout = makePreviewLayout()
        
        makeData()
    }
    
    private func makeData() {
        var snap = Snap()
        
        snap.appendSections([.main])
        snap.appendItems([HighlightModel(), HighlightModel(), HighlightModel()])
        
        imageDataSource.apply(snap)
        previewDataSource.apply(snap)
        
        contentView.previewCollectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .left)
    }

}


extension DayHighlightsViewController {
    
    private func setUpCollectionView(for collection : UICollectionView) {
                
        collection.delegate = self
        collection.register(DayHighlightCollectionViewCell.self, forCellWithReuseIdentifier: highlightCellID)
    }
    
    private func makeImageLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = DayHighlightCollectionViewCell.cellSize
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        return layout
    }
    
    private func makePreviewLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = DayHighlightCollectionViewCell.previewCellSize
        layout.minimumLineSpacing = DayHighlightCollectionViewCell.spacing
        layout.scrollDirection = .horizontal
        return layout
    }
    
    private func makeDataSource(for collection : UICollectionView) -> DataSource {
        
        let source = DataSource(collectionView: collection){
            [unowned self]collectionView, indexPath, item in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: highlightCellID, for: indexPath) as! DayHighlightCollectionViewCell
            
            cell.isPreviewCell = collection == contentView.previewCollectionView
            
            return cell
        }
        
        return source
    }
    
}


extension DayHighlightsViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            selectPreviewCellAfterScroll(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        selectPreviewCellAfterScroll(scrollView)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        contentView.imageCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        guard collectionView == contentView.previewCollectionView else { return .zero }
        
        let itemsCount = CGFloat(previewDataSource.snapshot().numberOfItems)
        let cellsWidth = itemsCount*DayHighlightCollectionViewCell.previewCellSize.width
        let spacing = DayHighlightCollectionViewCell.spacing*(max(itemsCount-1, 0))
        let contentWidth = cellsWidth+spacing
        let inset = max(UIScreen.main.bounds.width/2 - contentWidth/2, 0)

        return UIEdgeInsets(top: 0, left: inset, bottom: 0, right: 0)
    }
    
    private func selectPreviewCellAfterScroll(_ scrollView : UIScrollView) {
        
        for cell in contentView.imageCollectionView.visibleCells {
            
            let cellScreenX = cell.frame.maxX - scrollView.contentOffset.x

            if cellScreenX / UIScreen.main.bounds.width >= 0.95 {
                
                let i = contentView.imageCollectionView.indexPath(for: cell)
                
                contentView.previewCollectionView.selectItem(at: i, animated: false, scrollPosition: .centeredHorizontally)
                break
            }
        }
    }
    
}


extension DayHighlightsViewController : DayContentButtonDelegate {
    
    func backButtonPressed() {
        print("Back")
    }
    
}

