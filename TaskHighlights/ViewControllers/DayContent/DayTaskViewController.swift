//
//  DayTaskViewController.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import UIKit

class DayTaskViewController: UIViewController {
    
    private typealias DataSource = UITableViewDiffableDataSource<SectionModel, TaskModel>
    private typealias Snap = NSDiffableDataSourceSnapshot<SectionModel, TaskModel>
    
    private let contentView = DayTaskView()
    
    private lazy var dataSource = makeDataSource()
    private let taskCellID = "TaskCellIdentifier"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        view.addSubview(contentView)
        
        formatDay(Date())
        
        setUpTableView()
        makeData()
        
        print("Rows: \(contentView.tableView.numberOfRows(inSection: 0))")
    }
    

    private func formatDay(_ date : Date) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        contentView.date = formatter.string(from: date)
    }
    
    private func makeData() {
        
        var snap = Snap()
        snap.appendSections([.main])
        snap.appendItems([TaskModel(), TaskModel(), TaskModel(), TaskModel()])
        snap.appendItems([TaskModel(), TaskModel(), TaskModel(), TaskModel()])
        snap.appendItems([TaskModel(), TaskModel(), TaskModel(), TaskModel()])
        dataSource.apply(snap)
    }

}


extension DayTaskViewController {
    
    private func setUpTableView() {
        let tableView = contentView.tableView
        
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.allowsSelection = false
        tableView.rowHeight = DayTaskTableViewCell.height
        tableView.register(DayTaskTableViewCell.self, forCellReuseIdentifier: taskCellID)
    }
    
    
    private func makeDataSource() -> DataSource {
        
        let source = DataSource(tableView: contentView.tableView){
            [unowned self]tableView, indexPath, item in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: taskCellID, for: indexPath) as! DayTaskTableViewCell
            
            cell.isLastRow = indexPath.item == tableView.numberOfRows(inSection: 0) - 1
            
            return cell
        }
        
        return source
    }
    
}


extension DayTaskViewController : UITableViewDelegate {

    
    
}


