//
//  CameraSessionHelper.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-18.
//

import Foundation
import AVFoundation


class CameraSessionHelper {
    
    private let session = AVCaptureSession()
    
    private let frontCameraDiscovery : AVCaptureDevice.DiscoverySession
    private let backCameraDiscovery : AVCaptureDevice.DiscoverySession
    
    private let sessionQueue = DispatchQueue(label: "SessionQueue")
    
    private let photoOutput = AVCapturePhotoOutput()
    private var photoCaptureDelegate : PhotoCapture!
        
    private var currentVideoDevice : AVCaptureDeviceInput! {
        didSet { currentVideoDeviceChanged() }
    }
    
    var sessionIsRunning : Bool {
        get { session.isRunning }
    }
    
    unowned var configDelegate : CameraConfigActionDelegate!
    weak var photoActionDelegate : PhotoCaptureActionDelegate?
    

    init() {
        
        frontCameraDiscovery = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera],
            mediaType: .video,
            position: .front)
        
        backCameraDiscovery = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera],
            mediaType: .video,
            position: .back)
    }
    
    
    private func currentVideoDeviceChanged() {
        
        let device = currentVideoDevice.device
        
        do {
            try device.lockForConfiguration()
            
            if device.isExposureModeSupported(.continuousAutoExposure) {
                device.exposureMode = .continuousAutoExposure
            }
            
            if device.isFocusModeSupported(.continuousAutoFocus) {
                device.focusMode = .continuousAutoFocus
            }
            
            device.unlockForConfiguration()
        } catch {
            configDelegate.cameraConfigError()
            return
        }
    }
}


extension CameraSessionHelper {
    
    func startSession() {
        
        if session.isRunning {
            sessionQueue.async {[weak self] in
                self?.session.startRunning()
            }
        }
    }
    
    func stopSession() {
        session.stopRunning()
    }
    
    func setUpPreview(layer : AVCaptureVideoPreviewLayer) {
        layer.session = session
    }
    
    private func prepareTheSession() {
        
        session.beginConfiguration()
        
        let backCameraDevice = backCameraDiscovery.devices.first

        photoOutput.isHighResolutionCaptureEnabled = true
        
        guard
            let deviceVideoInput = try? AVCaptureDeviceInput(device: backCameraDevice!),
            session.canAddInput(deviceVideoInput),
            session.canAddOutput(photoOutput)
        else {
            configDelegate.cameraConfigError()
            return
        }
        
        session.addInput(deviceVideoInput)
        session.addOutput(photoOutput)
        
        session.sessionPreset = .hd1920x1080
        
        session.commitConfiguration()
        
        // Session Configuration is ready
        configDelegate.sessionConfigSuccess()
        session.startRunning()
        
        currentVideoDevice = deviceVideoInput
    }
}


extension CameraSessionHelper {
    
    func getPermission() {
        
        let cameraStatus = AVCaptureDevice.authorizationStatus(for: .video)
        handlePermissionStatus(cameraStatus)
    }
    
    private func handlePermissionStatus(_ status : AVAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            requestPermission()
            
        case .restricted:
            configDelegate.cameraRestricted()
            
        case .denied:
            configDelegate.cameraDenied()
            
        case .authorized:
            prepareTheSession()
            configDelegate.cameraAuthorized()
            
        @unknown default:
            configDelegate.cameraConfigError()
        }
    }
    
    private func requestPermission() {
        
        AVCaptureDevice.requestAccess(for: .video){[unowned self]_ in
            
            let status = AVCaptureDevice.authorizationStatus(for: .video)
            handlePermissionStatus(status)
        }
    }
    
}


extension CameraSessionHelper {
    
    func switchCamera() {
        
        sessionQueue.async { [weak self] in
            
            self?.switchCameraHandler()
        }
    }
    
    private func switchCameraHandler() {
        
        var newDevice : AVCaptureDevice?
        
        switch currentVideoDevice.device.position {
        case .front, .unspecified:
            newDevice = backCameraDiscovery.devices.first
        case .back:
            newDevice = frontCameraDiscovery.devices.first
        @unknown default:
            configDelegate.cameraConfigError()
        }
        
        session.beginConfiguration()
        
        session.removeInput(currentVideoDevice)
        
        let newDeviceInput = try? AVCaptureDeviceInput(device: newDevice!)
        
        if let newDeviceInput = newDeviceInput, session.canAddInput(newDeviceInput) {
            session.addInput(newDeviceInput)
            currentVideoDevice = newDeviceInput
        } else {
            session.addInput(currentVideoDevice)
        }
        
        session.commitConfiguration()
        
        // New device switched
        configDelegate.cameraDevicesSwitched()
    }
    
}


extension CameraSessionHelper {
    
    func takePhoto(_ orientation : AVCaptureVideoOrientation) {
        
        sessionQueue.async {[unowned self] in
            takePhotoHandler(orientation)
        }
    }
    
    private func takePhotoHandler(_ orientation : AVCaptureVideoOrientation) {
        
        if let photoConnection = photoOutput.connection(with: .video) {
            
            let isFrontCamera = currentVideoDevice.device.position == .front
            photoConnection.videoOrientation = orientation
            photoConnection.isVideoMirrored = isFrontCamera
        }
        
        var photoSettings : AVCapturePhotoSettings
        
        if photoOutput.availablePhotoCodecTypes.contains(.hevc) {
            photoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.hevc])
        } else {
            photoSettings = AVCapturePhotoSettings()
        }
        
        photoSettings.flashMode = .off
        photoSettings.isHighResolutionPhotoEnabled = true
        photoSettings.photoQualityPrioritization = photoOutput.maxPhotoQualityPrioritization
        
        if let pixelFormat = photoSettings.availablePreviewPhotoPixelFormatTypes.first {
            photoSettings.previewPhotoFormat = [
                kCVPixelBufferPixelFormatTypeKey : pixelFormat,
                kCVPixelBufferWidthKey : 1024,
                kCVPixelBufferHeightKey : 1024,
            ] as [String: Any]
        }
        
        photoSettings.embeddedThumbnailPhotoFormat = [
            AVVideoCodecKey : AVVideoCodecType.jpeg,
            AVVideoHeightKey : 1024,
            AVVideoWidthKey : 1024
        ]
        
        photoCaptureDelegate = PhotoCapture()
        photoCaptureDelegate.actionDelegate = photoActionDelegate
        photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureDelegate)
    }
    
}
