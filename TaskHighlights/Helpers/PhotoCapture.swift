//
//  PhotoCapture.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation
import AVFoundation
import UIKit


class PhotoCapture: NSObject, AVCapturePhotoCaptureDelegate {
    
    weak var actionDelegate : PhotoCaptureActionDelegate?
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard
            error == nil,
            let data = photo.fileDataRepresentation(),
            let image = UIImage(data: data)
        else {
            actionDelegate?.photoCaptureError()
            return
        }
        
        actionDelegate?.photoCaptured(image)
    }
    
}
