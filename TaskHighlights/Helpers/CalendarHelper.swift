//
//  CalendarHelper.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import Foundation


class CalendarHelper {
    
    private let calendar = Calendar.current
    private var selectedMonthDate : Date!
    
    
    init() {
        
        self.setSelectedMonth(Date())
    }
    
    
    func setSelectedMonth(_ date : Date) {
        
        let components = calendar.dateComponents([.year, .month], from: date)
        selectedMonthDate = calendar.date(from: components)!
    }
    
    func goToNextMonth() {
        selectedMonthDate = calendar.date(byAdding: .month, value: 1, to: selectedMonthDate)!
    }
    
    func goToPreviousMonth() {
        selectedMonthDate = calendar.date(byAdding: .month, value: -1, to: selectedMonthDate)!
    }
    
    func getMonthAndYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter.string(from: selectedMonthDate)
    }
    
    /*
    func refreshCurrentMonthDate() {
        
        let components = calendar.dateComponents([.year, .month], from: Date())
        selectedMonthDate = calendar.date(from: components)!
    }
    */
    func getSelectedDate(_ day : Int) -> Date {
        
        let zeroIndexDay = day - 1
        let date = calendar.date(byAdding: .day, value: zeroIndexDay, to: selectedMonthDate)!
        return date
    }
    
    func isDateInSelectedMonth(_ date : Date) -> Int? {
        
        let isSameYear = calendar.isDate(date, equalTo: selectedMonthDate, toGranularity: .year)
        let isSameMonth = calendar.isDate(date, equalTo: selectedMonthDate, toGranularity: .month)
        
        guard isSameYear, isSameMonth else { return nil }
        
        return calendar.component(.day, from: date)
    }
    
    func getDaysInSelectedMonth() -> Int {
        
        let daysInMonth = Calendar.current.range(of: .day, in: .month, for: selectedMonthDate)!
        return daysInMonth.count
    }
    
    func getFirstDayOfMonth() -> Int {
                
        let components = Calendar.current.dateComponents([.year, .month], from: selectedMonthDate)
        let firstOfMonthDay = Calendar.current.date(from: components)!
        let daysInFirstWeekOfMonth = Calendar.current.range(of: .day, in: .weekOfMonth, for: firstOfMonthDay)!
        let daysInWeek : Int = 8 // In calendar days start at 1
        
        return daysInWeek - daysInFirstWeekOfMonth.count
    }
    
}
