//
//  ViewAllButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import Foundation


protocol ViewAllButtonDelegate : AnyObject {
    
    func backButtonPressed()
}
