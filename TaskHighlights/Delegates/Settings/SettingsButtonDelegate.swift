//
//  SettingsButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import Foundation


protocol SettingsButtonDelegate : AnyObject {
    
    func buttonPressed(for type : SettingsButtonType)
    func tempUnitChanged(_ value : Int)
}

enum SettingsButtonType {
    case back, highlights, voiceNotes, faq, deleteMyData
}
