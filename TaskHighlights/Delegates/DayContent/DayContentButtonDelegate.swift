//
//  DayContentButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import Foundation


protocol DayContentButtonDelegate : AnyObject {
    
    func backButtonPressed()
}
