//
//  DayVoiceNoteModalButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import Foundation

protocol DayVoiceNoteModalButtonDelegate : AnyObject {
    
    func buttonPressed(for type : DayVoiceNoteModalButtonType)
}


enum DayVoiceNoteModalButtonType {
    case delete, audioAction, close
}
