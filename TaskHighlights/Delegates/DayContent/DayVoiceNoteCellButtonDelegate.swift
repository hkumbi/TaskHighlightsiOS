//
//  DayVoiceNoteCellButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import Foundation


protocol DayVoiceNoteCellButtonDelegate : AnyObject {
    
    func cellPressed(_ id : UUID)
    func audioActionButtonPressed(_ id : UUID)
}
