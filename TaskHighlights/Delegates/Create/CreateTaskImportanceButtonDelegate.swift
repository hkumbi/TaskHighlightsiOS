//
//  CreateTaskImportanceButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import Foundation


protocol CreateTaskImportanceButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateTaskImportanceButtonType)
}


enum CreateTaskImportanceButtonType {
    case cancel, save
}
