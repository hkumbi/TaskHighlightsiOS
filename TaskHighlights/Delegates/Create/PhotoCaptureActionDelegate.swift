//
//  PhotoCaptureActionDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation
import UIKit


protocol PhotoCaptureActionDelegate : AnyObject {
    
    func photoCaptureError()
    func photoCaptured(_ image : UIImage)
}
