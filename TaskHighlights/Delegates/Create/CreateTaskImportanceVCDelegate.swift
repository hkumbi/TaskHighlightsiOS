//
//  CreateTaskImportanceVCDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import Foundation


protocol CreateTaskImportanceVCDelegate : AnyObject {
    
    func importanceChanged(to importance : TaskImportance)
}
