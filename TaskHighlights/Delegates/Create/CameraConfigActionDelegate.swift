//
//  CameraConfigActionDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-18.
//

import Foundation


protocol CameraConfigActionDelegate : AnyObject {
    
    func cameraRestricted()
    func cameraDenied()
    func cameraAuthorized()
    func cameraConfigError()
    func sessionConfigSuccess()
    func cameraDevicesSwitched()
}
