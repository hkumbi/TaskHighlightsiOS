//
//  CreateTaskButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import Foundation


protocol CreateTaskButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateTaskButtonType)
}

enum CreateTaskButtonType {
    case back, date, importance, save
}
