//
//  CameraBlockingActionDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-18.
//

import Foundation


protocol CameraBlockingActionDelegate : AnyObject {
    
    var allowNavigationAction : Bool { get set }
    var allowCaptureAction : Bool { get set }
    var allowCameraConfigAction : Bool { get set }
    
    func navigationActionStarted()
    func flipCameraActionStarted()
    func captureActionStarted()
    
    func allowAllActions()
}
