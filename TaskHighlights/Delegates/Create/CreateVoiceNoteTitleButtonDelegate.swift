//
//  CreateVoiceNoteTitleButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-21.
//

import Foundation


protocol CreateVoiceNoteTitleButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateVoiceNoteTitleButtonType)
}

enum CreateVoiceNoteTitleButtonType {
    case back, date, audioControl, save
}
