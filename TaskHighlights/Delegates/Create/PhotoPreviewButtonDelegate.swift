//
//  PhotoPreviewButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-15.
//

import Foundation


protocol PhotoPreviewButtonDelegate : AnyObject {
    
    func buttonPressed(for type : PhotoPreviewButtonType)
}

enum PhotoPreviewButtonType {
case back, save
}
