//
//  CameraButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import Foundation


protocol CameraButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CameraButtonType)
}

enum CameraButtonType {
    case close, capture, flip, gallery, unsplash, cameraPermission
}
