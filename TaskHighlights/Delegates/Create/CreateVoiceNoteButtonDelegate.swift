//
//  CreateVoiceNoteButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-21.
//

import Foundation


protocol CreateVoiceNoteButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateVoiceNoteButtonType)
}

enum CreateVoiceNoteButtonType {
case action, close, allowMic
}
