//
//  UserPhotoGalleryButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-15.
//

import Foundation


protocol UserPhotoGalleryButtonDelegate : AnyObject {
    
    func buttonPressed(for type : UserPhotoGalleryButtonType)
}


enum UserPhotoGalleryButtonType {
    case cancel, save, allowAccess, manage, preview
}
