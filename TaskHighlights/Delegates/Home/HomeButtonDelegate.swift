//
//  HomeButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import Foundation


protocol HomeButtonDelegate : AnyObject {
    
    func buttonPressed(for type : HomeButtonType)
    
}


enum HomeButtonType {
    case weather, settings, viewCalendar, create, highlightsSection, remindersSection, tasksSection
}
