//
//  CreateVCDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation


protocol CreateVCDelegate : AnyObject {
    
    func createContent(for type : CreateContentType)
}

enum CreateContentType {
    case highlights, task, voiceNotes
}
