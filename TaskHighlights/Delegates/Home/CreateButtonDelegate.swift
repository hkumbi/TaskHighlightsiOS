//
//  CreateButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import Foundation


protocol CreateButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CreateButtonType)
}

enum CreateButtonType {
case photoHighlight, voiceNote, task, cancel
}
