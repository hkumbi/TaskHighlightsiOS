//
//  WeatherButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import Foundation


protocol WeatherButtonDelegate : AnyObject {
    
    func closeButtonPressed()
}
