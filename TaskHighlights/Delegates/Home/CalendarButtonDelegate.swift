//
//  CalendarButtonDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import Foundation


protocol CalendarButtonDelegate : AnyObject {
    
    func buttonPressed(for type : CalendarButtonType)
}


enum CalendarButtonType {
case previous, next, close
}
