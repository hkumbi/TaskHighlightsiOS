//
//  CalendarVCDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import Foundation


protocol CalendarVCDelegate : AnyObject {
    
    func selectedDateChanged(_ date : Date)
    func vcClosed()
}
