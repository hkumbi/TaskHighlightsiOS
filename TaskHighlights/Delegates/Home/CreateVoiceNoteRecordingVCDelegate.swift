//
//  CreateVoiceNoteRecordingVCDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation


protocol CreateVoiceNoteRecordingVCDelegate : AnyObject {
    
    func recordingCreated(_ url : URL) -> Bool
}
