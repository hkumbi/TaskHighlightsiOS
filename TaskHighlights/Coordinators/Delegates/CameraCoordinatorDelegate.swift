//
//  CameraCoordinatorDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation
import UIKit


protocol CameraCoordinatorDelegate : AnyObject {
    
    func navigationButtonPressed(for type : CameraNavigationType)
    func photoCaptured(_ image : UIImage)
}


enum CameraNavigationType {
    case close, gallery, unsplash
}
