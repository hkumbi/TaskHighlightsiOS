//
//  HomeCoordinatorDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation


protocol HomeCoordinatorDelegate : AnyObject {
    
    func createContent(for type : HomeCreateContentType)
    func voiceNoteCreated(_ url : URL) -> Bool
    func navigateToContent(_ type : HomeContentType, for date : Date)
}

enum HomeCreateContentType {
    case camera, task
}

enum HomeContentType {
    case highlights, voiceNotes, tasks
}

