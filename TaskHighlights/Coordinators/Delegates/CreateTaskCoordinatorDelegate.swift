//
//  CreateTaskCoordinatorDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation


protocol CreateTaskCoordinatorDelegate : AnyObject {
    
    func goBackFromCreateTaskVC()
}
