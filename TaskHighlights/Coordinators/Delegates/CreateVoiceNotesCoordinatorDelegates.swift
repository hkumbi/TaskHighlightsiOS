//
//  CreateVoiceNotesCoordinatorDelegates.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation


protocol CreateVoiceNotesTitleCoordinatorDelegate : AnyObject {
    
    func popCreateVoiceNoteTitleVC()
    func voiceNoteSaved(_ url : URL)
}
