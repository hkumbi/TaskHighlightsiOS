//
//  ChildCoordinatorDelegate.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation


protocol ChildCoordinatorDelegate : AnyObject {
    
    func resignControl()
}
