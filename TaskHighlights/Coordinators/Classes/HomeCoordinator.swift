//
//  HomeCoordinator.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-22.
//

import Foundation
import UIKit


class HomeCoordinator  {
    
    private let navigation : MainNavigationViewController
    
    init() {
        
        let homeVC = HomeViewController()
        navigation = MainNavigationViewController(rootViewController: homeVC)
        
        homeVC.coordinatorDelegate = self
    }
    
    func start() -> MainNavigationViewController {
        navigation
    }
    
}

extension HomeCoordinator : ChildCoordinatorDelegate {
    
    func resignControl() {
        
    }
    
}


extension HomeCoordinator : HomeCoordinatorDelegate {
    
    func createContent(for type: HomeCreateContentType) {
        
        switch type {
        case .camera:
            print("")
        case .task:
            let createTaskVC = CreateTaskViewController()
            createTaskVC.coordinatorDelegate = self
            navigation.pushViewController(createTaskVC, animated: true)
        }
    }
    
    func voiceNoteCreated(_ url: URL) -> Bool {
        
        let titleVC = CreateVoiceNoteTitleViewController()
        let audioURLWorks = titleVC.setAudioURL(url)
        
        if audioURLWorks {
            titleVC.coordinatorDelegate = self
            navigation.pushViewController(titleVC, animated: true)
        }
        
        return audioURLWorks
    }
    
    func navigateToContent(_ type : HomeContentType, for date: Date) {
        
        switch type {
        case .highlights:
            print("")
        case .voiceNotes:
            print("")
        case .tasks:
            print("")
        }
    }
    
}


extension HomeCoordinator : CreateVoiceNotesTitleCoordinatorDelegate {
    
    func popCreateVoiceNoteTitleVC() {
        navigation.popViewController(animated: true)
    }
    
    func voiceNoteSaved(_ url: URL) {
        print("Hello voice note saved")
    }
    
}

extension HomeCoordinator : CreateTaskCoordinatorDelegate {
    
    func goBackFromCreateTaskVC() {
        navigation.popViewController(animated: true)
    }
    
}
