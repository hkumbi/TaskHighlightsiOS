//
//  CameraCoordinator.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-23.
//

import Foundation
import UIKit


class CameraCoordinator {
    
    private let navigation : MainNavigationViewController
    private let initialVC : UIViewController
    
    unowned var childCoordinatorDelegate : ChildCoordinatorDelegate!
    
    
    init(_ navigation : MainNavigationViewController) {
        self.navigation = navigation
        initialVC = navigation.viewControllers.last!
    }
    
    func start() {
        
        let cameraVC = CameraViewController()
        cameraVC.coordinatorDelegate = self
        navigation.pushViewController(cameraVC, animated: true)
    }
    
}


extension CameraCoordinator : CameraCoordinatorDelegate {
    
    func navigationButtonPressed(for type: CameraNavigationType) {
        
        switch type {
        case .close:
            navigation.popViewController(animated: true)
            childCoordinatorDelegate.resignControl()
            
        case .gallery:
            print("Gallery")
        case .unsplash:
            print("Unsplash")
        }
    }
    
    func photoCaptured(_ image: UIImage) {
        
        let previewVC = PhotoPreviewViewController()
        previewVC.image = image
        navigation.pushViewController(previewVC, animated: true)
    }
    
}
