//
//  UserPhotoCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit

class UserPhotoCollectionViewCell: UICollectionViewCell {
    
    static let size = CGSize(width: width, height: height)
    
    private static let width = Int(UIScreen.main.bounds.width/3 - 1/3)
    private static let height = Int(width*5/4)
    
    override var isSelected: Bool {
        didSet {
            checkIconView.isHidden = !isSelected
            dimmingView.isHidden = !isSelected
        }
    }
    
    private let thumbnailView : UIImageView = .preppedForAutoLayout()
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    private let checkIconView : UIImageView = .preppedForAutoLayout()
    
    var identifier : String!
    
    var image : UIImage? {
        get { thumbnailView.image }
        set { thumbnailView.image = newValue }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(thumbnailView)
        self.contentView.addSubview(dimmingView)
        self.contentView.addSubview(checkIconView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        contentView.clipsToBounds = true
        
        dimmingView.backgroundColor = .black.withAlphaComponent(0.4)
        dimmingView.isHidden = true
        
        thumbnailView.contentMode = .scaleAspectFill
        
        checkIconView.image = UIImage(systemName: "checkmark.circle")
        checkIconView.tintColor = .green
        checkIconView.contentMode = .scaleAspectFit
        checkIconView.isHidden = true
    }
    
    private func makeConstraints() {
        
        thumbnailView.pin(to: contentView)
        dimmingView.pin(to: contentView)
        
        NSLayoutConstraint.activate([
            checkIconView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.2),
            checkIconView.heightAnchor.constraint(equalTo: checkIconView.widthAnchor),
            checkIconView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            checkIconView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -5),
        ])
    }
    
}
