//
//  UserPhotoGalleryView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit
import PhotosUI

class UserPhotoGalleryView: UIView {
    
    private let cancelButton : UIButton = .preppedForAutoLayout()
    private let saveButton : UIButton = .preppedForAutoLayout()
    private let previewImageView : UIImageView = .preppedForAutoLayout()
    private let previewButton : UIButton = .preppedForAutoLayout()
    private let managePermissionView : UIView = .viewPreppedForAutoLayout()
    private let managePermissionLabel : UILabel = .preppedForAutoLayout()
    private let managePermissionButton : UIButton = .preppedForAutoLayout()
    private let infoTitleLabel : UILabel = .preppedForAutoLayout()
    private let infoSubTitleLabel : UILabel = .preppedForAutoLayout()
    private let allowAccessButton : UIButton = .preppedForAutoLayout()
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    unowned var buttonDelegate : UserPhotoGalleryButtonDelegate!

    private let manageButtonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
    
    var selectedImage : UIImage? {
        get { previewImageView.image }
        set {
            previewImageView.image = newValue
            previewImageView.layer.borderWidth = newValue == nil ? 1 : 0
        }
    }
    
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(cancelButton)
        self.addSubview(saveButton)
        self.addSubview(previewImageView)
        self.addSubview(previewButton)
        self.addSubview(managePermissionView)
        self.addSubview(collectionView)
        self.addSubview(infoTitleLabel)
        self.addSubview(infoSubTitleLabel)
        self.addSubview(allowAccessButton)
        
        managePermissionView.addSubview(managePermissionLabel)
        managePermissionView.addSubview(managePermissionButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
        
        let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleTextPressColors(.black)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleTextPressColors(.customGreen)
        saveButton.titleLabel?.font = buttonFont
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        
        previewImageView.layer.borderWidth = 1
        previewImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        previewButton.setTitle("Preview", for: .normal)
        previewButton.setTitleTextPressColors(.black)
        previewButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        previewButton.addTarget(self, action: #selector(previewButtonPressed), for: .touchUpInside)
        
        managePermissionView.backgroundColor = .black.withAlphaComponent(0.1)
        
        managePermissionLabel.text = "Tasks Highlights has access to all your photos."
        managePermissionLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .medium))
        managePermissionLabel.textColor = .black
        managePermissionLabel.numberOfLines = 0
        
        managePermissionButton.setTitle("Manage", for: .normal)
        managePermissionButton.setTitleTextPressColors(.black)
        managePermissionButton.titleLabel?.font = manageButtonFont
        managePermissionButton.addTarget(self, action: #selector(manageButtonPressed), for: .touchUpInside)
        
        infoTitleLabel.textColor = .black
        infoTitleLabel.textAlignment = .center
        infoTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 22, weight: .bold))
        
        infoSubTitleLabel.textColor = .black
        infoSubTitleLabel.textAlignment = .center
        infoSubTitleLabel.numberOfLines = 0
        infoSubTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        allowAccessButton.setTitle("Allow Access", for: .normal)
        allowAccessButton.setTitleTextPressColors(.customGreen)
        allowAccessButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        allowAccessButton.addTarget(self, action: #selector(allowAccessButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let quarterY = UIScreen.main.bounds.height*0.25
        let manageButtonWidth = String.getLabelWidth(text: "Manage", font: manageButtonFont)
        
        NSLayoutConstraint.activate([
            cancelButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            cancelButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            saveButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            saveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            previewImageView.topAnchor.constraint(equalTo: saveButton.bottomAnchor, constant: 10),
            previewImageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.25),
            previewImageView.heightAnchor.constraint(equalTo: previewImageView.widthAnchor),
            previewImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            previewButton.leftAnchor.constraint(equalTo: previewImageView.rightAnchor, constant: 20),
            previewButton.bottomAnchor.constraint(equalTo: previewImageView.bottomAnchor),
            
            managePermissionView.topAnchor.constraint(equalTo: previewImageView.bottomAnchor, constant: 20),
            managePermissionView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.08),
            managePermissionView.widthAnchor.constraint(equalTo: widthAnchor),
            managePermissionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            managePermissionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            managePermissionLabel.centerYAnchor.constraint(equalTo: managePermissionView.centerYAnchor),
            managePermissionLabel.rightAnchor.constraint(equalTo: managePermissionButton.leftAnchor, constant: -spacing),
            
            managePermissionButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            managePermissionButton.widthAnchor.constraint(equalToConstant: manageButtonWidth),
            managePermissionButton.centerYAnchor.constraint(equalTo: managePermissionView.centerYAnchor),
            
            collectionView.topAnchor.constraint(equalTo: managePermissionView.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            infoTitleLabel.bottomAnchor.constraint(equalTo: centerYAnchor, constant: -quarterY - 20),
            infoTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            infoSubTitleLabel.topAnchor.constraint(equalTo: centerYAnchor, constant: -quarterY+20),
            infoSubTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            allowAccessButton.topAnchor.constraint(equalTo: centerYAnchor),
            allowAccessButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }
    
    @objc private func previewButtonPressed() {
        buttonDelegate.buttonPressed(for: .preview)
    }
    
    @objc private func manageButtonPressed() {
        buttonDelegate.buttonPressed(for: .manage)
    }

    @objc private func allowAccessButtonPressed() {
        buttonDelegate.buttonPressed(for: .allowAccess)
    }
    
}


extension UserPhotoGalleryView {

    func setViewState(_ status : PHAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            cancelButton.isHidden = true
            saveButton.isHidden = true
            previewImageView.isHidden = true
            previewButton.isHidden = true
            managePermissionView.isHidden = true
            infoTitleLabel.isHidden = true
            infoSubTitleLabel.isHidden = true
            allowAccessButton.isHidden = true
            collectionView.isHidden = true
            
        case .restricted:
            cancelButton.isHidden = false
            infoTitleLabel.isHidden = false
            infoSubTitleLabel.isHidden = false
            
            saveButton.isHidden = true
            previewImageView.isHidden = true
            previewButton.isHidden = true
            managePermissionView.isHidden = true
            collectionView.isHidden = true
            allowAccessButton.isHidden = true

            infoTitleLabel.text = "Photos Restricted"
            infoSubTitleLabel.text = "Access your photos has been restricted"

        case .denied:
            cancelButton.isHidden = false
            infoTitleLabel.isHidden = false
            infoSubTitleLabel.isHidden = false
            allowAccessButton.isHidden = false
            
            saveButton.isHidden = true
            previewImageView.isHidden = true
            previewButton.isHidden = true
            managePermissionView.isHidden = true
            collectionView.isHidden = true

            infoTitleLabel.text = "Create Highlights"
            infoSubTitleLabel.text = "Tasks Highlights needs access to your photos"
            
        case .authorized:
            allowedOrLimitedState()
            managePermissionLabel.text = "Tasks Highlights has access to all your photos."
            
        case .limited:
            allowedOrLimitedState()
            managePermissionLabel.text = "Tasks Highlights has limited access to your photos."
            
        @unknown default:
            fatalError("Non configured PHAuthorizationStatus")
        }
    }
    
    private func allowedOrLimitedState() {
        cancelButton.isHidden = false
        saveButton.isHidden = false
        previewImageView.isHidden = false
        previewButton.isHidden = false
        managePermissionView.isHidden = false
        collectionView.isHidden = false
        
        infoTitleLabel.isHidden = true
        infoSubTitleLabel.isHidden = true
        allowAccessButton.isHidden = true
    }
    
}

