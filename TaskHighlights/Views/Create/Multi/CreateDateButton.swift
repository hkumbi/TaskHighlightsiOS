//
//  CreateDateButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-15.
//

import UIKit

class CreateDateButton: BackgroundButton {
    
    static let spacing = UIScreen.main.bounds.width*0.05

    private let customTitleLabel : UILabel = .preppedForAutoLayout()
    private let valueLabel : UILabel = .preppedForAutoLayout()
    
    var customTitle : String {
        get { customTitleLabel.text ?? "" }
        set { customTitleLabel.text = newValue }
    }
    
    var value : String {
        get { valueLabel.text ?? "" }
        set { valueLabel.text = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(customTitleLabel)
        self.addSubview(valueLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.height/2
    }
    
    private func configureViews() {
        
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
        baseColor = .white
        highlightedColor = .black.withAlphaComponent(0.1)
        
        customTitleLabel.textColor = .black
        customTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        valueLabel.textColor = .black
        valueLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        let width = UIScreen.main.bounds.width*0.75
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: width*0.17),
            
            customTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Self.spacing),
            customTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            valueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -Self.spacing),
        ])
    }
}
