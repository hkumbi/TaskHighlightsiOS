//
//  CameraCaptureView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit

class CameraCaptureView: UIView {
    
    private let size = UIScreen.main.bounds.width*0.2
    
    unowned var buttonDelegate : CameraButtonDelegate!
    unowned var blockingActionDelegate : CameraBlockingActionDelegate!


    init() {
        super.init(frame: .zero)
        
        configureViews()
        configureGesture()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }

    private func configureViews() {
        
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = size*0.07
        layer.cornerRadius = size/2
    }
    
    private func configureGesture() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(buttonPressed))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: size),
            heightAnchor.constraint(equalToConstant: size)
        ])
    }
    
    @objc private func buttonPressed() {
        
        if blockingActionDelegate.allowCaptureAction {
            buttonDelegate.buttonPressed(for: .capture)
        }
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseInOut) {
            [unowned self] in
            
            transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        } completion: { [unowned self]_ in
            
            UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseInOut){
                [unowned self] in
                transform = .identity
            }
        }
    }
}
