//
//  CameraView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit
import AVFoundation

enum CameraViewState {
    case notDetermined, authorized, denied, restricted, error
}

class CameraView: UIView {
    
    let previewView = CameraPreviewView()
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let flipCameraButton : IconButton = .iconPreppedForAutoLayout()
    private let unsplashButton : UIButton = .preppedForAutoLayout()
    private let galleryButton : IconButton = .iconPreppedForAutoLayout()
    private let captureView = CameraCaptureView()
    private let infoTitleLabel : UILabel = .preppedForAutoLayout()
    private let infoSubtitleLabel : UILabel = .preppedForAutoLayout()
    private let cameraPermissionButton : UIButton = .preppedForAutoLayout()
    private let bottomSpacerView : UIView = .viewPreppedForAutoLayout()
    
    unowned var buttonDelegate : CameraButtonDelegate! {
        didSet { captureView.buttonDelegate = buttonDelegate }
    }
    
    unowned var blockingActionDelegate : CameraBlockingActionDelegate! {
        didSet { captureView.blockingActionDelegate = blockingActionDelegate }
    }
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(previewView)
        self.addSubview(closeButton)
        
        self.addSubview(infoTitleLabel)
        self.addSubview(infoSubtitleLabel)
        self.addSubview(cameraPermissionButton)
        self.addSubview(captureView)
        self.addSubview(bottomSpacerView)
        self.addSubview(flipCameraButton)
        self.addSubview(unsplashButton)
        self.addSubview(galleryButton)
        
        makeConstraint()
        screenBasedConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    private func configureViews() {
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.baseColor = .white
        closeButton.pinImageView()
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        infoTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 26, weight: .bold))
        infoTitleLabel.textColor = .white
        
        infoSubtitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        infoSubtitleLabel.textColor = .white
        infoSubtitleLabel.numberOfLines = 0
        infoSubtitleLabel.textAlignment = .center
        
        cameraPermissionButton.setTitle("Allow Camera Permission", for: .normal)
        cameraPermissionButton.setTitleTextPressColors(.customGreen)
        cameraPermissionButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        cameraPermissionButton.addTarget(self, action: #selector(cameraPermissionButtonPressed), for: .touchUpInside)
        
        flipCameraButton.setImage(UIImage(systemName: "arrow.triangle.2.circlepath"), for: .normal)
        flipCameraButton.baseColor = .white
        flipCameraButton.imageView?.contentMode = .scaleAspectFit
        flipCameraButton.addTarget(self, action: #selector(flipCameraButtonPressed), for: .touchUpInside)
        
        unsplashButton.setTitle("Unsplash", for: .normal)
        unsplashButton.setTitleTextPressColors(.white)
        unsplashButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        unsplashButton.addTarget(self, action: #selector(unsplashButtonPressed), for: .touchUpInside)
        
        galleryButton.setImage(UIImage(systemName: "photo"), for: .normal)
        galleryButton.baseColor = .white
        galleryButton.imageView?.contentMode = .scaleAspectFit
        galleryButton.addTarget(self, action: #selector(galleryButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraint() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let quarterY = UIScreen.main.bounds.height/4
        let bottomHeight = UIScreen.main.bounds.height*0.08
        
        galleryButton.pinImageView()
        flipCameraButton.pinImageView()
        
        NSLayoutConstraint.activate([
            previewView.topAnchor.constraint(equalTo: topAnchor),
            previewView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            closeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            infoTitleLabel.topAnchor.constraint(equalTo: centerYAnchor, constant: -quarterY - 20),
            infoTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            infoSubtitleLabel.topAnchor.constraint(equalTo: infoTitleLabel.bottomAnchor, constant: 40),
            infoSubtitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cameraPermissionButton.topAnchor.constraint(equalTo: centerYAnchor, constant: 30),
            cameraPermissionButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            captureView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            flipCameraButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            flipCameraButton.heightAnchor.constraint(equalTo: flipCameraButton.widthAnchor),
            flipCameraButton.centerYAnchor.constraint(equalTo: bottomSpacerView.centerYAnchor),
            flipCameraButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            unsplashButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            unsplashButton.centerYAnchor.constraint(equalTo: bottomSpacerView.centerYAnchor),
            
            galleryButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            galleryButton.heightAnchor.constraint(equalTo: galleryButton.widthAnchor),
            galleryButton.centerYAnchor.constraint(equalTo: bottomSpacerView.centerYAnchor),
            galleryButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            bottomSpacerView.heightAnchor.constraint(equalToConstant: bottomHeight),
            bottomSpacerView.widthAnchor.constraint(equalTo: widthAnchor),
            bottomSpacerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomSpacerView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
    
    private func screenBasedConstraints() {
        let screenAspectRatio = UIScreen.main.bounds.width/UIScreen.main.bounds.height
        let isLargeScreen = 17.5/9 >= screenAspectRatio
        var bottomConstraint : NSLayoutConstraint
        
        if isLargeScreen {
            bottomConstraint = captureView.bottomAnchor.constraint(equalTo: previewView.bottomAnchor, constant: -20)
        } else {
            bottomConstraint = captureView.bottomAnchor.constraint(equalTo: bottomSpacerView.topAnchor, constant: -20)
        }
        
        bottomConstraint.isActive = true
    }
    
    @objc private func closeButtonPressed() {
        guard blockingActionDelegate.allowNavigationAction else { return }
        buttonDelegate.buttonPressed(for: .close)
    }
    
    @objc private func galleryButtonPressed() {
        guard blockingActionDelegate.allowNavigationAction else { return }
        buttonDelegate.buttonPressed(for: .gallery)
    }
    
    @objc private func unsplashButtonPressed() {
        guard blockingActionDelegate.allowNavigationAction else { return }
        buttonDelegate.buttonPressed(for: .unsplash)
    }
    
    @objc private func flipCameraButtonPressed() {
        guard blockingActionDelegate.allowNavigationAction else { return }
        buttonDelegate.buttonPressed(for: .flip)
    }
    
    @objc private func cameraPermissionButtonPressed() {
        guard blockingActionDelegate.allowNavigationAction else { return }
        buttonDelegate.buttonPressed(for: .cameraPermission)
    }

}


extension CameraView {
    
    func changeViewState(to state : CameraViewState) {
        
        switch state {
        case .notDetermined:
            previewView.isHidden = true
            closeButton.isHidden = true
            flipCameraButton.isHidden = true
            unsplashButton.isHidden = true
            galleryButton.isHidden = true
            captureView.isHidden = true
            infoTitleLabel.isHidden = true
            infoSubtitleLabel.isHidden = true
            cameraPermissionButton.isHidden = true
            
        case .authorized:
            previewView.isHidden = false
            closeButton.isHidden = false
            flipCameraButton.isHidden = false
            unsplashButton.isHidden = false
            galleryButton.isHidden = false
            captureView.isHidden = false
            
            infoTitleLabel.isHidden = true
            infoSubtitleLabel.isHidden = true
            cameraPermissionButton.isHidden = true

        case .denied:
            closeButton.isHidden = false
            infoTitleLabel.isHidden = false
            infoSubtitleLabel.isHidden = false
            cameraPermissionButton.isHidden = false

            previewView.isHidden = true
            flipCameraButton.isHidden = true
            unsplashButton.isHidden = true
            galleryButton.isHidden = true
            captureView.isHidden = true
            
            infoTitleLabel.text = "Create Highlights"
            infoSubtitleLabel.text = "To create a photo highlight\nyou must allow camera permission"
            infoSubtitleLabel.addLineSpacing(10)
            
        case .restricted:
            previewView.isHidden = true
            flipCameraButton.isHidden = true
            unsplashButton.isHidden = true
            galleryButton.isHidden = true
            captureView.isHidden = true
            cameraPermissionButton.isHidden = true

            closeButton.isHidden = false
            infoTitleLabel.isHidden = false
            infoSubtitleLabel.isHidden = false

            infoTitleLabel.text = "Camera Restricted"
            infoSubtitleLabel.text = "The system has restricted access to the camera.\nPlease try again later."
            infoSubtitleLabel.addLineSpacing(10)

        case .error:
            previewView.isHidden = true
            flipCameraButton.isHidden = true
            unsplashButton.isHidden = true
            galleryButton.isHidden = true
            captureView.isHidden = true
            cameraPermissionButton.isHidden = true
            
            closeButton.isHidden = false
            infoTitleLabel.isHidden = false
            infoSubtitleLabel.isHidden = false
            
            infoTitleLabel.text = "Camera Error"
            infoSubtitleLabel.text = "There was an error with the camera.\nPlease try again later."
            infoSubtitleLabel.addLineSpacing(10)
        }
    }
    
}
