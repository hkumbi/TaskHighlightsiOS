//
//  CameraPreviewView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-14.
//

import UIKit
import AVFoundation

class CameraPreviewView: UIView {
    
    override class var layerClass: AnyClass {
        AVCaptureVideoPreviewLayer.self
    }

    var videoPreviewLayer : AVCaptureVideoPreviewLayer {
        layer as! AVCaptureVideoPreviewLayer
    }
    
    var orientation : AVCaptureVideoOrientation? {
        get { videoPreviewLayer.connection?.videoOrientation }
    }
    

    init() {
        super.init(frame: .zero)
        
        makeConstraints()
        
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .black
        clipsToBounds = true
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
    }
    
    private func makeConstraints() {
        let width = UIScreen.main.bounds.width
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalTo: widthAnchor, multiplier: 16/9)
        ])
    }
    
    

}
