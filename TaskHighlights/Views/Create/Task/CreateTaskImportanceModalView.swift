//
//  CreateTaskImportanceModalView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import UIKit

class CreateTaskImportanceModalView: UIView {

    private let cancelButton : UIButton = .preppedForAutoLayout()
    private let saveButton : UIButton = .preppedForAutoLayout()
    private let selectedTitleLabel : UILabel = .preppedForAutoLayout()
    private let selectedImportanceLabel : UILabel = .preppedForAutoLayout()
    private let pickerView : UIPickerView = .preppedForAutoLayout()
    
    private var heightConstraint : NSLayoutConstraint!

    private let height : CGFloat
    private let pickerHeight = UIScreen.main.bounds.height*0.15
    private let topSpacing = UIScreen.main.bounds.height*0.02
    private let textTopSpacing = UIScreen.main.bounds.height*0.01
    
    private let buttonFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
    private let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .semibold))
    private let selectedImportanceFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
    
    unowned var buttonDelegate : CreateTaskImportanceButtonDelegate!
    
    var heightConstant : CGFloat {
        get { heightConstraint.constant }
    }
    
    var bottomSafeAreaConstant : CGFloat {
        get { heightConstraint.constant - height }
        set { heightConstraint.constant = height + newValue }
    }
    
    var pickerDataSource : UIPickerViewDataSource? {
        get { pickerView.dataSource }
        set { pickerView.dataSource = newValue }
    }
    
    var pickerDelegate : UIPickerViewDelegate? {
        get { pickerView.delegate }
        set { pickerView.delegate = newValue }
    }
    
    var pickerSelectedRow : Int {
        get { pickerView.selectedRow(inComponent: 0) }
    }
    
    var selectedValue : String {
        get { selectedImportanceLabel.text ?? "" }
        set { selectedImportanceLabel.text = newValue }
    }
    
    
    init() {
        
        let buttonHeight = String.getLabelHeight(text: "", font: buttonFont)
        let spacingHeight = topSpacing*4 + textTopSpacing
        let titleHeight = String.getLabelHeight(text: "", font: titleFont)
        let valueHeight = String.getLabelHeight(text: "", font: selectedImportanceFont)
        
        height = buttonHeight + pickerHeight + spacingHeight + titleHeight + valueHeight
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(cancelButton)
        self.addSubview(saveButton)
        self.addSubview(selectedTitleLabel)
        self.addSubview(selectedImportanceLabel)
        self.addSubview(pickerView)
        
        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        backgroundColor = .white
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        layer.cornerRadius = UIScreen.main.bounds.width*0.05
        
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleTextPressColors(.black)
        cancelButton.titleLabel?.font = buttonFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleTextPressColors(.customGreen)
        saveButton.titleLabel?.font = buttonFont
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        
        selectedTitleLabel.text = "Selected Value"
        selectedTitleLabel.textColor = .black
        selectedTitleLabel.font = titleFont
        
        selectedImportanceLabel.text = "Low"
        selectedImportanceLabel.textColor = .black
        selectedImportanceLabel.font = selectedImportanceFont
        
    }
    
    private func makeConstraint() {
        
        let width = UIScreen.main.bounds.width
        let buttonSideSpacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightConstraint,
            
            cancelButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            cancelButton.leftAnchor.constraint(equalTo: leftAnchor, constant: buttonSideSpacing),
            
            saveButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            saveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -buttonSideSpacing),
            
            selectedTitleLabel.topAnchor.constraint(equalTo: cancelButton.bottomAnchor, constant: topSpacing),
            selectedTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            selectedImportanceLabel.topAnchor.constraint(equalTo: selectedTitleLabel.bottomAnchor, constant: textTopSpacing),
            selectedImportanceLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            pickerView.topAnchor.constraint(equalTo: selectedImportanceLabel.bottomAnchor, constant: topSpacing),
            pickerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.6),
            pickerView.heightAnchor.constraint(equalToConstant: pickerHeight),
            pickerView.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }

}


extension CreateTaskImportanceModalView {
    
    func setValue(_ index : Int) {
        pickerView.selectRow(index, inComponent: 0, animated: false)
    }
}
