//
//  CreateTaskView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-16.
//

import UIKit

class CreateTaskView: UIView {
    
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let taskTitleField : UITextField = .preppedForAutoLayout()
    private let taskBottomView : UIView = .viewPreppedForAutoLayout()
    private let taskTitleMaxCharLabel : UILabel = .preppedForAutoLayout()
    private let taskTextView : UITextView = .preppedForAutoLayout()
    private let taskPlaceholderLabel : UILabel = .preppedForAutoLayout()
    private let taskMaxCharLabel : UILabel = .preppedForAutoLayout()
    private let dateButton = CreateDateButton()
    private let importanceButton = CreateImportanceButton()
    private let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateTaskButtonDelegate!
    
    var textFieldDelegate : UITextFieldDelegate? {
        get { taskTitleField.delegate }
        set { taskTitleField.delegate = newValue }
    }
    
    var textViewDelegate : UITextViewDelegate? {
        get { taskTextView.delegate }
        set { taskTextView.delegate = newValue }
    }
    
    var taskTitle : String {
        get { taskTitleField.text ?? "" }
    }
    
    var taskText : String {
        get { taskTextView.text }
    }
    
    var titleMaxCharIsHidden : Bool {
        get { taskTitleMaxCharLabel.isHidden }
        set { taskTitleMaxCharLabel.isHidden = newValue }
    }
    
    var textMaxCharIsHidden : Bool {
        get { taskMaxCharLabel.isHidden }
        set { taskMaxCharLabel.isHidden = newValue }
    }
    
    var textViewPlaceholderIsHidden : Bool {
        get { taskPlaceholderLabel.isHidden }
        set { taskPlaceholderLabel.isHidden = newValue }
    }
    
    var date : String {
        get { dateButton.value }
        set { dateButton.value = newValue}
    }
    
    var importanceText : String {
        get { importanceButton.value }
        set { importanceButton.value = newValue }
    }
    
    var importanceIconColor : UIColor {
        get { importanceButton.iconColor }
        set { importanceButton.iconColor = newValue }
    }
    
    var saveIsEnabled : Bool {
        get { saveButton.isEnabled }
        set { saveButton.isEnabled = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(backButton)
        self.addSubview(taskTitleField)
        self.addSubview(taskBottomView)
        self.addSubview(taskTitleMaxCharLabel)
        self.addSubview(taskTextView)
        self.addSubview(taskPlaceholderLabel)
        self.addSubview(taskMaxCharLabel)
        self.addSubview(dateButton)
        self.addSubview(importanceButton)
        self.addSubview(saveButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    private func configureViews() {
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .black
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        taskTitleField.placeholder = "Title"
        taskTitleField.textAlignment = .center
        taskTitleField.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 20, weight: .medium))
        
        taskBottomView.backgroundColor = .lightGray
        
        let maxCharText = "Max character reached"
        let maxCharFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
        
        taskTitleMaxCharLabel.text = maxCharText
        taskTitleMaxCharLabel.font = maxCharFont
        taskTitleMaxCharLabel.textColor = .red
        taskTitleMaxCharLabel.isHidden = true
        
        let taskFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        taskPlaceholderLabel.text = "Type your reminder . . ."
        taskPlaceholderLabel.font = taskFont
        taskPlaceholderLabel.textColor = .gray
        
        taskTextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        taskTextView.font = taskFont
        taskTextView.layer.borderColor = UIColor.lightGray.cgColor
        taskTextView.layer.borderWidth = 1
        taskTextView.layer.cornerRadius = UIScreen.main.bounds.width*0.055
        
        taskMaxCharLabel.text = maxCharText
        taskMaxCharLabel.font = maxCharFont
        taskMaxCharLabel.textColor = .red
        taskMaxCharLabel.isHidden = true
        
        dateButton.customTitle = "Date"
        dateButton.addTarget(self, action: #selector(dateButtonPressed), for: .touchUpInside)
        
        importanceButton.addTarget(self, action: #selector(importanceButtonPressed), for: .touchUpInside)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.baseColor = .customGreen
        saveButton.highlightedColor = .customGreen.withAlphaComponent(0.3)
        saveButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissTapped))
        tap.numberOfTapsRequired = 1
        addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.05
        let buttonTopSpacing = UIScreen.main.bounds.height*0.03
        let sideSpacing = UIScreen.main.bounds.width*0.05
        
        backButton.pinImageView()
        
        NSLayoutConstraint.activate([
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            backButton.topAnchor.constraint(equalTo: topAnchor, constant: sideSpacing),
            
            taskTitleField.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: topSpacing),
            taskTitleField.centerXAnchor.constraint(equalTo: centerXAnchor),
            taskTitleField.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
            
            taskBottomView.topAnchor.constraint(equalTo: taskTitleField.bottomAnchor, constant: 5),
            taskBottomView.heightAnchor.constraint(equalToConstant: 1),
            taskBottomView.widthAnchor.constraint(equalTo: taskTitleField.widthAnchor),
            taskBottomView.centerXAnchor.constraint(equalTo: taskTitleField.centerXAnchor),
            
            taskTitleMaxCharLabel.topAnchor.constraint(equalTo: taskBottomView.bottomAnchor, constant: 5),
            taskTitleMaxCharLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            taskTextView.topAnchor.constraint(equalTo: taskBottomView.bottomAnchor, constant: topSpacing),
            taskTextView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            taskTextView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.2),
            taskTextView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            taskPlaceholderLabel.topAnchor.constraint(equalTo: taskTextView.topAnchor, constant: 10),
            taskPlaceholderLabel.leftAnchor.constraint(equalTo: taskTextView.leftAnchor, constant: 10+5),
            
            taskMaxCharLabel.topAnchor.constraint(equalTo: taskTextView.bottomAnchor, constant: 5),
            taskMaxCharLabel.leftAnchor.constraint(equalTo: taskTextView.leftAnchor),
            
            dateButton.topAnchor.constraint(equalTo: taskTextView.bottomAnchor, constant: topSpacing),
            dateButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            importanceButton.topAnchor.constraint(equalTo: dateButton.bottomAnchor, constant: buttonTopSpacing),
            importanceButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            saveButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            saveButton.heightAnchor.constraint(equalTo: saveButton.widthAnchor, multiplier: 0.14),
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func dismissTapped() {
        endEditing(true)
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func dateButtonPressed() {
        buttonDelegate.buttonPressed(for: .date)
    }
    
    @objc private func importanceButtonPressed() {
        buttonDelegate.buttonPressed(for: .importance)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }

}
