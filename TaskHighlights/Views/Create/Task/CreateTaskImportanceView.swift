//
//  CreateTaskImportanceView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-17.
//

import UIKit

class CreateTaskImportanceView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.6

    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = CreateTaskImportanceModalView()
    
    private var modalBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : CreateTaskImportanceButtonDelegate!
    
    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    
    var modalBottomConstant : CGFloat {
        get { modalBottomConstraint.constant }
        set { modalBottomConstraint.constant = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
    
    private func configureViews() {
        
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    private func makeConstraints() {
        
        dimmingView.pin(to: self)
        
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        
        NSLayoutConstraint.activate([
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
            modalBottomConstraint
        ])
    }
    
    @objc private func dimmingTapped() {
        buttonDelegate.buttonPressed(for: .cancel)
    }

}


extension CreateTaskImportanceView {
    
    func addPanGesture(_ pan : UIPanGestureRecognizer) {
        modalView.addGestureRecognizer(pan)
    }
}
