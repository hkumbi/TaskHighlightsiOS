//
//  CreateImportanceButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-16.
//

import UIKit

class CreateImportanceButton: BackgroundButton {
    
    static let spacing = UIScreen.main.bounds.width*0.05
    
    private let customTitleLabel : UILabel = .preppedForAutoLayout()
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let valueLabel : UILabel = .preppedForAutoLayout()
    
    var iconColor : UIColor {
        get { iconView.tintColor }
        set { iconView.tintColor = newValue }
    }
    
    var value : String {
        get { valueLabel.text ?? "" }
        set { valueLabel.text = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(customTitleLabel)
        self.addSubview(iconView)
        self.addSubview(valueLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.height/2
    }
    
    private func configureViews() {
        layer.borderWidth = 1
        layer.borderColor = UIColor.lightGray.cgColor
        baseColor = .white
        highlightedColor = .black.withAlphaComponent(0.1)
        
        customTitleLabel.text = "Importance"
        customTitleLabel.textColor = .black
        customTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        iconView.image = UIImage(systemName: "exclamationmark.triangle")
        iconView.contentMode = .scaleAspectFit
        
        valueLabel.textColor = .black
        valueLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        let width = UIScreen.main.bounds.width*0.75
        let iconWidth = UIScreen.main.bounds.width*0.07
        
        translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: width*0.17),
            
            customTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Self.spacing),
            customTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            iconView.widthAnchor.constraint(equalToConstant: iconWidth),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.rightAnchor.constraint(equalTo: valueLabel.leftAnchor, constant: -Self.spacing),
            
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            valueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -Self.spacing),
        ])
    }
}
