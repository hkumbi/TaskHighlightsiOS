//
//  CreateVoiceNoteView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-20.
//

import UIKit

class CreateVoiceNoteView: UIView {

    private let linearGradientView = GradientLayerView()
    private let timeLeftLabel : UILabel = .preppedForAutoLayout()
    private let recordControlButton = CreateVoiceNoteActionButton()
    private let recordControlIconView : UIImageView = .preppedForAutoLayout()
    private let allowMicButton : UIButton = .preppedForAutoLayout()
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    
    unowned var buttonDelegate : CreateVoiceNoteButtonDelegate!
    
    private var contentHeight : CGFloat = 0
    private let closeButtonBottomSpacing = UIScreen.main.bounds.height*0.01

    
    private var closeButtonBottomConstant : NSLayoutConstraint!
    private let timeLeftFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14, weight: .medium))

    var timeLeft : String {
        get { timeLeftLabel.text ?? "" }
        set { timeLeftLabel.text = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(linearGradientView)
        self.addSubview(timeLeftLabel)
        self.addSubview(recordControlButton)
        self.addSubview(recordControlIconView)
        self.addSubview(allowMicButton)
        self.addSubview(closeButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
        
    private func configureViews()  {
        
        linearGradientView.alpha = 0
        
        timeLeftLabel.text = "01:23"
        timeLeftLabel.textColor = .white
        timeLeftLabel.font = timeLeftFont
        
        recordControlIconView.tintColor = .white
        recordControlIconView.contentMode = .scaleAspectFit
        
        recordControlButton.addTarget(self, action: #selector(recordControlButtonPressed), for: .touchUpInside)
                
        closeButton.setImage(UIImage(systemName: "xmark", withConfiguration: UIImage.SymbolConfiguration(weight: .medium)), for: .normal)
        closeButton.baseColor = .white
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        allowMicButton.setTitle("Allow Microphone Permission", for: .normal)
        allowMicButton.setTitleTextPressColors(.white)
        allowMicButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .bold))
        allowMicButton.addTarget(self, action: #selector(allowMicButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let closeIconSize = UIScreen.main.bounds.width*0.08
        let iconSize = recordControlButton.size*0.3
        let spacing = UIScreen.main.bounds.height*0.04
        
        contentHeight = closeButtonBottomSpacing + spacing*2 + closeIconSize + recordControlButton.size + String.getLabelHeight(text: "", font: timeLeftFont)
        
        closeButtonBottomConstant = closeButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: contentHeight)
                
        closeButton.pinImageView()

        NSLayoutConstraint.activate([
            linearGradientView.heightAnchor.constraint(equalTo: heightAnchor),
            linearGradientView.widthAnchor.constraint(equalTo: widthAnchor),
            linearGradientView.centerXAnchor.constraint(equalTo: centerXAnchor),
            linearGradientView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            timeLeftLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            timeLeftLabel.bottomAnchor.constraint(equalTo: recordControlButton.centerYAnchor, constant: -recordControlButton.size/2 - spacing),
            
            recordControlButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            recordControlButton.centerYAnchor.constraint(equalTo: closeButton.topAnchor, constant: -recordControlButton.size/2 - spacing),
            
            recordControlIconView.widthAnchor.constraint(equalToConstant: iconSize),
            recordControlIconView.heightAnchor.constraint(equalTo: recordControlIconView.widthAnchor),
            recordControlIconView.centerXAnchor.constraint(equalTo: recordControlButton.centerXAnchor),
            recordControlIconView.centerYAnchor.constraint(equalTo: recordControlButton.centerYAnchor),
            
            allowMicButton.bottomAnchor.constraint(equalTo: closeButton.topAnchor, constant: -recordControlButton.size/2 - spacing),
            allowMicButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            closeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            closeButtonBottomConstant,
        ])
    }

    @objc private func closeButtonPressed(){
        buttonDelegate.buttonPressed(for: .close)
    }
    
    @objc private func recordControlButtonPressed(){
        buttonDelegate.buttonPressed(for: .action)
    }
    
    @objc private func allowMicButtonPressed(){
        buttonDelegate.buttonPressed(for: .allowMic)
    }

}


extension CreateVoiceNoteView {
    
    
    func animateOpen() {
        
        closeButtonBottomConstant.constant = contentHeight
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            linearGradientView.alpha = 1
            closeButtonBottomConstant.constant = -safeAreaInsets.bottom - closeButtonBottomSpacing
            layoutIfNeeded()
        }
    }
    
    func animateClose(completionHandler : (() -> Void)? = nil) {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            linearGradientView.alpha = 0
            closeButtonBottomConstant.constant = contentHeight
            layoutIfNeeded()
        } completion: { _ in
            
            if let completionHandler = completionHandler {
                completionHandler()
            }
        }
    }

    func setToIdleState() {
        
        recordControlIconView.image = UIImage(systemName: "mic.fill")
        recordControlButton.setToIdleState()
        recordControlButton.stopAnimation()
    }
    
    func setToRecordingState() {
        
        recordControlIconView.image = UIImage(systemName: "square.fill")
        recordControlButton.setToRecordingState()
        recordControlButton.startAnimation()
    }
    
    func changeViewState(isMicAllowed : Bool) {
        
        timeLeftLabel.isHidden = !isMicAllowed
        recordControlButton.isHidden = !isMicAllowed
        recordControlIconView.isHidden = !isMicAllowed
        allowMicButton.isHidden = isMicAllowed
        setToIdleState()
    }
    
}

