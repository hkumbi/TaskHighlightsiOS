//
//  CreateVoiceNoteTitleView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-21.
//

import UIKit

class CreateVoiceNoteTitleView: UIView {
        
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let titleTextView : UITextView = .preppedForAutoLayout()
    private let titlePlaceholderLabel : UILabel = .preppedForAutoLayout()
    private let maxCharLabel : UILabel = .preppedForAutoLayout()
    private let dateButton = CreateDateButton()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    private let progressBackgroundView : UIView = .viewPreppedForAutoLayout()
    private let progressView : UIView = .viewPreppedForAutoLayout()
    private let audioControlButton : IconButton = .iconPreppedForAutoLayout()
    private let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    private var progressRightConstraint : NSLayoutConstraint!

    private let progressWidth = UIScreen.main.bounds.width*0.95
    
    unowned var buttonDelegate : CreateVoiceNoteTitleButtonDelegate!
    
    var textViewDelegate : UITextViewDelegate? {
        get { titleTextView.delegate }
        set { titleTextView.delegate = newValue }
    }
    
    var titlePlaceholderIsHidden : Bool {
        get { titlePlaceholderLabel.isHidden }
        set { titlePlaceholderLabel.isHidden = newValue }
    }
    
    var date : String {
        get { dateButton.value }
        set { dateButton.value = newValue }
    }
    
    var time : String {
        get { timeLabel.text ?? "" }
        set { timeLabel.text = newValue }
    }

    var progress : CGFloat {
        get { progressRightConstraint.constant/progressWidth }
        set {
            let value = min(max(0, newValue), 1)
            progressRightConstraint.constant = value*progressWidth
        }
    }
    
    var audioControlIcon : UIImage? {
        get { audioControlButton.image(for: .normal) }
        set { audioControlButton.setImage(newValue, for: .normal) }
    }
    
    var isMaxCharLabelHidden : Bool {
        get { maxCharLabel.isHidden }
        set { maxCharLabel.isHidden = newValue }
    }
    
    var saveIsEnabled : Bool {
        get { saveButton.isEnabled }
        set { saveButton.isEnabled = newValue }
    }

    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(backButton)
        self.addSubview(titleTextView)
        self.addSubview(titlePlaceholderLabel)
        self.addSubview(maxCharLabel)
        self.addSubview(dateButton)
        self.addSubview(timeLabel)
        self.addSubview(progressBackgroundView)
        self.addSubview(progressView)
        self.addSubview(audioControlButton)
        self.addSubview(saveButton)

        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        saveButton.layer.cornerRadius = saveButton.frame.height/2
        progressBackgroundView.layer.cornerRadius = progressBackgroundView.frame.height/2
        progressView.layer.cornerRadius = progressView.frame.height/2
    }
    
    private func configureViews() {
        backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .black
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        titleTextView.font = titleFont
        titleTextView.textContainerInset = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
        titleTextView.layer.borderColor = UIColor.lightGray.cgColor
        titleTextView.layer.borderWidth = 1
        titleTextView.layer.cornerRadius = UIScreen.main.bounds.width*0.06
        
        titlePlaceholderLabel.text = "Write your title . . ."
        titlePlaceholderLabel.textColor = .gray
        titlePlaceholderLabel.font = titleFont
        
        maxCharLabel.text = "Max Character reached"
        maxCharLabel.textColor = .red
        maxCharLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))

        dateButton.customTitle = "Date"
        dateButton.addTarget(self, action: #selector(dateButtonPressed), for: .touchUpInside)
        
        timeLabel.text = "02:34"
        timeLabel.textColor = .black
        timeLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))

        progressBackgroundView.backgroundColor = .customGreen.withAlphaComponent(0.2)
        progressView.backgroundColor = .customGreen
        progressView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        audioControlButton.setImage(UIImage(systemName: "play"), for: .normal)
        audioControlButton.baseColor = .black
        audioControlButton.pinImageView()
        audioControlButton.imageView?.contentMode = .scaleAspectFit
        audioControlButton.addTarget(self, action: #selector(audioControlButtonPressed), for: .touchUpInside)
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.baseColor = .customGreen
        saveButton.highlightedColor = .customGreen.withAlphaComponent(0.3)
        saveButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
    }
    
    private func setUpGesture() {
        
        let dismissTap = UITapGestureRecognizer(target: self, action: #selector(dismissTapped))
        dismissTap.numberOfTapsRequired = 1
        addGestureRecognizer(dismissTap)
    }
    
    private func makeConstraint() {
        
        let topSpacing = UIScreen.main.bounds.height*0.05
        let sideSpacing = UIScreen.main.bounds.width*0.05
        let timeTopSpacing = UIScreen.main.bounds.height*0.03
        
        translatesAutoresizingMaskIntoConstraints = false
        
        progressRightConstraint = progressView.rightAnchor.constraint(equalTo: progressBackgroundView.leftAnchor)
        
        NSLayoutConstraint.activate([
                        
            backButton.topAnchor.constraint(equalTo: topAnchor),
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            titleTextView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: topSpacing),
            titleTextView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            titleTextView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.15),
            titleTextView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            titlePlaceholderLabel.leftAnchor.constraint(equalTo: titleTextView.leftAnchor, constant: 10+5),
            titlePlaceholderLabel.topAnchor.constraint(equalTo: titleTextView.topAnchor, constant: 15),
            
            maxCharLabel.topAnchor.constraint(equalTo: titleTextView.bottomAnchor, constant: 5),
            maxCharLabel.leftAnchor.constraint(equalTo: titleTextView.leftAnchor),
            
            dateButton.topAnchor.constraint(equalTo: titleTextView.bottomAnchor, constant: topSpacing),
            dateButton.leftAnchor.constraint(equalTo: titleTextView.leftAnchor),
            
            timeLabel.topAnchor.constraint(equalTo: dateButton.bottomAnchor, constant: topSpacing),
            timeLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            progressBackgroundView.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: timeTopSpacing),
            progressBackgroundView.widthAnchor.constraint(equalToConstant: progressWidth),
            progressBackgroundView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.01),
            progressBackgroundView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            progressView.topAnchor.constraint(equalTo: progressBackgroundView.topAnchor),
            progressView.leftAnchor.constraint(equalTo: progressBackgroundView.leftAnchor),
            progressView.bottomAnchor.constraint(equalTo: progressBackgroundView.bottomAnchor),
            progressRightConstraint,
            
            audioControlButton.topAnchor.constraint(equalTo: progressBackgroundView.bottomAnchor, constant: timeTopSpacing),
            audioControlButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            audioControlButton.heightAnchor.constraint(equalTo: audioControlButton.widthAnchor),
            audioControlButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            saveButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            saveButton.heightAnchor.constraint(equalTo: saveButton.widthAnchor, multiplier: 1/7),
            saveButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.95),
            saveButton.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func dismissTapped() {
        endEditing(true)
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func dateButtonPressed() {
        buttonDelegate.buttonPressed(for: .date)
    }
    
    @objc private func audioControlButtonPressed() {
        buttonDelegate.buttonPressed(for: .audioControl)
    }
    
    @objc private func saveButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }

}
