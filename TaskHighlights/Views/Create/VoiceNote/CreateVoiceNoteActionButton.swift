//
//  CreateVoiceNoteActionButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-20.
//

import UIKit

class CreateVoiceNoteActionButton: BackgroundButton {
    
    let size = UIScreen.main.bounds.width/4
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        baseColor = .clear
        highlightedColor = .clear
        
        layer.borderColor = UIColor.white.cgColor
        layer.cornerRadius = size/2
    }
    
    
    private func makeConstraint() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: size),
            heightAnchor.constraint(equalToConstant: size)
        ])
    }
    
}


extension CreateVoiceNoteActionButton {
    
    func setToIdleState() {
        baseColor = .red
        highlightedColor = .red.withAlphaComponent(0.4)
        layer.borderWidth = 0
    }
    
    func setToRecordingState() {
        baseColor = .clear
        highlightedColor = .clear
        layer.borderWidth = 4
    }


    func startAnimation() {
        
        UIView.animate(withDuration: 0.8, delay: 0, options: [.autoreverse, .repeat, .allowUserInteraction]){
            [unowned self] in
            transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        }
    }
    
    func stopAnimation() {
        layer.removeAllAnimations()
        transform = .identity
    }
    
}
