//
//  PhotoPreviewView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-15.
//

import UIKit

class PhotoPreviewView: UIView {
    
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let saveButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : PhotoPreviewButtonDelegate!

    var image : UIImage? {
        get { imageView.image }
        set { imageView.image = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(imageView)
        self.addSubview(backButton)
        self.addSubview(saveButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        saveButton.layer.cornerRadius = saveButton.frame.height/2
    }
    
    private func configureViews() {
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .white
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)

        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UIScreen.main.bounds.width*0.055
        
        saveButton.setTitle("Save", for: .normal)
        saveButton.setTitleTextPressColors(.white)
        saveButton.baseColor = .customGreen
        saveButton.highlightedColor = .customGreen.withAlphaComponent(0.3)
        saveButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        saveButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        let buttonWidth = UIScreen.main.bounds.width*0.25
        
        translatesAutoresizingMaskIntoConstraints = false
        
        backButton.pinImageView()
        
        NSLayoutConstraint.activate([
            
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 16/9),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
                        
            saveButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            saveButton.heightAnchor.constraint(equalTo: saveButton.widthAnchor, multiplier: 0.5),
            saveButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            saveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
        ])
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func nextButtonPressed() {
        buttonDelegate.buttonPressed(for: .save)
    }

}
