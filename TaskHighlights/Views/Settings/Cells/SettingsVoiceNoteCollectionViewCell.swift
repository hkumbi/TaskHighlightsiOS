//
//  SettingsVoiceNoteCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit


class SettingsVoiceNoteCollectionViewCell: UICollectionViewCell {
    
    static let cellSize = CGSize(width: width, height: height)
    static let spacing = UIScreen.main.bounds.width*0.1/3
    
    private static let width = UIScreen.main.bounds.width*0.45
    private static let height = width*3/4
    
    override var isHighlighted: Bool {
        didSet { dimmingView.alpha = isHighlighted ? 0.2 : 0 }
    }
    
    private let timeProgressView : UIView = .viewPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    private let playButton : IconButton = .iconPreppedForAutoLayout()
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    
    private var timeProgressRightConstraint : NSLayoutConstraint!
    
    var progress : CGFloat {
        get {
            let width = frame.width == 0 ? Self.width : frame.width
            return timeProgressRightConstraint.constant / width
        }
        
        set {
            let width = frame.width == 0 ? Self.width : frame.width
            let value = min(max(newValue, 0), 1)
            timeProgressRightConstraint.constant = value*width
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(timeProgressView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(timeLabel)
        self.contentView.addSubview(playButton)
        self.contentView.addSubview(dimmingView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        contentView.clipsToBounds = true
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.customGreen.cgColor
        contentView.layer.cornerRadius = 20
        
        timeProgressView.backgroundColor = .customGreen.withAlphaComponent(0.3)
        
        titleLabel.text = "THis is a title for a voice note"
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        
        timeLabel.text = "01:23"
        timeLabel.textColor = .black
        timeLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13))
        
        playButton.setImage(UIImage(systemName: "play"), for: .normal)
        playButton.imageView?.contentMode = .scaleAspectFit
        playButton.pinImageView()
        playButton.baseColor = .black
        
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func makeConstraints() {
        let quarterCenterX = Self.height/4
        
        dimmingView.pin(to: contentView)
        
        timeProgressRightConstraint = timeProgressView.rightAnchor.constraint(equalTo: timeProgressView.leftAnchor)
        
        NSLayoutConstraint.activate([
            timeProgressView.topAnchor.constraint(equalTo: contentView.topAnchor),
            timeProgressView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            timeProgressView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            timeProgressRightConstraint,
            
            titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.9),
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -quarterCenterX),
            
            timeLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            timeLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            playButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15),
            playButton.heightAnchor.constraint(equalTo: playButton.widthAnchor),
            playButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            playButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: quarterCenterX),
        ])
    }
    
}
