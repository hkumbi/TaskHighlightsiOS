//
//  SettingsHighlightsCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class SettingsHighlightsCollectionViewCell: UICollectionViewCell {
    
    static let cellSize = CGSize(width: width, height: height)
    
    private static let width = Int(UIScreen.main.bounds.width/3 - 1/3)
    private static let height = width*5/4
    
    override var isHighlighted: Bool {
        didSet { dimmingView.alpha = isHighlighted ? 0.4 : 0 }
    }
    
    private let imageView : UIImageView = .preppedForAutoLayout()
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    
    var image : UIImage? {
        get { imageView.image }
        set { imageView.image = newValue }
    }
    
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(dimmingView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        imageView.image = UIImage(named: "snow")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func makeConstraints() {
        
        imageView.pin(to: contentView)
        dimmingView.pin(to: contentView)
    }
    
}
