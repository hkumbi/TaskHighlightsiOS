//
//  SettingsOptionButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class SettingsOptionButton: BackgroundButton {
    
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let optionTitleLabel : UILabel = .preppedForAutoLayout()
    private let goIconView : UIImageView = .preppedForAutoLayout()
    
    var optionTitle : String {
        get { optionTitleLabel.text ?? "" }
        set { optionTitleLabel.text = newValue }
    }
    
    var icon : UIImage? {
        get { iconView.image }
        set { iconView.image = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(optionTitleLabel)
        self.addSubview(goIconView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        baseColor = .white
        highlightedColor = .black.withAlphaComponent(0.05)
        
        iconView.contentMode = .scaleAspectFit
        iconView.tintColor = .black
        
        optionTitleLabel.textColor = .black
        optionTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        
        goIconView.image = UIImage(systemName: "chevron.right", withConfiguration: UIImage.SymbolConfiguration(weight: .medium))
        goIconView.tintColor = .black
        goIconView.contentMode = .scaleAspectFit
    }
    
    private func makeConstraints() {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height*0.07
        let iconSize = UIScreen.main.bounds.width*0.07
        let goIconSize = UIScreen.main.bounds.width*0.05
        let spacing = UIScreen.main.bounds.width*0.07
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: height),
            
            iconView.widthAnchor.constraint(equalToConstant: iconSize),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            optionTitleLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: spacing),
            optionTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            goIconView.widthAnchor.constraint(equalToConstant: goIconSize),
            goIconView.heightAnchor.constraint(equalTo: goIconView.widthAnchor),
            goIconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            goIconView.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
        ])
    }
    
}
