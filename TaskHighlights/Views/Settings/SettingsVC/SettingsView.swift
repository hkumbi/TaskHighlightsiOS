//
//  SettingsView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class SettingsView: UIView {
    
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let settingsTitleLabel : UILabel = .preppedForAutoLayout()
    private let highlightsButton = SettingsOptionButton()
    private let voicesNoteButton = SettingsOptionButton()
    private let faqButton = SettingsOptionButton()
    private let temperatureUnitButton = SettingsTemperatureUnitView()
    private let deleteDataButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    
    unowned var buttonDelegate : SettingsButtonDelegate! {
        didSet { temperatureUnitButton.buttonDelegate = buttonDelegate }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(backButton)
        self.addSubview(settingsTitleLabel)
        self.addSubview(highlightsButton)
        self.addSubview(voicesNoteButton)
        self.addSubview(faqButton)
        self.addSubview(temperatureUnitButton)
        self.addSubview(deleteDataButton)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        deleteDataButton.layer.cornerRadius = deleteDataButton.frame.height/2
    }
    
    private func configureViews() {
        
        backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .black
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        settingsTitleLabel.text = "Settings"
        settingsTitleLabel.textColor = .black
        settingsTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        
        highlightsButton.icon = UIImage(systemName: "camera")
        highlightsButton.optionTitle = "View All Highlights"
        highlightsButton.addTarget(self, action: #selector(highlightsButtonPressed), for: .touchUpInside)
        
        voicesNoteButton.icon = UIImage(systemName: "mic")
        voicesNoteButton.optionTitle = "View All Voice Notes"
        voicesNoteButton.addTarget(self, action: #selector(voiceNotesButtonPressed), for: .touchDown)
        
        faqButton.icon = UIImage(systemName: "questionmark")
        faqButton.optionTitle = "FAQ"
        faqButton.addTarget(self, action: #selector(faqButtonPressed), for: .touchUpInside)
        
        deleteDataButton.setTitle("Delete My Data", for: .normal)
        deleteDataButton.setTitleTextPressColors(.white)
        deleteDataButton.baseColor = .red
        deleteDataButton.highlightedColor = .red.withAlphaComponent(0.5)
        deleteDataButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .bold))
        deleteDataButton.addTarget(self, action: #selector(deleteDataButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let topSpacing = UIScreen.main.bounds.height*0.02
        let spacing = UIScreen.main.bounds.width*0.05
        
        NSLayoutConstraint.activate([
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: settingsTitleLabel.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            settingsTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            settingsTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
                        
            highlightsButton.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: topSpacing),
            highlightsButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            voicesNoteButton.topAnchor.constraint(equalTo: highlightsButton.bottomAnchor, constant: topSpacing),
            voicesNoteButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            faqButton.topAnchor.constraint(equalTo: voicesNoteButton.bottomAnchor, constant: topSpacing),
            faqButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            temperatureUnitButton.topAnchor.constraint(equalTo: faqButton.bottomAnchor, constant: topSpacing),
            temperatureUnitButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            deleteDataButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.6),
            deleteDataButton.heightAnchor.constraint(equalTo: deleteDataButton.widthAnchor, multiplier: 0.22),
            deleteDataButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            deleteDataButton.bottomAnchor.constraint(equalTo: bottomAnchor),

        ])
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.buttonPressed(for: .back)
    }
    
    @objc private func highlightsButtonPressed() {
        buttonDelegate.buttonPressed(for: .highlights)
    }
    
    @objc private func voiceNotesButtonPressed() {
        buttonDelegate.buttonPressed(for: .voiceNotes)
    }
    
    @objc private func faqButtonPressed() {
        buttonDelegate.buttonPressed(for: .faq)
    }
    
    @objc private func deleteDataButtonPressed() {
        buttonDelegate.buttonPressed(for: .deleteMyData)
    }

}
