//
//  SettingsTemperatureUnitView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class SettingsTemperatureUnitView: UIView {
    
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let optionTitleLabel : UILabel = .preppedForAutoLayout()
    private let unitSegmentedControl : UISegmentedControl = .preppedForAutoLayout()
    
    unowned var buttonDelegate : SettingsButtonDelegate!

    var selectedIndex : Int {
        get { unitSegmentedControl.selectedSegmentIndex }
        set { unitSegmentedControl.selectedSegmentIndex = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(optionTitleLabel)
        self.addSubview(unitSegmentedControl)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        iconView.image = UIImage(systemName: "thermometer.sun")
        iconView.tintColor = .black
        iconView.contentMode = .scaleAspectFit
        
        optionTitleLabel.text = "Temperature Unit"
        optionTitleLabel.textColor = .black
        optionTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
                
        unitSegmentedControl.insertSegment(withTitle: "Cº", at: 0, animated: false)
        unitSegmentedControl.insertSegment(withTitle: "Fº", at: 1, animated: false)
        unitSegmentedControl.selectedSegmentIndex = 0
        unitSegmentedControl.addTarget(self, action: #selector(unitValueChanged), for: .valueChanged)
    }
    
    private func makeConstraints() {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height*0.07
        let iconSize = UIScreen.main.bounds.width*0.07

        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: height),
            
            iconView.widthAnchor.constraint(equalToConstant: iconSize),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: iconSize),
            
            optionTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            optionTitleLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: iconSize),
            
            unitSegmentedControl.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.3),
            unitSegmentedControl.centerYAnchor.constraint(equalTo: centerYAnchor),
            unitSegmentedControl.rightAnchor.constraint(equalTo: rightAnchor, constant: -iconSize),
        ])
    }
    
    @objc private func unitValueChanged() {
        buttonDelegate.tempUnitChanged(unitSegmentedControl.selectedSegmentIndex)
    }

}
