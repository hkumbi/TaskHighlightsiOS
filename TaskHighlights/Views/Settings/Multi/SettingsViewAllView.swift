//
//  SettingsViewAllView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class SettingsViewAllView: UIView {

    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let viewTitleLabel : UILabel = .preppedForAutoLayout()
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    unowned var buttonDelegate : ViewAllButtonDelegate!
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(backButton)
        self.addSubview(viewTitleLabel)
        self.addSubview(collectionView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor),
        ])
    }
    
    private func configureViews() {
        
        backgroundColor = .white
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .black
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        viewTitleLabel.text = "View All Highlights"
        viewTitleLabel.textColor = .black
        viewTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
        
        collectionView.alwaysBounceVertical = true
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.centerYAnchor.constraint(equalTo: viewTitleLabel.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            viewTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: spacing),
            viewTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            collectionView.topAnchor.constraint(equalTo: viewTitleLabel.bottomAnchor, constant: spacing),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.backButtonPressed()
    }

}
