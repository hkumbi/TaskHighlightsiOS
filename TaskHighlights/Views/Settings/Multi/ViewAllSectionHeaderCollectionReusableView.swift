//
//  ViewAllSectionHeaderCollectionReusableView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-27.
//

import UIKit

class ViewAllSectionHeaderCollectionReusableView: UICollectionReusableView {
    
    static let size = CGSize(width: UIScreen.main.bounds.width, height: height)
    
    private static let height = UIScreen.main.bounds.height*0.07
    
    private let dateLabel : UILabel = .preppedForAutoLayout()
    
    var date : String {
        get { dateLabel.text ?? "" }
        set { dateLabel.text = newValue }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.addSubview(dateLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        
        dateLabel.textColor = .black
        dateLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        NSLayoutConstraint.activate([
            dateLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            dateLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
}
