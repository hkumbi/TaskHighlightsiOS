//
//  BackgroundButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class BackgroundButton: UIButton {
    
    override var isEnabled: Bool {
        didSet { backgroundColor = isEnabled ? baseColor : baseColor.withAlphaComponent(0.4) }
    }

    override var isHighlighted: Bool {
        didSet { backgroundColor = isHighlighted ? highlightedColor : baseColor }
    }
    
    var baseColor : UIColor = .white {
        didSet { backgroundColor = baseColor }
    }
    
    var highlightedColor : UIColor = .white

}
