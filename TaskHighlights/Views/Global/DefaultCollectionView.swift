//
//  DefaultCollectionView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class DefaultCollectionView: UICollectionView {

    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewLayout())
        
        self.backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view.isKind(of: UIButton.self) {
          return true
        }

        return super.touchesShouldCancel(in: view)
    }

}
