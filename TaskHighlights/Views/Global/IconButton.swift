//
//  IconButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class IconButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet { tintColor = isHighlighted ? highlightedColor : baseColor  }
    }

    var baseColor : UIColor = .black {
        didSet {
            tintColor = baseColor
            highlightedColor = baseColor.withAlphaComponent(0.5)
        }
    }
    
    private var highlightedColor : UIColor = .black.withAlphaComponent(0.5)

}
