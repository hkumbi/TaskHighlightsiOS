//
//  GradientLayerView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-20.
//

import UIKit

class GradientLayerView: UIView {
    
    override class var layerClass: AnyClass { CAGradientLayer.self }
    
    var gradientLayer : CAGradientLayer {
        layer as! CAGradientLayer
    }

    init() {
        super.init(frame: .zero)
        
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        let topColor = UIColor.black.withAlphaComponent(0.4).cgColor
        let bottomColor = UIColor.black.cgColor

        gradientLayer.locations = [0, 1]
        gradientLayer.colors = [topColor, bottomColor]
        gradientLayer.startPoint = .zero
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        print("Klojihugvh")
        translatesAutoresizingMaskIntoConstraints = false
    }

}
