//
//  DefaultTableView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DefaultTableView: UITableView {
    
    init() {
        super.init(frame: .zero, style: .plain)
        
        self.backgroundColor = .white
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        
        return super.touchesShouldCancel(in: view)
    }

}
