//
//  DayTaskModalContentView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-09-02.
//

import UIKit

class DayTaskModalContentView: UIView {

    private let trashButton : IconButton = .iconPreppedForAutoLayout()
    private let editButton : IconButton = .iconPreppedForAutoLayout()
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    
    private let iconSize = UIScreen.main.bounds.width*0.07
    private let topSpacing = UIScreen.main.bounds.height*0.02
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(trashButton)
        self.addSubview(editButton)
        self.addSubview(closeButton)
        self.addSubview(titleLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        trashButton.setImage(UIImage(systemName: "trash"), for: .normal)
        trashButton.imageView?.contentMode = .scaleAspectFit
        trashButton.pinImageView()
        trashButton.baseColor = .black
        
        editButton.setImage(UIImage(systemName: "pencil"), for: .normal)
        editButton.imageView?.contentMode = .scaleAspectFit
        editButton.pinImageView()
        editButton.baseColor = .black
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.pinImageView()
        closeButton.baseColor = .black
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            trashButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            trashButton.widthAnchor.constraint(equalToConstant: iconSize),
            trashButton.heightAnchor.constraint(equalTo: trashButton.widthAnchor),
            trashButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            editButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            editButton.widthAnchor.constraint(equalToConstant: iconSize),
            editButton.heightAnchor.constraint(equalTo: editButton.widthAnchor),
            editButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            closeButton.widthAnchor.constraint(equalToConstant: iconSize),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            titleLabel.topAnchor.constraint(equalTo: editButton.bottomAnchor, constant: topSpacing),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }

}
