//
//  DayTaskView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import UIKit

class DayTaskView: UIView {

    private let topSpacerView : UIView = .viewPreppedForAutoLayout()
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    private let viewTitleLabel : UILabel = .preppedForAutoLayout()
    private let dateLabel : UILabel = .preppedForAutoLayout()
    let tableView : DefaultTableView = .defaultPreppedForAutoLayout()
    
    var date : String {
        get { dateLabel.text ?? "" }
        set { dateLabel.text = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(topSpacerView)
        self.addSubview(backButton)
        self.addSubview(viewTitleLabel)
        self.addSubview(dateLabel)
        self.addSubview(tableView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor),
            leftAnchor.constraint(equalTo: superview.leftAnchor),
            rightAnchor.constraint(equalTo: superview.rightAnchor),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor)
        ])
    }
    
    private func configureViews() {
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.baseColor = .black
        
        viewTitleLabel.text = "Tasks"
        viewTitleLabel.textColor = .black
        viewTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .bold))
        
        dateLabel.textColor = .black
        dateLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            topSpacerView.topAnchor.constraint(equalTo: viewTitleLabel.topAnchor),
            topSpacerView.bottomAnchor.constraint(equalTo: dateLabel.bottomAnchor),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            backButton.centerYAnchor.constraint(equalTo: topSpacerView.centerYAnchor),
            
            viewTitleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            viewTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            dateLabel.topAnchor.constraint(equalTo: viewTitleLabel.bottomAnchor, constant: 10),
            dateLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            tableView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: spacing),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc private func backButtonPressed() {
        
    }

}
