//
//  DayHighlightContentView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DayHighlightContentView: UIView {
    
    private let backButton : IconButton = .iconPreppedForAutoLayout()
    let imageCollectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    let previewCollectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    
    unowned var buttonDelegate : DayContentButtonDelegate!
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(imageCollectionView)
        self.addSubview(previewCollectionView)
        self.addSubview(backButton)
        
        makeConstraints()
        screenBasedConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
    
    private func configureViews() {
        backgroundColor = .black
        
        imageCollectionView.backgroundColor = .black
        imageCollectionView.alwaysBounceHorizontal = true
        imageCollectionView.isPagingEnabled = true
        imageCollectionView.showsHorizontalScrollIndicator = false
        
        backButton.setImage(UIImage(systemName: "arrow.left"), for: .normal)
        backButton.baseColor = .white
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.pinImageView()
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        
        previewCollectionView.backgroundColor = .clear
        previewCollectionView.showsHorizontalScrollIndicator = false
        previewCollectionView.alwaysBounceHorizontal = true
    }
    
    private func makeConstraints() {
        
        let spacing = UIScreen.main.bounds.width*0.05
        
        NSLayoutConstraint.activate([
            imageCollectionView.leftAnchor.constraint(equalTo: leftAnchor),
            imageCollectionView.rightAnchor.constraint(equalTo: rightAnchor),
            imageCollectionView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 16/9),
            
            backButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            backButton.heightAnchor.constraint(equalTo: backButton.widthAnchor),
            backButton.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: spacing),
            backButton.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            previewCollectionView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.08),
            previewCollectionView.widthAnchor.constraint(equalTo: widthAnchor),
            previewCollectionView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            previewCollectionView.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    private func screenBasedConstraint() {
        
        var topConstraint : NSLayoutConstraint
        let screenRatio = round(UIScreen.main.bounds.height/UIScreen.main.bounds.width)
        let isLargeScreen = screenRatio > round(CGFloat(16)/CGFloat(9))
        
        if isLargeScreen {
            topConstraint = imageCollectionView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor)
        } else {
            topConstraint = imageCollectionView.topAnchor.constraint(equalTo: topAnchor)
        }
        
        topConstraint.isActive = true
    }
    
    @objc private func backButtonPressed() {
        buttonDelegate.backButtonPressed()
    }
    
}
