//
//  DayVoiceNoteModalContentView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import UIKit

class DayVoiceNoteModalContentView: UIView {

    private let deleteButton : IconButton = .iconPreppedForAutoLayout()
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let audioActionButton : IconButton = .iconPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let timeProgressView : UIView = .viewPreppedForAutoLayout()
    
    private let minimumHeight : CGFloat
    private let topSpacing = UIScreen.main.bounds.height*0.03
    private let iconSize = UIScreen.main.bounds.width*0.07
    private let contentWidth = UIScreen.main.bounds.width*0.9
    private let progressHeight = UIScreen.main.bounds.height*0.01
    
    private var heightConstraint : NSLayoutConstraint!
    private var timeProgressRightConstraint : NSLayoutConstraint!
    private var titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    private var titleHeight : CGFloat = 0

    unowned var buttonDelegate : DayVoiceNoteModalButtonDelegate!
    
    var bottomSafeAreaConstant : CGFloat = 0 {
        didSet { updateHeight() }
    }
    
    var height : CGFloat {
        get { heightConstraint.constant }
        set { heightConstraint.constant = newValue }
    }
    
    var title : String {
        get { titleLabel.text ?? "" }
        set {
            let height = String.getLabelHeight(text: newValue, font: titleFont)
            titleHeight = height
            titleLabel.text = newValue
            updateHeight()
        }
    }
    
    var progress : CGFloat {
        get { timeProgressRightConstraint.constant / contentWidth }
        set {
            let value = min(max(newValue, 0), 1)
            timeProgressRightConstraint.constant = value*contentWidth
        }
    }
    
    
    init() {
        
        minimumHeight = iconSize + progressHeight + topSpacing*3
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(deleteButton)
        self.addSubview(audioActionButton)
        self.addSubview(closeButton)
        self.addSubview(titleLabel)
        self.addSubview(timeProgressView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        backgroundColor = .white
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        deleteButton.setImage(UIImage(systemName: "trash"), for: .normal)
        deleteButton.imageView?.contentMode = .scaleAspectFit
        deleteButton.pinImageView()
        deleteButton.baseColor = .black
        deleteButton.addTarget(self, action: #selector(deleteButtonPressed), for: .touchUpInside)
        
        audioActionButton.setImage(UIImage(systemName: "play"), for: .normal)
        audioActionButton.imageView?.contentMode = .scaleAspectFit
        audioActionButton.pinImageView()
        audioActionButton.baseColor = .black
        audioActionButton.addTarget(self, action: #selector(audioActionButtonPressed), for: .touchUpInside)
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.pinImageView()
        closeButton.baseColor = .black
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        titleLabel.textAlignment = .center
        titleLabel.textColor = .black
        titleLabel.font = titleFont
        
        timeProgressView.backgroundColor = .customGreen
        timeProgressView.layer.cornerRadius = progressHeight/2
    }
    
    private func makeConstraints() {
        
        let sideSpacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: minimumHeight)
        timeProgressRightConstraint =             timeProgressView.rightAnchor.constraint(equalTo: timeProgressView.leftAnchor)

        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            deleteButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            deleteButton.heightAnchor.constraint(equalTo: deleteButton.widthAnchor),
            deleteButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            deleteButton.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            
            audioActionButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            audioActionButton.heightAnchor.constraint(equalTo: audioActionButton.widthAnchor),
            audioActionButton.centerYAnchor.constraint(equalTo: deleteButton.centerYAnchor),
            audioActionButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            closeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.centerYAnchor.constraint(equalTo: deleteButton.centerYAnchor),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            titleLabel.topAnchor.constraint(equalTo: deleteButton.bottomAnchor, constant: topSpacing),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: contentWidth),
            
            timeProgressView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: topSpacing),
            timeProgressView.heightAnchor.constraint(equalToConstant: progressHeight),
            timeProgressView.leftAnchor.constraint(equalTo: leftAnchor, constant: sideSpacing),
            timeProgressRightConstraint
        ])
    }
    
    private func updateHeight() {
        heightConstraint.constant = titleHeight + minimumHeight + bottomSafeAreaConstant
    }

    @objc private func deleteButtonPressed() {
        buttonDelegate.buttonPressed(for: .delete)
    }
    
    @objc private func audioActionButtonPressed() {
        buttonDelegate.buttonPressed(for: .audioAction)
    }
    
    @objc private func closeButtonPressed() {
        buttonDelegate.buttonPressed(for: .close)
    }
}

