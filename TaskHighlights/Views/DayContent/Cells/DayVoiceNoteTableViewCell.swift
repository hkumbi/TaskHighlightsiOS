//
//  DayVoiceNoteTableViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DayVoiceNoteTableViewCell: UITableViewCell {
    
    static let height = contentHeight + spacing
    
    private static let contentWidth = UIScreen.main.bounds.width*0.95
    private static let contentHeight = UIScreen.main.bounds.height*0.08
    private static let spacing = UIScreen.main.bounds.width*0.05
    
    private let containerButton: BackgroundButton = .backgroundPreppedForAutoLayout()
    private let progressView : UIView = .viewPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    private let audioActionButton : IconButton = .iconPreppedForAutoLayout()
    
    private let timeFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 13))
    private var progressRightConstraint : NSLayoutConstraint!
    
    var id : UUID!
    
    unowned var buttonDelegate : DayVoiceNoteCellButtonDelegate!
    
    var progress : CGFloat {
        get { progressRightConstraint.constant / Self.contentWidth }
        set {
            let value = min(max(0, newValue), 1)
            progressRightConstraint.constant = Self.contentWidth*value
        }
    }
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureViews()
        
        self.contentView.addSubview(containerButton)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(timeLabel)
        self.contentView.addSubview(audioActionButton)
        
        self.containerButton.addSubview(progressView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        selectionStyle = .none
        
        containerButton.layer.cornerRadius = Self.contentHeight/2
        containerButton.layer.borderWidth = 1
        containerButton.layer.borderColor = UIColor.customGreen.cgColor
        containerButton.clipsToBounds = true
        containerButton.baseColor = .white
        containerButton.highlightedColor = .black.withAlphaComponent(0.1)
        containerButton.addTarget(self, action: #selector(containerButtonPressed), for: .touchUpInside)
        
        progressView.isUserInteractionEnabled = false
        progressView.backgroundColor = .customGreen.withAlphaComponent(0.2)
        
        titleLabel.text = "Hello there, Hello there, Hello there, Hello there, Hello there"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
        
        timeLabel.text = "01:23"
        timeLabel.textColor = .black
        timeLabel.textAlignment = .center
        timeLabel.font = timeFont
        
        audioActionButton.setImage(UIImage(systemName: "play"), for: .normal)
        audioActionButton.imageView?.contentMode = .scaleAspectFit
        audioActionButton.pinImageView()
        audioActionButton.baseColor = .black
        audioActionButton.addTarget(self, action: #selector(audioActionButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        
        let timeWidth = String.getLabelWidth(text: "00:00", font: timeFont)
        
        progressRightConstraint = progressView.rightAnchor.constraint(equalTo: containerButton.leftAnchor)

        NSLayoutConstraint.activate([
            
            containerButton.widthAnchor.constraint(equalToConstant: Self.contentWidth),
            containerButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, constant: -Self.spacing),
            containerButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            containerButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            progressView.topAnchor.constraint(equalTo: containerButton.topAnchor),
            progressView.leftAnchor.constraint(equalTo: containerButton.leftAnchor),
            progressView.bottomAnchor.constraint(equalTo: containerButton.bottomAnchor),
            progressRightConstraint,
            
            titleLabel.leftAnchor.constraint(equalTo: containerButton.leftAnchor, constant: Self.spacing),
            titleLabel.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            titleLabel.rightAnchor.constraint(equalTo: timeLabel.leftAnchor, constant: -Self.spacing),
            
            timeLabel.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            timeLabel.rightAnchor.constraint(equalTo: audioActionButton.leftAnchor, constant: -10),
            timeLabel.widthAnchor.constraint(equalToConstant: timeWidth),
            
            audioActionButton.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3),
            audioActionButton.widthAnchor.constraint(equalTo: audioActionButton.heightAnchor),
            audioActionButton.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            audioActionButton.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Self.spacing ),
        ])
    }
    
    @objc private func containerButtonPressed() {
        buttonDelegate.cellPressed(id)
    }

    @objc private func audioActionButtonPressed() {
        buttonDelegate.audioActionButtonPressed(id)
    }
    
}
