//
//  DayTaskTableViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-29.
//

import UIKit

class DayTaskTableViewCell: UITableViewCell {
    
    static let height = contentHeight
    
    private static let contentHeight = UIScreen.main.bounds.height*0.09
    private static let contentWidth = UIScreen.main.bounds.width*0.9
    private static let spacing = UIScreen.main.bounds.width*0.05
    
    private let containerButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let warningIconView : UIImageView = .preppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let completedButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let bottomLineView : UIView = .viewPreppedForAutoLayout()
    
    var isLastRow : Bool {
        get { bottomLineView.isHidden }
        set { bottomLineView.isHidden = newValue }
    }
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureViews()
        
        self.contentView.addSubview(containerButton)
        self.contentView.addSubview(warningIconView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(completedButton)
        
        self.containerButton.addSubview(bottomLineView)
        
        makeConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
                
        containerButton.baseColor = .white
        containerButton.highlightedColor = .black.withAlphaComponent(0.05)
        containerButton.clipsToBounds = true
        
        warningIconView.image = UIImage(systemName: "exclamationmark.triangle")
        warningIconView.contentMode = .scaleAspectFit
        warningIconView.tintColor = .red
        
        titleLabel.text = "Hello"
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .medium))
        
        completedButton.setImage(UIImage(systemName: "checkmark"), for: .normal)
        completedButton.tintColor = .white
        completedButton.baseColor = .white
        completedButton.highlightedColor = .customGreen.withAlphaComponent(0.8)
        completedButton.layer.borderColor = UIColor.customGreen.cgColor
        completedButton.layer.borderWidth = 1
        completedButton.addTarget(self, action: #selector(completedButtonPressed), for: .touchUpInside)
        
        bottomLineView.backgroundColor = .black.withAlphaComponent(0.2)
    }
    
    private func makeConstraints() {

        let buttonSize = Self.contentHeight*0.5
        let bottomLineHeight : CGFloat = 1
        
        completedButton.layer.cornerRadius = buttonSize/2
        
        NSLayoutConstraint.activate([
            containerButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -bottomLineHeight),
            containerButton.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            containerButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            containerButton.heightAnchor.constraint(equalToConstant: Self.contentHeight),
            
            warningIconView.heightAnchor.constraint(equalTo: containerButton.heightAnchor, multiplier: 0.3),
            warningIconView.widthAnchor.constraint(equalTo: warningIconView.heightAnchor),
            warningIconView.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            warningIconView.leftAnchor.constraint(equalTo: containerButton.leftAnchor, constant: Self.spacing),
            
            titleLabel.leftAnchor.constraint(equalTo: warningIconView.rightAnchor, constant: Self.spacing),
            titleLabel.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            
            completedButton.widthAnchor.constraint(equalToConstant: buttonSize),
            completedButton.heightAnchor.constraint(equalTo: completedButton.widthAnchor),
            completedButton.centerYAnchor.constraint(equalTo: containerButton.centerYAnchor),
            completedButton.rightAnchor.constraint(equalTo: containerButton.rightAnchor, constant: -Self.spacing),
            
            bottomLineView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.4),
            bottomLineView.heightAnchor.constraint(equalToConstant: bottomLineHeight),
            bottomLineView.centerXAnchor.constraint(equalTo: containerButton.centerXAnchor),
            bottomLineView.bottomAnchor.constraint(equalTo: containerButton.bottomAnchor),
        ])
    }
    
    var completed : Bool = false
    
    @objc private func completedButtonPressed() {
        completedButton.baseColor = completed ? .customGreen : .white
        completed = !completed
    }

}
