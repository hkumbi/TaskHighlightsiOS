//
//  DayHighlightCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-28.
//

import UIKit

class DayHighlightCollectionViewCell: UICollectionViewCell {
    
    static let cellSize = CGSize(width: width, height: height)
    static let previewCellSize = CGSize(width: previewWidth, height: previewHeight)
    static let spacing : CGFloat = 10
    
    private static let width = UIScreen.main.bounds.width
    private static let height = width*16/9
    private static let previewWidth = previewHeight*9/16
    private static let previewHeight = UIScreen.main.bounds.height*0.07
    
    override var isSelected: Bool {
        didSet { contentView.layer.borderWidth = isSelected ? 1 : 0 }
    }
    
    private let imageView : UIImageView = .preppedForAutoLayout()
    
    var isPreviewCell : Bool {
        get { contentView.layer.cornerRadius == Self.previewWidth*0.1 }
        set {
            let radius = newValue ? Self.previewWidth*0.2 : Self.width*0.05
            contentView.layer.cornerRadius = radius
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(imageView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        contentView.layer.borderWidth = 0
    }
    
    private func configureViews() {
        
        contentView.clipsToBounds = true
        contentView.layer.borderColor = UIColor.customGreen.cgColor

        imageView.image = UIImage(named: "skiresort")
        imageView.contentMode = .scaleAspectFill
    }
    
    private func makeConstraints() {
        
        imageView.pin(to: contentView)
    }
    
}
