//
//  CreateView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import UIKit

class CreateView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.6
    
    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = CreateModalView()
    
    private var modalBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : CreateButtonDelegate!

    var modalBottomConstant : CGFloat {
        get { modalBottomConstraint.constant }
        set { modalBottomConstraint.constant = newValue }
    }
    
    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    

    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
    
    private func configureViews() {
        
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func setUpGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    private func makeConstraint() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        dimmingView.pin(to: self)
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: modalView.heightConstant)
        
        NSLayoutConstraint.activate([
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
            modalBottomConstraint
        ])
    }

    @objc private func dimmingViewTapped() {
        buttonDelegate.buttonPressed(for: .cancel)
    }

}


extension CreateView {
    
    func animateOpen() {
        
        layoutIfNeeded()

        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            dimmingAlpha = Self.maxDimmingAlpha
            modalBottomConstant = 0
            layoutIfNeeded()
        }
    }
    
    func animateClose(completionHandler : @escaping () -> Void) {
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveLinear){
            [unowned self] in
            
            dimmingAlpha = 0
            modalBottomConstant = modalView.heightConstant
            layoutIfNeeded()
        } completion: {_ in
            completionHandler()
        }
    }
    
}
