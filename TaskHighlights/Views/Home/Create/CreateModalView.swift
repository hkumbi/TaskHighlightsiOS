//
//  CreateModalView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import UIKit

class CreateModalView: UIView {
    
    private let highlightsButton = CreateSelectionButton()
    private let voiceNotesButton = CreateSelectionButton()
    private let taskButton = CreateSelectionButton()
    private let cancelButton : UIButton = .preppedForAutoLayout()
    
    private var heightConstraint : NSLayoutConstraint!
    
    private let height : CGFloat
    private let spacing = UIScreen.main.bounds.height*0.015
    
    private let cancelFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .semibold))
    
    unowned var buttonDelegate : CreateButtonDelegate!
    
    var heightConstant : CGFloat {
        get { heightConstraint.constant }
    }

    init() {
        
        let buttonHeight = String.getLabelHeight(text: "", font: cancelFont)
        height = CreateSelectionButton.height*3 + spacing*6 + buttonHeight
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(highlightsButton)
        self.addSubview(voiceNotesButton)
        self.addSubview(taskButton)
        self.addSubview(cancelButton)
        
        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        heightConstraint.constant = height + safeAreaInsets.bottom
    }
    
    private func configureViews() {
        backgroundColor = .white
        clipsToBounds = true
        layer.cornerRadius = UIScreen.main.bounds.width*0.055
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        highlightsButton.icon = UIImage(systemName: "wand.and.stars")
        highlightsButton.text = "Photo Highlight"
        highlightsButton.addTarget(self, action: #selector(highlightsButtonPressed), for: .touchUpInside)
        
        voiceNotesButton.icon = UIImage(systemName: "mic")
        voiceNotesButton.text = "Voice Note"
        voiceNotesButton.addTarget(self, action: #selector(voiceNotesButtonPressed), for: .touchUpInside)
        
        taskButton.icon = UIImage(systemName: "list.number")
        taskButton.text = "Task"
        taskButton.addTarget(self, action: #selector(taskButtonPressed), for: .touchUpInside)
                
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleTextPressColors(.black)
        cancelButton.titleLabel?.font = cancelFont
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraint() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            highlightsButton.topAnchor.constraint(equalTo: topAnchor, constant: spacing*2),
            highlightsButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            voiceNotesButton.topAnchor.constraint(equalTo: highlightsButton.bottomAnchor, constant: spacing),
            voiceNotesButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            taskButton.topAnchor.constraint(equalTo: voiceNotesButton.bottomAnchor, constant: spacing),
            taskButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            cancelButton.topAnchor.constraint(equalTo: taskButton.bottomAnchor, constant: spacing),
            cancelButton.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func highlightsButtonPressed() {
        buttonDelegate.buttonPressed(for: .photoHighlight)
    }
    
    @objc private func voiceNotesButtonPressed() {
        buttonDelegate.buttonPressed(for: .voiceNote)
    }
    
    @objc private func taskButtonPressed() {
        buttonDelegate.buttonPressed(for: .task)
    }
    
    @objc private func cancelButtonPressed() {
        buttonDelegate.buttonPressed(for: .cancel)
    }

}
