//
//  CreateSelectionButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-13.
//

import UIKit

class CreateSelectionButton: UIButton {
    
    static let height = UIScreen.main.bounds.height*0.06
    
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? .black.withAlphaComponent(0.05) : .white
        }
    }
    
    private let iconView : UIImageView = .preppedForAutoLayout()
    private let textLabel : UILabel = .preppedForAutoLayout()
    
    var icon : UIImage? {
        get { iconView.image }
        set { iconView.image = newValue }
    }
    
    var text : String {
        get { textLabel.text ?? "" }
        set { textLabel.text = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(textLabel)
        
        makeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
                
        iconView.tintColor = .black
        iconView.contentMode = .scaleAspectFit
        
        textLabel.textColor = .black
        textLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraint() {
        let width = UIScreen.main.bounds.width
        let spacing = UIScreen.main.bounds.width*0.1
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalToConstant: Self.height),
            
            iconView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.7),
            iconView.widthAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            textLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: spacing),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }

}
