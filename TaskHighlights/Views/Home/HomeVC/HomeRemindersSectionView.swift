//
//  HomeRemindersSectionView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class HomeRemindersSectionView: UIView {

    private let sectionTitleLabel : UILabel = .preppedForAutoLayout()
    private let backgroundButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let timeLabel : UILabel = .preppedForAutoLayout()
    
    unowned var buttonDelegate : HomeButtonDelegate!
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(sectionTitleLabel)
        self.addSubview(backgroundButton)
        self.addSubview(titleLabel)
        self.addSubview(timeLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundButton.layer.cornerRadius = backgroundButton.frame.width/5
    }
    
    private func configureViews() {
                
        sectionTitleLabel.text = "Reminders"
        sectionTitleLabel.textColor = .black
        sectionTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        backgroundButton.highlightedColor = .black.withAlphaComponent(0.05)
        backgroundButton.layer.borderColor = UIColor.lightGray.cgColor
        backgroundButton.layer.borderWidth = 1
        backgroundButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        titleLabel.text = "This is a reminder"
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .medium))
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        
        timeLabel.text = "01:28:56"
        timeLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
        timeLabel.textAlignment = .center
        timeLabel.textColor = .black
    }
    
    private func makeConstraints() {
        
        let width = UIScreen.main.bounds.width*0.4
        let height = width*1.25
        let reminderHeight = height*0.8
        
        
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1.25),

            sectionTitleLabel.topAnchor.constraint(equalTo: topAnchor),
            sectionTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),

            backgroundButton.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8),
            backgroundButton.widthAnchor.constraint(equalTo: widthAnchor),
            backgroundButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            backgroundButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: backgroundButton.topAnchor, constant: reminderHeight*0.1),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: reminderHeight*0.6),
            
            timeLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            timeLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -reminderHeight*0.1),
            timeLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
        ])
    }
    
    @objc private func buttonPressed() {
        buttonDelegate.buttonPressed(for: .remindersSection)
    }
    
}
