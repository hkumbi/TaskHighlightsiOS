//
//  HomeHighlightsSectionView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class HomeHighlightsSectionView: UIView {

    private let sectionTitleLabel : UILabel = .preppedForAutoLayout()
    private let thumbnailButton : UIButton = .preppedForAutoLayout()
    private let noImageIconView : UIImageView = .preppedForAutoLayout()
    private let noImageLabel : UILabel = .preppedForAutoLayout()
    
    unowned var buttonDelegate : HomeButtonDelegate!
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(sectionTitleLabel)
        self.addSubview(thumbnailButton)
        self.addSubview(noImageIconView)
        self.addSubview(noImageLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbnailButton.layer.cornerRadius = thumbnailButton.frame.width/5
    }
    
    private func configureViews() {
        
        thumbnailButton.addTarget(self, action: #selector(thumbnailButtonPressed), for: .touchUpInside)
        
        sectionTitleLabel.text = "Highlights"
        sectionTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))
        
        thumbnailButton.setImage(UIImage(named: "winter"), for: .normal)
        thumbnailButton.imageView?.contentMode = .scaleAspectFill
        thumbnailButton.clipsToBounds = true
        
        noImageIconView.image = UIImage(systemName: "sparkles.rectangle.stack")
        noImageIconView.tintColor = .darkGray
        noImageIconView.contentMode = .scaleAspectFit
        noImageIconView.isHidden = true
        
        noImageLabel.text = "No Highlights"
        noImageLabel.textColor = .darkGray
        noImageLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 14))
        noImageLabel.isHidden = true
    }

    private func makeConstraints() {
        
        let width = UIScreen.main.bounds.width*0.4
        let spacing = width*0.1

        translatesAutoresizingMaskIntoConstraints = false
                
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1.25),

            sectionTitleLabel.topAnchor.constraint(equalTo: topAnchor),
            sectionTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            thumbnailButton.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8),
            thumbnailButton.widthAnchor.constraint(equalTo: widthAnchor),
            thumbnailButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            thumbnailButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            noImageIconView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.25),
            noImageIconView.heightAnchor.constraint(equalTo: noImageIconView.widthAnchor),
            noImageIconView.centerXAnchor.constraint(equalTo: centerXAnchor),
            noImageIconView.bottomAnchor.constraint(equalTo: thumbnailButton.centerYAnchor),
            
            noImageLabel.topAnchor.constraint(equalTo: thumbnailButton.centerYAnchor, constant: spacing),
            noImageLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    @objc private func thumbnailButtonPressed() {
        buttonDelegate.buttonPressed(for: .highlightsSection)
    }
}
