//
//  HomeTaskSectionView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class HomeTaskSectionView: UIView {
    
    private let sectionTitleLabel : UILabel = .preppedForAutoLayout()
    private let backgroundButton : BackgroundButton = .backgroundPreppedForAutoLayout()
    private let warningIconView : UIImageView = .preppedForAutoLayout()
    private let completedCircleView : UIView = .viewPreppedForAutoLayout()
    private let completedIconView : UIImageView = .preppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    
    unowned var buttonDelegate : HomeButtonDelegate!


    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(sectionTitleLabel)
        self.addSubview(backgroundButton)
        self.addSubview(warningIconView)
        self.addSubview(completedCircleView)
        self.addSubview(completedIconView)
        self.addSubview(titleLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backgroundButton.layer.cornerRadius = backgroundButton.frame.width/5
        completedCircleView.layer.cornerRadius = completedCircleView.frame.width/2
    }
    
    private func configureViews() {
                
        sectionTitleLabel.text = "Tasks"
        sectionTitleLabel.textColor = .black
        sectionTitleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .semibold))

        backgroundButton.highlightedColor = .black.withAlphaComponent(0.05)
        backgroundButton.layer.borderColor = UIColor.lightGray.cgColor
        backgroundButton.layer.borderWidth = 1
        backgroundButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        warningIconView.image = UIImage(systemName: "exclamationmark.triangle")
        warningIconView.contentMode = .scaleAspectFit
        warningIconView.tintColor = .systemYellow
        
        completedCircleView.backgroundColor = .customGreen
        completedCircleView.layer.borderWidth = 1
        completedCircleView.layer.borderColor = UIColor.customGreen.cgColor
        
        completedIconView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .medium))
        completedIconView.tintColor = .white
        completedIconView.contentMode = .scaleAspectFit
        
        titleLabel.text = "This is a tasks"
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 18, weight: .medium))
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
    }
    
    private func makeConstraints() {
        
        let width = UIScreen.main.bounds.width*0.4
        let height = width*1.25
        let buttonHeight = height*0.8
        let buttonSpacing = buttonHeight*0.1
        
        // Calculation to set text label height
        let iconSize = width*0.15
        let spacing = width*0.1
        let textHeight = buttonHeight - (buttonSpacing*2+iconSize)

        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1.25),

            sectionTitleLabel.topAnchor.constraint(equalTo: topAnchor),
            sectionTitleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),

            backgroundButton.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8),
            backgroundButton.widthAnchor.constraint(equalTo: widthAnchor),
            backgroundButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            backgroundButton.bottomAnchor.constraint(equalTo: bottomAnchor),

            warningIconView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15),
            warningIconView.heightAnchor.constraint(equalTo: warningIconView.widthAnchor),
            warningIconView.topAnchor.constraint(equalTo: backgroundButton.topAnchor, constant: buttonSpacing),
            warningIconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            
            completedCircleView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15),
            completedCircleView.heightAnchor.constraint(equalTo: completedCircleView.widthAnchor),
            completedCircleView.centerYAnchor.constraint(equalTo: warningIconView.centerYAnchor),
            completedCircleView.rightAnchor.constraint(equalTo: rightAnchor, constant: -spacing),
            
            completedIconView.widthAnchor.constraint(equalTo: completedCircleView.widthAnchor, multiplier: 0.9),
            completedIconView.heightAnchor.constraint(equalTo: completedIconView.widthAnchor),
            completedIconView.centerXAnchor.constraint(equalTo: completedCircleView.centerXAnchor),
            completedIconView.centerYAnchor.constraint(equalTo: completedCircleView.centerYAnchor),
            
            titleLabel.heightAnchor.constraint(equalToConstant: textHeight),
            titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -buttonSpacing),
        ])
    }
    
    @objc private func buttonPressed() {
        buttonDelegate.buttonPressed(for: .remindersSection)
    }

}
