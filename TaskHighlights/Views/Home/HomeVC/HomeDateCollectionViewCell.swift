//
//  HomeDateCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class HomeDateCollectionViewCell: UICollectionViewCell {
    
    static let cellSize = CGSize(width: size, height: size)
    
    private static let size = UIScreen.main.bounds.height*0.1-1
    
    override var isSelected: Bool {
        didSet { isSelectedChanged() }
    }
    
    private let containerView : UIView = .viewPreppedForAutoLayout()
    private let dayLabel : UILabel = .preppedForAutoLayout()
    private let weekdayLabel : UILabel = .preppedForAutoLayout()
    
    private let highlightedColor : UIColor = .customGreen.withAlphaComponent(0.7)
    
    var day : String {
        get { dayLabel.text ?? "" }
        set { dayLabel.text = newValue }
    }
    
    var weekday : String {
        get { weekdayLabel.text ?? "" }
        set { weekdayLabel.text = newValue }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configrueViews()
        
        self.contentView.addSubview(containerView)
        self.contentView.addSubview(dayLabel)
        self.contentView.addSubview(weekdayLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configrueViews() {
        
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = Self.size/5
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        
        dayLabel.textAlignment = .center
        dayLabel.textColor = .black
        dayLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 20))
        
        weekdayLabel.textAlignment = .center
        weekdayLabel.textColor = .gray
        weekdayLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16))
    }
    
    private func makeConstraints() {
        
        containerView.pin(to: contentView)
        
        NSLayoutConstraint.activate([
            dayLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor),
            dayLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            weekdayLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 5),
            weekdayLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        ])
    }
    
    private func isSelectedChanged() {
        
        containerView.layer.borderWidth = isSelected ? 0 : 1
        containerView.backgroundColor = isSelected ? highlightedColor : .white
        
        dayLabel.textColor = isSelected ? .white : .black
        weekdayLabel.textColor = isSelected ? .white : .gray
    }
    
}
