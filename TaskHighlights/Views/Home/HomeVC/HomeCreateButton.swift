//
//  HomeCreateButton.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class HomeCreateButton: UIButton {

    override var isHighlighted: Bool {
        didSet { backgroundColor = isHighlighted ? highlightedColor : .customGreen }
    }

    private let iconView : UIImageView = .preppedForAutoLayout()
    private let textLabel : UILabel = .preppedForAutoLayout()
    
    private let highlightedColor : UIColor = .customGreen.withAlphaComponent(0.5)
    private let textFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 17, weight: .semibold))
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(iconView)
        self.addSubview(textLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        backgroundColor = .customGreen
        clipsToBounds = true
        
        iconView.image = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(weight: .medium))
        iconView.tintColor = .white
        iconView.contentMode = .scaleAspectFit
        
        textLabel.text = "Create"
        textLabel.textColor = .white
        textLabel.font = textFont
    }
    
    private func makeConstraints() {
        
        let iconSize = UIScreen.main.bounds.width*0.06
        let height = iconSize*2
        let spacing = UIScreen.main.bounds.width*0.04
        let textSize = String.getLabelWidth(text: "Create", font: textFont)
        let width = spacing*3 + iconSize + textSize
        
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = height/2
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: height),
            widthAnchor.constraint(equalToConstant: width),
            
            iconView.widthAnchor.constraint(equalToConstant: iconSize),
            iconView.heightAnchor.constraint(equalTo: iconView.widthAnchor),
            iconView.leftAnchor.constraint(equalTo: leftAnchor, constant: spacing),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            textLabel.leftAnchor.constraint(equalTo: iconView.rightAnchor, constant: spacing),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
    }
    
}


