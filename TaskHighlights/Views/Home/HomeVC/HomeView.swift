//
//  HomeView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-11.
//

import UIKit

class HomeView: UIView {
    
    private let weatherButton : IconButton = .iconPreppedForAutoLayout()
    private let settingsButton : IconButton = .iconPreppedForAutoLayout()
    private let monthAndYearLabel : UILabel = .preppedForAutoLayout()
    let collectionView : DefaultCollectionView = .defaultPreppedForAutoLayout()
    private let viewCalendarButton : UIButton = .preppedForAutoLayout()
    private let createButton = HomeCreateButton()
    private let highlightsSectionView = HomeHighlightsSectionView()
    private let reminderSectionView = HomeRemindersSectionView()
    private let tasksSectionView = HomeTaskSectionView()
    
    unowned var buttonDelegate : HomeButtonDelegate! {
        didSet {
            highlightsSectionView.buttonDelegate = buttonDelegate
            reminderSectionView.buttonDelegate = buttonDelegate
            tasksSectionView.buttonDelegate = buttonDelegate
        }
    }
    
    var monthAndYearText : String {
        get { monthAndYearLabel.text ?? "" }
        set { monthAndYearLabel.text = newValue }
    }
    

    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(weatherButton)
        self.addSubview(settingsButton)
        self.addSubview(monthAndYearLabel)
        self.addSubview(collectionView)
        self.addSubview(viewCalendarButton)
        self.addSubview(createButton)
        self.addSubview(highlightsSectionView)
        self.addSubview(reminderSectionView)
        self.addSubview(tasksSectionView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pinToSafeArea(superview)
    }
    
    private func configureViews() {
        
        backgroundColor = .white
        
        weatherButton.setImage(UIImage(systemName: "cloud.sun.rain"), for: .normal)
        weatherButton.imageView?.contentMode = .scaleAspectFit
        weatherButton.baseColor = .black
        weatherButton.pinImageView()
        weatherButton.addTarget(self, action: #selector(weatherButtonPressed), for: .touchUpInside)
        
        settingsButton.setImage(UIImage(systemName: "gearshape"), for: .normal)
        settingsButton.imageView?.contentMode = .scaleAspectFit
        settingsButton.baseColor = .black
        settingsButton.pinImageView()
        settingsButton.addTarget(self, action: #selector(settingsButtonPressed), for: .touchUpInside)
        
        monthAndYearLabel.textColor = .black
        monthAndYearLabel.textAlignment = .center
        monthAndYearLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 20, weight: .bold))

        viewCalendarButton.setTitle("View Full Calendar", for: .normal)
        viewCalendarButton.setTitleTextPressColors(.black)
        viewCalendarButton.titleLabel?.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 15, weight: .semibold))
        viewCalendarButton.addTarget(self, action: #selector(viewFullCalendarButtonPressed), for: .touchUpInside)
        
        createButton.addTarget(self, action: #selector(createButtonPressed), for: .touchUpInside)
    }
    
    private func makeConstraints() {
        let height = UIScreen.main.bounds.height*0.1
        let collectionSpacing = UIScreen.main.bounds.height*0.04
        let buttonSpacing = UIScreen.main.bounds.width*0.05
        let sectionSpacing = UIScreen.main.bounds.width*0.05
                
        NSLayoutConstraint.activate([
            weatherButton.topAnchor.constraint(equalTo: topAnchor),
            weatherButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            weatherButton.heightAnchor.constraint(equalTo: weatherButton.widthAnchor),
            weatherButton.leftAnchor.constraint(equalTo: leftAnchor, constant: buttonSpacing),
           
            settingsButton.topAnchor.constraint(equalTo: topAnchor),
            settingsButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            settingsButton.heightAnchor.constraint(equalTo: settingsButton.widthAnchor),
            settingsButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -buttonSpacing),
            
            monthAndYearLabel.topAnchor.constraint(equalTo: settingsButton.bottomAnchor),
            monthAndYearLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            monthAndYearLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            
            collectionView.topAnchor.constraint(equalTo: monthAndYearLabel.bottomAnchor, constant: collectionSpacing),
            collectionView.widthAnchor.constraint(equalTo: widthAnchor),
            collectionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: height),
            
            viewCalendarButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 15),
            viewCalendarButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -buttonSpacing),
            
            createButton.topAnchor.constraint(equalTo: viewCalendarButton.bottomAnchor, constant: 30),
            createButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            highlightsSectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: sectionSpacing),
            highlightsSectionView.topAnchor.constraint(equalTo: createButton.bottomAnchor, constant: 30),
            
            reminderSectionView.topAnchor.constraint(equalTo: highlightsSectionView.bottomAnchor, constant: 30),
            reminderSectionView.centerXAnchor.constraint(equalTo: highlightsSectionView.centerXAnchor),
            
            tasksSectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -sectionSpacing),
            tasksSectionView.topAnchor.constraint(equalTo: highlightsSectionView.topAnchor),
        ])
    }
    
    @objc private func weatherButtonPressed() {
        buttonDelegate.buttonPressed(for: .weather)
    }
    
    @objc private func settingsButtonPressed() {
        buttonDelegate.buttonPressed(for: .settings)
    }
    
    @objc private func viewFullCalendarButtonPressed() {
        buttonDelegate.buttonPressed(for: .viewCalendar)
    }
    
    @objc private func createButtonPressed() {
        buttonDelegate.buttonPressed(for: .create)
    }

}
