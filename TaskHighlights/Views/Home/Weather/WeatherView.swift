//
//  WeatherView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-25.
//

import UIKit

class WeatherView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.7

    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = WeatherModalView()
    
    private var modalBottomConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : WeatherButtonDelegate!

    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    
    var modalBottomConstant : CGFloat {
        get { modalBottomConstraint.constant }
        set { modalBottomConstraint.constant = newValue }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGesture()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        modalView.bottomSafeArea = safeAreaInsets.bottom
    }

    private func configureViews() {
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        dimmingView.pin(to: self)
        modalBottomConstraint = modalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
        
        NSLayoutConstraint.activate([
            modalBottomConstraint,
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func setUpGesture() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingTapped))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    @objc private func dimmingTapped() {
        buttonDelegate.closeButtonPressed()
    }
    
}
