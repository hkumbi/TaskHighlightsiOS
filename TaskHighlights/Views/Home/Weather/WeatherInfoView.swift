//
//  WeatherInfoView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-26.
//

import UIKit

class WeatherInfoView: UIView {
    
    static let height = String.getLabelHeight(text: "", font: titleFont) + spacing + String.getLabelHeight(text: "", font: valueFont)
    
    private static let spacing = UIScreen.main.bounds.height*0.02
    private static let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
    private static let valueFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 22, weight: .semibold))

    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let valueLabel : UILabel = .preppedForAutoLayout()
    
    private var widthConstraint : NSLayoutConstraint!
            
    var title : String {
        get { titleLabel.text ?? "" }
        set {
            titleLabel.text = newValue
            updatedWidth()
        }
    }
    
    var value : String {
        get { valueLabel.text ?? "" }
        set {
            valueLabel.text = newValue
            updatedWidth()
        }
    }

    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(titleLabel)
        self.addSubview(valueLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        titleLabel.font = Self.titleFont
        titleLabel.textColor = .white
        
        valueLabel.font = Self.valueFont
        valueLabel.textColor = .white
    }
    
    private func makeConstraints() {
                
        translatesAutoresizingMaskIntoConstraints = false
        
        widthConstraint = widthAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Self.height),
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            valueLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            valueLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func updatedWidth() {
        let titleWidth = String.getLabelWidth(text: title, font: Self.titleFont)
        let valueWidth = String.getLabelWidth(text: value, font: Self.valueFont)
        
        widthConstraint.constant = titleWidth.returnHighest(valueWidth)
    }
    
}
