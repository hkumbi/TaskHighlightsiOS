//
//  WeatherTimeOfDayView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-25.
//

import UIKit

class WeatherTimeOfDayView: UIView {
    
    static let height = String.getLabelHeight(text: "", font: titleFont) + spacing +  String.getLabelHeight(text: "", font: valueFont)
    
    private static let spacing = UIScreen.main.bounds.height*0.02
    private static let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12, weight: .medium))
    private static let valueFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 22, weight: .semibold))

    
    private let titleLabel : UILabel = .preppedForAutoLayout()
    private let tempLabel : UILabel = .preppedForAutoLayout()
    private let feelsLikeTempLabel : UILabel = .preppedForAutoLayout()
    
    private var tempWidth : CGFloat = 0
    private var feelsLikeWidth : CGFloat = 0
    private var minimumWidth : CGFloat = 0
    private let spacing = UIScreen.main.bounds.width*0.04
    
    private var widthConstraint : NSLayoutConstraint!

    
    var title : String {
        get { titleLabel.text ?? "" }
        set {
            titleLabel.text = newValue
            minimumWidth = String.getLabelWidth(text: newValue, font: Self.titleFont)
            updateWidth()
        }
    }
    
    var temp : String {
        get { tempLabel.text ?? "" }
        set {
            tempLabel.text = newValue
            tempWidth = String.getLabelWidth(text: newValue, font: Self.valueFont)
            updateWidth()
        }
    }
    
    var feelsLikeTemp : String {
        get { feelsLikeTempLabel.text ?? "" }
        set {
            feelsLikeTempLabel.text = newValue
            feelsLikeWidth = String.getLabelWidth(text: newValue, font: Self.valueFont)
            updateWidth()
        }
    }
    
    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(titleLabel)
        self.addSubview(tempLabel)
        self.addSubview(feelsLikeTempLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        
        titleLabel.font = Self.titleFont
        titleLabel.textColor = .white
        
        tempLabel.font = Self.valueFont
        tempLabel.textColor = .white
        
        feelsLikeTempLabel.font = Self.valueFont
        feelsLikeTempLabel.textColor = .white.withAlphaComponent(0.7)
    }
    
    private func makeConstraints() {
        
        translatesAutoresizingMaskIntoConstraints = false
        
        widthConstraint = widthAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: Self.height),
            widthConstraint,
            
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            tempLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            tempLabel.leftAnchor.constraint(equalTo: leftAnchor),
            
            feelsLikeTempLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            feelsLikeTempLabel.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
    
    private func updateWidth() {
        let valueWidth = tempWidth + feelsLikeWidth + spacing
        widthConstraint.constant = valueWidth.returnHighest(minimumWidth)
    }

}
