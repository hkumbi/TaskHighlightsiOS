//
//  WeatherModalView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-25.
//

import UIKit

enum Weather {
case sun, sunCloudy, sunCloudyRain, cloudBolt, cloudRainBolt, cloudRain, cloudSnow
}

class WeatherModalView: UIView {

    private let lineView : UILabel = .preppedForAutoLayout()
    private let closeButton : IconButton = .iconPreppedForAutoLayout()
    private let updatedTimeLabel : UILabel = .preppedForAutoLayout()
    private let tempIconView : UIImageView = .preppedForAutoLayout()
    private let weatherLabel : UILabel = .preppedForAutoLayout()
    private let tempLabel : UILabel = .preppedForAutoLayout()
    private let morningTempView = WeatherTimeOfDayView()
    private let dayTempView = WeatherTimeOfDayView()
    private let nightTempView = WeatherTimeOfDayView()
    private let tempTitleLabel : UILabel = .preppedForAutoLayout()
    private let feelsLikeTitleLabel : UILabel = .preppedForAutoLayout()
    private let infoStackView : UIStackView = .preppedForAutoLayout()
    private let humidityView = WeatherInfoView()
    private let windView = WeatherInfoView()
    private let precipitationView = WeatherInfoView()

    let height : CGFloat
    private let tempIconHeight = UIScreen.main.bounds.width*0.15
    private let topSpacing : CGFloat = 15
    private let lineHeight : CGFloat = 2
    private let spacing = UIScreen.main.bounds.height*0.03
    
    private var heightConstraint : NSLayoutConstraint!
    private var infoStackWidthConstraint : NSLayoutConstraint!
    private let timeFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12))
        
    unowned var buttonDelegate : WeatherButtonDelegate!
    
    var bottomSafeArea : CGFloat {
        get { heightConstraint.constant - height }
        set { heightConstraint.constant = newValue + height }
    }
    
    
    init() {
        
        let timeHeight = String.getLabelHeight(text: "", font: timeFont)
        
        height = topSpacing + lineHeight + timeHeight + tempIconHeight + WeatherTimeOfDayView.height*2 + WeatherInfoView.height + spacing*8

        super.init(frame: .zero)
                
        configureViews()
        
        self.addSubview(lineView)
        self.addSubview(closeButton)
        self.addSubview(updatedTimeLabel)
        self.addSubview(tempIconView)
        self.addSubview(tempLabel)
        self.addSubview(morningTempView)
        self.addSubview(dayTempView)
        self.addSubview(nightTempView)
        self.addSubview(tempTitleLabel)
        self.addSubview(feelsLikeTitleLabel)
        self.addSubview(infoStackView)
        
        infoStackView.addArrangedSubview(humidityView)
        infoStackView.addArrangedSubview(windView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    private func configureViews() {
        layer.cornerRadius = 15
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        lineView.backgroundColor = .white
        lineView.layer.cornerRadius = 1
        lineView.clipsToBounds = true
        
        closeButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        closeButton.baseColor = .white
        closeButton.imageView?.contentMode = .scaleAspectFit
        closeButton.pinImageView()
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        updatedTimeLabel.text = "Friday 5:45PM"
        updatedTimeLabel.textColor = .white
        updatedTimeLabel.font = timeFont
        
        let config = UIImage.SymbolConfiguration(paletteColors: [.white, .yellow])
        tempIconView.image = UIImage(systemName: "cloud.bolt.fill")?.applyingSymbolConfiguration(config)
        tempIconView.contentMode = .scaleAspectFit
        
        tempLabel.text = "30º"
        tempLabel.textColor = .white
        tempLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 30, weight: .bold))
        
        morningTempView.title = "Morning"
        morningTempView.temp = "16º"
        morningTempView.feelsLikeTemp = "14º"
        
        dayTempView.title = "Day"
        dayTempView.temp = "27º"
        dayTempView.feelsLikeTemp = "30º"
        
        nightTempView.title = "Night"
        nightTempView.temp = "14º"
        nightTempView.feelsLikeTemp = "18º"
        
        let titleFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 12, weight: .medium))
        
        tempTitleLabel.text = "Temp"
        tempTitleLabel.textColor = .white
        tempTitleLabel.font = titleFont
        
        feelsLikeTitleLabel.text = "Feels Like"
        feelsLikeTitleLabel.textColor = .white.withAlphaComponent(0.7)
        feelsLikeTitleLabel.font = titleFont
        
        infoStackView.axis = .horizontal
        infoStackView.alignment = .center
        infoStackView.distribution = .fillEqually
        infoStackView.layer.borderColor = UIColor.white.cgColor
        
        humidityView.title = "Humidity"
        humidityView.value = "56%"
        
        windView.title = "Wind"
        windView.value = "18kph"
        
        precipitationView.title = "Rain"
        precipitationView.value = "45%"
    }
    
    private func makeConstraints() {
        
        let centerSpacing = UIScreen.main.bounds.width/3
        let sideSpacing = UIScreen.main.bounds.width*0.05
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: height)
        infoStackWidthConstraint = infoStackView.widthAnchor.constraint(equalToConstant: 0)
        
        NSLayoutConstraint.activate([
            heightConstraint,
            widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            lineView.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            lineView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.08),
            lineView.heightAnchor.constraint(equalToConstant: lineHeight),
            lineView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            closeButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.06),
            closeButton.heightAnchor.constraint(equalTo: closeButton.widthAnchor),
            closeButton.topAnchor.constraint(equalTo: topAnchor, constant: topSpacing),
            closeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -sideSpacing),
            
            updatedTimeLabel.topAnchor.constraint(equalTo: lineView.bottomAnchor, constant: spacing),
            updatedTimeLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            tempIconView.topAnchor.constraint(equalTo: updatedTimeLabel.bottomAnchor, constant: spacing),
            tempIconView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15),
            tempIconView.heightAnchor.constraint(equalTo: tempIconView.widthAnchor),
            tempIconView.rightAnchor.constraint(equalTo: centerXAnchor, constant: -sideSpacing/2),
            
            tempLabel.centerYAnchor.constraint(equalTo: tempIconView.centerYAnchor),
            tempLabel.leftAnchor.constraint(equalTo: centerXAnchor, constant: sideSpacing/2),
            
            morningTempView.topAnchor.constraint(equalTo: dayTempView.bottomAnchor, constant: spacing),
            morningTempView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -centerSpacing),
            
            dayTempView.topAnchor.constraint(equalTo: tempIconView.bottomAnchor, constant: spacing),
            dayTempView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            nightTempView.topAnchor.constraint(equalTo: dayTempView.bottomAnchor, constant: spacing),
            nightTempView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: centerSpacing),
            
            tempTitleLabel.topAnchor.constraint(equalTo: nightTempView.bottomAnchor, constant: spacing),
            tempTitleLabel.rightAnchor.constraint(equalTo: centerXAnchor, constant: -sideSpacing/2),
            
            feelsLikeTitleLabel.topAnchor.constraint(equalTo: nightTempView.bottomAnchor, constant: spacing),
            feelsLikeTitleLabel.leftAnchor.constraint(equalTo: centerXAnchor, constant: sideSpacing/2),
            
            infoStackView.topAnchor.constraint(equalTo: tempTitleLabel.bottomAnchor, constant: spacing*2),
            infoStackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            infoStackView.heightAnchor.constraint(equalToConstant: WeatherInfoView.height),
            infoStackWidthConstraint
        ])
    }
    
    @objc private func closeButtonPressed() {
        buttonDelegate.closeButtonPressed()
    }
    
}

extension WeatherModalView {
    
    func setPrecipitationVisibility(_ visible : Bool) {
        
        let width = UIScreen.main.bounds.width
        if visible {
            
            infoStackView.addArrangedSubview(precipitationView)
            infoStackWidthConstraint.constant = width*0.9
            precipitationView.isHidden = false
        } else {

            infoStackView.removeArrangedSubview(precipitationView)
            infoStackWidthConstraint.constant = width*0.6
            precipitationView.isHidden = true
        }
    }

    
}
