//
//  CalendarModalView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class CalendarModalView: UIView {

    private let previousButton : IconButton = .iconPreppedForAutoLayout()
    private let monthAndYearLabel : UILabel = .preppedForAutoLayout()
    private let nextButton : IconButton = .iconPreppedForAutoLayout()
    let collectionView : UICollectionView = .preppedForAutoLayout()
    private let bottomBarView : UIView = .viewPreppedForAutoLayout()
    
    private let height : CGFloat
    private let collecionHeight = CalendarDayCollectionViewCell.size*6
    private let spacing = UIScreen.main.bounds.height*0.04
    private let edgeSpacing = UIScreen.main.bounds.height*0.02
    private let bottomBarHeight : CGFloat = 3
    
    private var heightConstraint : NSLayoutConstraint!
    private var topMonthAndYearLabelConstraint : NSLayoutConstraint!
    private let monthAndYearFont = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 20, weight: .bold))
    
    unowned var buttonDelegate : CalendarButtonDelegate!
    
    var heightConstant : CGFloat {
        get { heightConstraint.constant }
        set { heightConstraint.constant = newValue }
    }
    
    var monthAndYear : String {
        get { monthAndYearLabel.text ?? "" }
        set { monthAndYearLabel.text = newValue }
    }
    
    
    init() {
        
        let titleHeight = String.getLabelHeight(text: "", font: monthAndYearFont)
        
        height = titleHeight + spacing*2 + collecionHeight + bottomBarHeight + edgeSpacing*2
        
        super.init(frame: .zero)
        
        configureViews()
        
        self.addSubview(previousButton)
        self.addSubview(monthAndYearLabel)
        self.addSubview(nextButton)
        self.addSubview(collectionView)
        self.addSubview(bottomBarView)
        
        makeConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        topMonthAndYearLabelConstraint.constant = safeAreaInsets.top + edgeSpacing
        heightConstraint.constant = height + safeAreaInsets.top
    }
    
    
    private func configureViews() {
        backgroundColor = .white
        layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        layer.cornerRadius = 20
        
        previousButton.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        previousButton.baseColor = .black
        previousButton.imageView?.contentMode = .scaleAspectFit
        previousButton.addTarget(self, action: #selector(previousButtonPressed), for: .touchUpInside)
        
        monthAndYearLabel.text = "September 2022"
        monthAndYearLabel.textColor = .black
        monthAndYearLabel.textAlignment = .center
        monthAndYearLabel.font = monthAndYearFont

        nextButton.setImage(UIImage(systemName: "chevron.right"), for: .normal)
        nextButton.baseColor = .black
        nextButton.imageView?.contentMode = .scaleAspectFit
        nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        
        
        bottomBarView.backgroundColor = .black
        bottomBarView.layer.cornerRadius = bottomBarHeight/2
    }
    
    private func makeConstraints() {
        
        let width = UIScreen.main.bounds.width
        
        translatesAutoresizingMaskIntoConstraints = false
        
        heightConstraint = heightAnchor.constraint(equalToConstant: 0)
        topMonthAndYearLabelConstraint = monthAndYearLabel.topAnchor.constraint(equalTo: topAnchor)
        
        previousButton.pinImageView()
        nextButton.pinImageView()
        
        NSLayoutConstraint.activate([
            widthAnchor.constraint(equalToConstant: width),
            heightConstraint,
            
            previousButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            previousButton.heightAnchor.constraint(equalTo: previousButton.widthAnchor),
            previousButton.centerYAnchor.constraint(equalTo: monthAndYearLabel.centerYAnchor),
            previousButton.rightAnchor.constraint(equalTo: monthAndYearLabel.leftAnchor),
            
            topMonthAndYearLabelConstraint,
            monthAndYearLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            monthAndYearLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.6),
            
            nextButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.07),
            nextButton.heightAnchor.constraint(equalTo: nextButton.widthAnchor),
            nextButton.centerYAnchor.constraint(equalTo: monthAndYearLabel.centerYAnchor),
            nextButton.leftAnchor.constraint(equalTo: monthAndYearLabel.rightAnchor),
            
            collectionView.topAnchor.constraint(equalTo: monthAndYearLabel.bottomAnchor, constant: spacing),
            collectionView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
            collectionView.heightAnchor.constraint(equalToConstant: collecionHeight),
            collectionView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            bottomBarView.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: spacing),
            bottomBarView.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomBarView.heightAnchor.constraint(equalToConstant: bottomBarHeight),
            bottomBarView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.1),
            
        ])
    }
    
    @objc private func previousButtonPressed() {
        buttonDelegate.buttonPressed(for: .previous)
    }
    
    @objc private func nextButtonPressed() {
        buttonDelegate.buttonPressed(for: .next)
    }
    
}
