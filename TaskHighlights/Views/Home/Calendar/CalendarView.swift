//
//  CalendarView.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class CalendarView: UIView {
    
    static let maxDimmingAlpha : CGFloat = 0.6

    private let dimmingView : UIView = .viewPreppedForAutoLayout()
    let modalView = CalendarModalView()
    
    private var modalTopConstraint : NSLayoutConstraint!
    
    unowned var buttonDelegate : CalendarButtonDelegate!

    var dimmingAlpha : CGFloat {
        get { dimmingView.alpha }
        set { dimmingView.alpha = newValue }
    }
    
    var modalTopConstant : CGFloat {
        get { modalTopConstraint.constant }
        set { modalTopConstraint.constant = newValue }
    }

    
    init() {
        super.init(frame: .zero)
        
        configureViews()
        setUpGestures()
        
        self.addSubview(dimmingView)
        self.addSubview(modalView)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        guard let superview = superview else { return }
        
        pin(to: superview)
    }

    private func configureViews() {
        dimmingView.backgroundColor = .black
        dimmingView.alpha = 0
    }
    
    private func makeConstraints() {
        
        dimmingView.pin(to: self)
        
        modalTopConstraint = modalView.topAnchor.constraint(equalTo: topAnchor)
        
        NSLayoutConstraint.activate([
            modalTopConstraint,
            modalView.centerXAnchor.constraint(equalTo: centerXAnchor),
        ])
    }
    
    private func setUpGestures() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissTap))
        tap.numberOfTapsRequired = 1
        dimmingView.addGestureRecognizer(tap)
    }
    
    @objc private func dismissTap() {
        buttonDelegate.buttonPressed(for: .close)
    }
    
    func setUpModalPanGesture(_ pan : UIPanGestureRecognizer) {
        modalView.addGestureRecognizer(pan)
    }
}
