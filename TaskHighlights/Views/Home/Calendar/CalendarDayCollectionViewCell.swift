//
//  CalendarDayCollectionViewCell.swift
//  TaskHighlights
//
//  Created by Herve Kumbi on 2022-08-12.
//

import UIKit

class CalendarDayCollectionViewCell: UICollectionViewCell {
    
    static let cellSize = CGSize(width: size, height: size)
    static let size = UIScreen.main.bounds.width*0.9/7
    
    
    override var isSelected: Bool {
        didSet {
            containerView.backgroundColor = isSelected ? .customGreen : .white
            titleLabel.textColor = isSelected ? .white : .black
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            containerView.backgroundColor = isHighlighted ? highlightedColor : .white
        }
    }
    
    private let containerView : UIView = .viewPreppedForAutoLayout()
    private let titleLabel : UILabel = .preppedForAutoLayout()
    
    private let highlightedColor : UIColor = .customGreen.withAlphaComponent(0.3)
    
    
    var text : String {
        get { titleLabel.text ?? "" }
        set { titleLabel.text = newValue }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
        
        self.contentView.addSubview(containerView)
        self.contentView.addSubview(titleLabel)
        
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not using storyboards")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        text = ""
    }
    
    private func configureViews() {
        
        containerView.clipsToBounds = true
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = Self.size/5
        
        titleLabel.textColor = .black
        titleLabel.font = UIFontMetrics.default.scaledFont(for: UIFont.systemFont(ofSize: 16, weight: .medium))
    }
    
    private func makeConstraints() {
        
        containerView.pin(to: contentView)
        
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
}

